package solutions;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Threads;
import org.openjdk.jmh.infra.Blackhole;

/**
 * 1 pom
 *
 *
 * Benchmarks:
 Result "solutions.AvgArraySolution.smallArr6_andk4":
 162045688.119 ±(99.9%) 3318074.020 ops/s [Average]
 (min, avg, max) = (146085487.792, 162045688.119, 170911314.475), stdev = 5897873.658
 CI (99.9%): [158727614.099, 165363762.139] (assumes normal distribution)


 # Run complete. Total time: 00:03:01

 Benchmark                   Mode  Cnt          Score         Error  Units
 AvgArraySolution.bigArr180_andk10  thrpt   40    7910199.903 ±   75386.714  ops/s
 AvgArraySolution.smallArr6_andk4   thrpt   40  162045688.119 ± 3318074.020  ops/s

 Same but with Threads = 1 (results are almost the same, just double checking that default is one)

 Benchmark                   Mode  Cnt          Score         Error  Units
 AvgArraySolution.bigArr180_andk10  thrpt   40    7982239.509 ±   97923.645  ops/s
 AvgArraySolution.smallArr6_andk4   thrpt   40  170159802.427 ± 2897759.457  ops/s

 here after removal of Math.max (and on batter - check when plugged)

 AvgArraySolution.bigArr180_andk10                      thrpt   40    8589531.310 ±   78996.247   ops/s
 AvgArraySolution.smallArr6_andk4                       thrpt   40  160458087.135 ± 1984585.971   ops/s

 *
 */
@Threads(1)
@State(Scope.Thread)
public class AvgArraySolution {
    public static void main(String[] args) {
//        char a = (char)( 1+'0');
        long a = 6913259244L * 71103343L;
        System.out.println(a);
    }
    private int[] smallArr6 = new int[]{1,12,-5,-6,50,3};

    private int[] bigArr180 = new int[]{1,12,-5,-6,50, 3, 20, 40, 1, 1,12,-5,-6,50,3, 1,12,-5,-6,50,3, 1,12,-5,-6,50,
            1,12,-5,-6,50, 3, 20, 40, 1, 1,12,-5,-6,50,3, 1,12,-5,-6,50,3, 1,12,-5,-6,50,3,
            1,12,-5,-6,50, 3, 20, 40, 1, 1,12,-5,-6,50,3, 1,12,-5,-6,50,3, 1,12,-5,-6,50,15,
            1,12,-5,-6,50, 3, 20, 40, 1, 1,12,-5,-6,50,3, 1,12,-5,-6,50,3, 1,12,-5,-6,50,3,
            1,12,-5,-6,50, 3, 20, 40, 1, 1,12,-5,-6,50,3, 1,12,-5,-6,50,3, 1,12,-5,-6,50,3,
            1,12,-5,-6,50, 3, 20, 40, 1, 1,12,-5,-6,50,3, 1,12,-5,-6,50,3, 1,12,-5,-6,50,3,
            1,12,-5,-6,50, 3, 20, 40, 1, 1,12,-5,-6,50,3, 1,12,-5,-6,50,3
    };

    @Benchmark
    public void smallArr6_andk4(Blackhole bh) {
        bh.consume(findMaxAverage(smallArr6, 4));
    }

    @Benchmark
    public void bigArr180_andk10(Blackhole bh) {
        bh.consume(findMaxAverage(bigArr180, 10));
    }


    public double findMaxAverage(int[] nums, int k) {
        int currSum = 0;
        for (int i = 0; i < k; i++) {
            currSum += nums[i];
        }
        int max = currSum;
        for (int i = 0; i < nums.length - k; i++) {
            currSum = currSum - nums[i] + nums[i + k]; // drop the first one and add a new one from right
            if(currSum > max ) max = currSum;
        }
        return ((double) max / k);
    }
}
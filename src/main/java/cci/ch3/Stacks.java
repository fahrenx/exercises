package cci.ch3;

import org.assertj.core.api.Assertions;

import java.util.Arrays;
import java.util.Stack;

public class Stacks {

    public static class QueueOnStacks {

        private final Stack<Integer> a = new Stack<>();
        private final Stack<Integer> b = new Stack<>();

        public void add(int el) {
            a.push(el);


        }

        public int peek() {
            return b.peek();
        }

        public int remove() {
            return b.pop();
        }

    }


    public static class MinStack extends Stack<Integer> {

        private final Stack<Integer> mins = new Stack<>();

        public Integer push(Integer el){
            if(mins.isEmpty() || el < mins.peek()){
                mins.push(el);
            }
            return super.push(el);
        }

        public Integer pop(){
            Integer el = super.pop();
            if(el.equals(mins.peek())){
                mins.pop();
            }
            return el;
        }

        public Integer min() {
            return mins.peek();
        }
    }


    public static void hanoi(int n, Stack<Integer> from, Stack<Integer> to, Stack<Integer> aux){
        if(n > 0){
            hanoi(n-1, from, aux, to);
            to.push(from.pop());
            printStacks(from, aux, to);
            hanoi(n-1, aux, to, from);
        }
    }

    private static void printStacks(Stack<Integer> a, Stack<Integer> b, Stack<Integer> c) {
        System.out.println("|-" + Arrays.toString(a.toArray(new Integer[]{})));
        System.out.println("|-" + Arrays.toString(b.toArray(new Integer[]{})));
        System.out.println("|-" + Arrays.toString(c.toArray(new Integer[]{})));
        System.out.println("----------------");
    }


    public static void main(String[] args) {
        testMinStack();
        testHanoi();
        testSortingOnStacks();
    }

    private static void testSortingOnStacks(){
        Stack<Integer> s = new Stack<>();
        s.push(8);
        s.push(4);
        s.push(5);
        s.push(2);
        s.push(6);
        sortStack(s);
        Assertions.assertThat(s.pop()).isEqualTo(8);
        Assertions.assertThat(s.pop()).isEqualTo(6);
        Assertions.assertThat(s.pop()).isEqualTo(5);
        Assertions.assertThat(s.pop()).isEqualTo(4);
        Assertions.assertThat(s.pop()).isEqualTo(2);
    }

    private static void sortStack(Stack<Integer> s){
        Stack<Integer> aux = new Stack<>();
        while(!s.isEmpty()){
            int el = s.pop();
            // finding the right place for el on aux, on top of smaller one
            while(!aux.isEmpty() && aux.peek() < el){
                int temp = aux.pop();
                s.push(temp);
            }
            aux.push(el);
        }
        // or don't do and just return aux stack
        while(!aux.isEmpty()){
            s.push(aux.pop());
        }
    }

    private static void testHanoi() {
        Stack<Integer> s = new Stack<>();
        s.push(4);
        s.push(3);
        s.push(2);
        s.push(1);
        hanoi(s.size(), s, new Stack<>(), new Stack<>());
    }

    private static void testMinStack() {
        MinStack stack = new MinStack();
        stack.push(8);
        stack.push(4);
        stack.push(5);
        stack.push(2);
        Assertions.assertThat(stack.min()).isEqualTo(2);
        Assertions.assertThat(stack.pop()).isEqualTo(2);
        Assertions.assertThat(stack.min()).isEqualTo(4);
        Assertions.assertThat(stack.pop()).isEqualTo(5);
        Assertions.assertThat(stack.min()).isEqualTo(4);
        Assertions.assertThat(stack.pop()).isEqualTo(4);
        Assertions.assertThat(stack.min()).isEqualTo(8);
        Assertions.assertThat(stack.pop()).isEqualTo(8);
    }
}

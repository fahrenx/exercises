package cci.ch5;

public class Exercises {


    public static void main(String[] args) {
        System.out.println(Integer.toBinaryString(updateBits(128, 16, 1, 6)));
        System.out.println(doubleToBinary(0.125));
        System.out.println(Integer.toBinaryString(findNextBiggerSameBits(6)));
        System.out.println(Integer.toBinaryString(findNextBiggerSameBits(4)));
        System.out.println(howManyToConvert(4, 2));
    }

    private static int howManyToConvert(int a, int b) {
        int xor = a ^ b;
        int bitCount = 0;
        for(int c = xor ; c !=0 ; c = c >> 1){
            if((c & 1) == 1) bitCount++;
        }
        for(int c = xor ; c !=0 ; c = c & (c-1)){
            bitCount++;
        }
        return bitCount;
    }

    private static int findNextBiggerSameBits(int n) {
        // finding first non-trailing one
        int c = n;
        int c0 = 0;
        while((c & 1) == 0 && c != 0){
            c0++;
            c>>=1;
        }
        // count trailing ones (starting from LSB)
        int c1 = 0;
        while((c & 1) == 1 && c != 0){
            c1++;
            c>>=1;
        }
        // if 0000...111 or 111..00 then impossible to find bigger one with the same number of bits
        if(c0 + c1 == 31 || c0 + c1 == 0) {
            return -1;
        }


        int p = c0 + c1; // place where the new bit will be set

        n |= (1 << p);
        n &= ~((1 << p) - 1); // clear all bits after p
        n |= (1 << (c1 -1)) - 1; // setting first c1-1 bits;
        return n;
    }


    private static String doubleToBinary(double d) {
        assert d > 0 && d <= 1.0;
        StringBuilder sb = new StringBuilder(".");
        while(d > 0.0){
            if(sb.length() >= 32) throw new IllegalArgumentException("ERROR");

            d = 2.0 * d;
            if(d >= 1.0) {
                sb.append("1");
                d = d - 1.0;
            } else {
                sb.append("0");
            }
        }
        return sb.toString();
    }

    private static int updateBits(int n, int m, int i, int j){
        int allOnes = ~0;

        // all ones until j, then zeros , e.g. 11100000 (j==4)
        int left = allOnes << (j+1);

        // all zeros until i, then ones, e.g. 00000011 (i==2)
        int right = (1 << i) - 1;

        int combinedMask = left | right;

        int nZeroedInRange = n & combinedMask;
        int shiftedM = m << i;

        return nZeroedInRange | shiftedM;
    }
}

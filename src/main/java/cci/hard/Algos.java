package cci.hard;

import java.util.Arrays;
import java.util.Random;

public class Algos {


    public static void main(String[] args) {
        System.out.println(addBinary(1, 5));
        System.out.println(Arrays.toString(shuffleDeck(new int[]{3, 4, 5, 10, 13})));
        System.out.println(countTwos(122));
    }


    static int countTwos(int n) {
        int score = 1;
        int mul = 10;
        int twos = 0;
        int num = n;
        while (n* 10 >= mul) {
            // crude
            twos += (n / mul) * score;
            System.out.println("Before fine-grain: " + twos + " // mul ==" + mul);
            // fine-grained - looking at previous digit as maybe we can count another one or partial at least
            int digit = num%10;
            num = num/10;
            int toAdd = 0;
            System.out.println("Digit: " + digit);
            if(digit < 2){
                // nothing to add e.g. 1112 and mul=100 gives 11 * 10 twos and that's enough
            } else if( digit == 2) {
                // e.g. 1322 and mul=100 gives 13 * 10 of twos and now toAdd = 3
                // calculated as 1322 - (13* mul + 2 * (mul/10)) +1
                toAdd = n - ((n/mul) * mul + 2 * mul/10)+1;

            } else {
                // e.g. 1330 and mul =100 gives 13 * 10 of twos and now toAdd = 10
                toAdd = score;
            }
            twos+=toAdd;
            System.out.println("After fine-grain: " + twos + " // mul ==" + mul);
            mul *= 10;
            score *= 10;
        }
        return twos;
    }

    static int perfectRNG(int lower, int upper) {
        return lower + (int) (new Random().nextDouble() * (upper - lower + 1));
    }

    private static int[] shuffleDeck(int[] deck) {
        for (int i = 1; i < deck.length; i++) {
            int k = perfectRNG(0, i);
            int temp = deck[i];
            deck[i] = deck[k];
            deck[k] = temp;
        }
        return deck;
    }

    private static int addBinary(int a, int b) {
        if (b == 0) return a;
        int sum = (a ^ b);
        int carry = (a & b) << 1;
        return addBinary(sum, carry);
    }

}
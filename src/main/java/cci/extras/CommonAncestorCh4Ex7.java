package cci.extras;

import utils.markers.Solution;

public class CommonAncestorCh4Ex7 {


  public static void main(String[] args) {
    Node root = new Node(20, new Node(3, new Node(6, new Node(11), null), new Node(7)), new Node(8));
    System.out.println(findCommonAncestor(root, new Node(11), new Node(7)));

  }

  @Solution
  public static Node findCommonAncestor(Node root, Node p, Node q) {
    while( root !=null ) {
      boolean pOnLeft = containsNode(root.left, p);
      boolean qOnLeft = containsNode(root.left, q);
      if(pOnLeft ^ qOnLeft) return root;
      if(pOnLeft){
        root = root.left;
      } else {
        root = root.right;
      }
    }
    return null;
  }

  public static boolean containsNode(Node root, Node p) {
    if (root == null) {
      return false;
    }
    if (p.getKey() == root.getKey()) {
      return true;
    }
    return containsNode(root.left, p) || containsNode(root.right, p);
  }


  public static class Node {

    private final int key;
    private final Node left;
    private final Node right;

    @Override
    public String toString() {
      return "Node{" +
              "key=" + key +
              '}';
    }

    public Node(int key, Node left, Node right) {
      this.key = key;
      this.left = left;
      this.right = right;
    }

    public Node(int key) {
      this(key, null, null);
    }


    public int getKey() {
      return key;
    }

    public Node getLeft() {
      return left;
    }

    public Node getRight() {
      return right;
    }
  }

}

package cci.extras;

public class FactorialTrailingZeros173 {

  public static void main(String[] args) {
    System.out.println(trailingZerosInFactorial(10)); //2
    System.out.println(trailingZerosInFactorial(12)); //2
    System.out.println(trailingZerosInFactorial(15)); //3
    System.out.println(trailingZerosInFactorial(20)); //4
    System.out.println(trailingZerosInFactorial(21)); //4
    System.out.println(trailingZerosInFactorial(33)); //7
    System.out.println(trailingZerosInFactorial(39)); //8
  }

  public static int trailingZerosInFactorial(int n) {
    int fullTensNumber = n / 10;
    // each full ten contains 2,5 and 10 so it generates two trailing zeros
    int result = fullTensNumber * 2;
    // extra zeros coming from double, triple 5s in 25, 125, 625...
    int temp = n;
    int fives = 0;
    while(temp > 0){
      temp/=5;
      fives++;
    }
    result+= fives - 2;

    if (n % 10 >= 5) {
      return result + 1;
    } else {
      return result;
    }
  }

}

package cci.extras;

public class TicTacToe172 {

  public static void main(String[] args) {
    int[][] game = new int[][]{
            {0, 1, 1},
            {1, 0, 0},
            {1, 0, 0}};
    System.out.println(solve(game));
  }

  public static int solve(int[][] game) {
    for (int i = 0; i < 3; i++) {
      int sumInRow = 0;
      int sumInColumn = 0;
      for (int j = 0; j < 3; j++) {
        sumInRow += game[i][j];
        sumInColumn += game[j][i];
      }
      if (sumInRow == 0 || sumInColumn == 0) return 0;
      else if (sumInRow == 3 || sumInColumn == 3) return 1;
    }
    int diagonalDecreasing = game[0][0] + game[1][1] + game[2][2];
    int diagonalIncreasing = game[2][0] + game[1][1] + game[0][2];
    if (diagonalDecreasing == 0 || diagonalIncreasing == 0) return 0;
    else if (diagonalDecreasing == 3 || diagonalIncreasing == 3) return 1;
    return -1;
  }


}

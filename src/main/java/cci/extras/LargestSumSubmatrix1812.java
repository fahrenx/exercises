package cci.extras;

public class LargestSumSubmatrix1812 {

  public static void main(String[] args) {
    int[][] m = new int[][]{
            {-1, 2, 3},
            {2, -3, 4},
            {-4, 5, 6}};

    long largestSum = findLargestSum(m, 3);
    System.out.println(largestSum);
  }

  public static long findLargestSum(int[][] m, int n) {
    long max = Long.MIN_VALUE;
    for (int startRow = 0; startRow < n; startRow++) {
      int[] partialForColumns = new int[n]; //optimization
      for (int endRow = startRow; endRow < n; endRow++) {
        for (int i = 0; i < n; i++) {
          partialForColumns[i] += m[endRow][i];
        }
        long sum = findOptimalColumnsAndSum(partialForColumns);
        max = Math.max(max, sum);
        System.out.println(String.format("%d, %d, max: %d", startRow, endRow, max));
      }
    }
    return max;
  }

  private static long findOptimalColumnsAndSum(int[] partial) {
    long max = Long.MIN_VALUE;
    long current = 0;
    for (int i = 0; i < partial.length; i++) {
      current = Math.max(0, current + partial[i]);
      max = Math.max(max, current);
    }
    return max;
  }

}

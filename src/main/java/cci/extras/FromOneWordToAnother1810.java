package cci.extras;

import com.google.common.collect.ImmutableSet;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;

public class FromOneWordToAnother1810 {

  public static void main(String[] args) {
    test();
    test2();
  }

  private static void test2() {
    Set<String> dict = ImmutableSet.of("damp", "lamp", "limp", "lime", "like");
    List<String> result = transform("damp", "like", dict);
    assertThat(result).containsExactly("damp", "lamp", "limp", "lime", "like");
  }

  private static void test() {
    Set<String> dict = ImmutableSet.of("car", "can", "cab", "win", "tin", "tan");
    List<String> result = transform("car", "tin", dict);
    assertThat(result).containsExactly("car", "can", "tan", "tin");
  }

  public static List<String> transform(String from, String to, Set<String> dict) {
    Queue<String> queue = new LinkedList<>();
    queue.add(from);
    Set<String> visited = new HashSet<>();
    Map<String, String> parents = new HashMap<>();
    visited.add(from);
    while (!queue.isEmpty()) {
      String currentWord = queue.poll();
      System.out.println(currentWord);
      if (currentWord.equals(to)) {
        System.out.println("Return!");
        List<String> results = getPath(from, parents, currentWord);
        return results;
      }
      for (String adjWord : getWordsOneEditAway(currentWord, dict)) {
        if (!visited.contains(adjWord)) {
          parents.put(adjWord, currentWord);
          queue.add(adjWord);
          visited.add(adjWord);
        }
      }
    }
    return null;
  }

  private static List<String> getPath(String from, Map<String, String> parents, String currentWord) {
    List<String> results = new LinkedList<>();
    results.add(currentWord);
    String element = currentWord;
    do {
      element = parents.get(element);
      results.add(element);
    } while (!element.equals(from));
    Collections.reverse(results);
    return results;
  }

  public static List<String> getWordsOneEditAway(String word, Set<String> dict) {
    List<String> result = new LinkedList<>();
    for (int i = 0; i < word.length(); i++) {
      StringBuilder sb = new StringBuilder(word);
      for (int j = 0; j <= 'z' - 'a'; j++) {
        char newChar = (char) ((int) 'a' + j);
        if (newChar != word.charAt(i)) {
          sb.setCharAt(i, newChar);
          if (dict.contains(sb.toString())) {
            result.add(sb.toString());
          }
        }
      }
    }
    return result;
  }


}

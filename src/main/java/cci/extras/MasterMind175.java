package cci.extras;

import java.util.HashSet;
import java.util.Set;

public class MasterMind175 {

  public static void main(String[] args) {
    System.out.println(getResult("RGBY", "GGRR"));
  }

  /**
   *  assumption that can only be single pseudohit for a given colour - if not then frequencies array[charCodeShifted]->int needed
   *  fixed size
   */
  public static MMResult getResult(String solution, String guess) {
    char[] s = solution.toCharArray();
    char[] g = guess.toCharArray();

    int hits = 0;
    Set<Character> sCharsSet = new HashSet<Character>();
    for (int i = 0; i < 4; i++) {
      if(s[i] == g[i]){
        hits++;
      } else {
        sCharsSet.add(s[i]);
      }
    }
    int pseudoHits = 0;
    for (int i = 0; i <4; i++) {
      if (s[i] != g[i] && sCharsSet.contains(g[i])) {
          pseudoHits++;
          sCharsSet.remove(g[i]);
      }
    }
    return new MMResult(hits, pseudoHits);
  }


  public static class MMResult {

    public final int hits;
    public final int pseudoHits;

    public int getHits() {
      return hits;
    }

    public int getPseudoHits() {
      return pseudoHits;
    }

    @Override
    public String toString() {
      return "MMResult{" +
              "hits=" + hits +
              ", pseudoHits=" + pseudoHits +
              '}';
    }

    public MMResult(int hits, int pseudoHits) {
      this.hits = hits;
      this.pseudoHits = pseudoHits;
    }
  }

}

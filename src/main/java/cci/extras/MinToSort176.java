package cci.extras;

import java.util.Arrays;


/**
 * lessons learnt:
 *  - one off errors! but it wasn't that bad!
 */
public class MinToSort176 {

  public static void main(String[] args) {

    test2();
  }

  private static void test2() {
    int[] input = {1, 2, 4, 7, 10, 11, 7, 12, 6, 7, 16, 18, 19};
    int[] ab = findIndexesToSort(input);
    System.out.println(Arrays.toString(ab));
  }

  private static void test1() {
    int[] input = {2, 4, 6, 5, 8, 13, 11, 12};
    int[] ab = findIndexesToSort(input);
    System.out.println(Arrays.toString(ab));
  }

  private static int[] findIndexesToSort(int[] a) {
    int i = 0;
    while (i < a.length - 1 && a[i] < a[i + 1]) {
      i++;
    }
    if (i == a.length) {
      return new int[]{-1, -1};
    }
    int j = a.length - 1;
    while (j > 0 && a[j] > a[j - 1]) {
      j--;
    }
    // array is sorted from 0 to i inclusive and from j to end (also inclusive), let's check the middle of the array
    System.out.println(String.format("i %d j %d" , i, j));

    // let's find min and max element in mid section of the array
    int min = Integer.MAX_VALUE;
    int max = Integer.MIN_VALUE;
    for (int k = i + 1; k < j; k++) {
      min = Math.min(min, a[k]);
      max = Math.max(max, a[k]);
    }
    System.out.println(String.format("min %d max %d" , min, max));
    // now shrinking 0 to i and j to end given max and min in mid section

    int x = i;
    while (x >= 1 && a[x - 1] >= min) {
      x--;
    }
    // now x points to left end of area that must be sorted

    int y = j;
    while (y < a.length - 1 && a[y + 1] <= max) {
      y++;
    }
    // now y points to left end of area that must be sorted

    return new int[]{x, y};
  }

}

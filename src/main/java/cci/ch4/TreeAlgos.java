package cci.ch4;

import java.util.LinkedList;
import java.util.List;

public class TreeAlgos {

    private static class Node {
        final int val;
        Node left;
        Node right;
        Node parent;

        Node(int val) {
            this.val = val;
        }

        @Override
        public String toString() {
            return "[" + val + "]";
        }
    }


    public static void main(String[] args) {
        int[] a = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        inOrder(minBST(a, 0, a.length - 1));
        List<List<Node>> lists = treeToList(minBST(a, 0, a.length - 1));
        for (List<Node> ls : lists) {
            System.out.println(ls);
        }
        Node root = successor();
        commonAncestor(root, 13, 17);
    }

    // TODO
    private static Node commonAncestor(Node root, int a, int b) {
        return null;


    }

    private static Node successor(){
        // a tree I drew
        Node n10 = new Node(10);
        Node n5 = new Node(5);
        Node n2 = new Node(2);
        Node n7 = new Node(7);
        Node n15 = new Node(15);
        Node n13 = new Node(13);
        Node n17 = new Node(17);
        n10.left = n5;
        n10.right = n15;
        n5.parent = n10;
        n5.left = n2;
        n5.right = n7;
        n2.parent = n5;
        n7.parent = n5;
        n15.left = n13;
        n15.right = n17;
        n13.parent = n15;
        n17.parent = n15;

        System.out.println(inOrderSuccessor(n2));
        return n10;
    }

    private static void inOrder(Node node) {
        if (node == null) return;
        if (node.left != null) {
            inOrder(node.left);
        }
        System.out.println(node.val);
        if (node.right != null) {
            inOrder(node.right);
        }
    }

    private static Node minBST(int[] a, int start, int end) {
        if (start > end) return null;
        int mid = start + (end - start) / 2;
        Node n = new Node(a[mid]);
        n.left = minBST(a, start, mid - 1);
        n.right = minBST(a, mid + 1, end);
        return n;
    }

    private static List<List<Node>> treeToList(Node n) {
        List<List<Node>> ls = new LinkedList<>();
        treeToListRec(n, ls, 0);
        return ls;
    }

    private static Node inOrderSuccessor(Node node) {
        Node next = node.right;
        if (node.right != null) { // left-most in right subtree
            next = node.right;
            while (next.left != null) {
                next = next.left;
            }
        } else { // first predecessor/parent on the right
            Node temp = node.parent;
            while (temp != null) {
                if (temp.val > node.val) return temp;
                temp = temp.parent;
            }
            return null;
        }
        return next;

    }

    private static void treeToListRec(Node n, List<List<Node>> ls, int height) {
        if (ls.size() < height + 1) {
            ls.add(new LinkedList<>());
        }
        ls.get(height).add(n);
        if (n.left != null) {
            treeToListRec(n.left, ls, height + 1);
        }
        if (n.right != null) {
            treeToListRec(n.right, ls, height + 1);
        }
    }

}

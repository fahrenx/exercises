package cci.ch4;

import java.util.*;

public class GraphAlgos {

    public static class Graph {
        private Map<Integer, Node> nodes;
        Map<Integer, List<Node>> adjacency;

        public Graph(List<Integer> nodes) {
            this.nodes = new HashMap<>();
            this.adjacency = new HashMap<>();
            for (int v : nodes) {
                Node n = new Node(v, this);
                this.nodes.put(n.val, n);
                this.adjacency.put(n.val, new ArrayList<>());
            }
        }

        public void addEdge(int u, int v) {
            adjacency.get(u).add(nodes.get(v));
            adjacency.get(v).add(nodes.get(u));
        }

        public Node get(int val) {
            return nodes.get(val);
        }

        public Collection<Node> allNodes() {
            return new ArrayList<>(nodes.values());
        }
    }

    public static class Node {
        public int val;
        private Graph g;

        public Node(int val, Graph g) {
            this.val = val;
            this.g = g;
        }

        private List<Node> adjacentNodes() {
            return g.adjacency.get(val);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Node node = (Node) o;
            return val == node.val;
        }

        @Override
        public int hashCode() {
            return Objects.hash(val);
        }

        @Override
        public String toString() {
            return "[" + val + "]";
        }
    }

    public static void main(String[] args) {
        Graph graph = new Graph(Arrays.asList(1, 2, 3, 4, 5));
        graph.addEdge(1, 2);
        graph.addEdge(1, 3);
        graph.addEdge(2, 4);

        System.out.println(areConnectedDFS(graph, graph.get(1), graph.get(4)));
    }

    private static boolean areConnectedBFS(Graph graph, Node u, Node v) {
        Set<Node> visited = new HashSet<>();
        Queue<Node> q = new LinkedList<>();
        q.add(u);
        while (!q.isEmpty()) {
            Node n = q.poll();
            System.out.println("Visiting " + n);
            for (Node adj : n.adjacentNodes()) {
                if (!visited.contains(adj)) {
                    if (adj == v) return true;
                    q.add(adj);
                }
            }
            visited.add(n);
        }
        return false;
    }


    private static boolean areConnectedDFS(Graph graph, Node u, Node v) {
        Set<Node> visited = new HashSet<>();
        return visit(u, visited, v);
    }

    private static boolean visit(Node u, Set<Node> visited, Node target) {
        System.out.println("Visiting " + u);
        visited.add(u);
        for (Node n : u.adjacentNodes()) {
            if (n == target) return true;
            if (!visited.contains(n)) {
                boolean found = visit(n, visited, target);
                if (found) return true;
            }
        }
        return false;
    }
}

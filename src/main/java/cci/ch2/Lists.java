package cci.ch2;

import org.assertj.core.api.Assertions;

import java.util.Arrays;
import java.util.List;
import java.util.Stack;

public class Lists {

    private static class ListNode{
        ListNode next;
        int val;

        ListNode(int val){
            this.val = val;
        }
    }

    public static void main(String[] args) {
        testPalndromes();
        printList(sumNumbers(ls(Arrays.asList(4)), ls(Arrays.asList(1))));

    }

    private static void printList(ListNode node) {
        System.out.println("LIST:");
        while(node !=null){
            System.out.println(node.val);
            node = node.next;
        }
    }


    public static ListNode sumNumbers(ListNode a, ListNode b){
        ListNode head = null;
        ListNode result = null;
        ListNode aTemp = a;
        ListNode bTemp = b;
        int carry = 0;
        while(aTemp != null && bTemp != null){
            int sum = aTemp.val  + bTemp.val + carry;
            if(sum >= 10) {
                carry = sum/10;
                sum -= carry * 10;
            }
            ListNode newNode = new ListNode(sum);
            if(result == null) {
                result = newNode;
                head = result;
            } else {
                result.next = newNode;
                result = result.next;
            }
            aTemp = aTemp.next;
            bTemp = bTemp.next;
        }
        ListNode temp = null;
        if(aTemp == null) {
            temp = bTemp;
        } else {
            temp = aTemp;
        }

        while(temp != null){
                int sum = temp.val + carry;
                if(sum >= 10) {
                    carry = sum/10;
                    sum -= carry * 10;
                }
                ListNode newNode = new ListNode(sum);
                if(result == null) {
                    result = newNode;
                    head = result;
                } else {
                    result.next = newNode;
                    result = result.next;
                }
                temp = temp.next;
        }
        if(carry!=0){
            result.next = new ListNode(carry);
        }

        return head;
    }


    private static void testPalndromes() {
        Assertions.assertThat(isPalindromeReverseAndCompare(ls(Arrays.asList(1, 2, 3, 2, 1)))).isTrue();
        Assertions.assertThat(isPalindromeReverseAndCompare(ls(Arrays.asList(1, 2, 3, 2, 3)))).isFalse();
        Assertions.assertThat(isPalindromeReverseAndCompare(ls(Arrays.asList(1)))).isTrue();
        Assertions.assertThat(isPalindromeReverseAndCompare(ls(Arrays.asList(1, 2)))).isFalse();
        Assertions.assertThat(isPalindromeReverseAndCompare(ls(Arrays.asList()))).isTrue();

        Assertions.assertThat(isPalindromeWithStack(ls(Arrays.asList(1, 2, 3, 2, 1)))).isTrue();
        Assertions.assertThat(isPalindromeWithStack(ls(Arrays.asList(1, 2, 3, 2, 3)))).isFalse();
        Assertions.assertThat(isPalindromeWithStack(ls(Arrays.asList(1)))).isTrue();
        Assertions.assertThat(isPalindromeWithStack(ls(Arrays.asList(1, 2)))).isFalse();
        Assertions.assertThat(isPalindromeWithStack(ls(Arrays.asList()))).isTrue();


        Assertions.assertThat(isPalindromeRecursion(ls(Arrays.asList(1, 2, 3, 2, 1)))).isTrue();
        Assertions.assertThat(isPalindromeRecursion(ls(Arrays.asList(1, 2, 3, 2, 3)))).isFalse();
        Assertions.assertThat(isPalindromeRecursion(ls(Arrays.asList(1)))).isTrue();
        Assertions.assertThat(isPalindromeRecursion(ls(Arrays.asList(1, 2)))).isFalse();
        Assertions.assertThat(isPalindromeRecursion(ls(Arrays.asList()))).isTrue();
    }


    private static class Result {
        boolean success;
        ListNode node;

        public Result(boolean success, ListNode node){
            this.success = success;
            this.node = node;
        }
    }

    private static boolean isPalindromeRecursion(ListNode head) {
        int size = 0;
        ListNode temp = head;
        while(temp != null){
            size++;
            temp = temp.next;
        }
        return isPalindromeRec(head, size).success;
    }


    private static Result isPalindromeRec(ListNode head, int length){
        if(head  == null  || length == 0) {
            return new Result(true, null);
        }
        if(length == 1) {
            return new Result(true, head.next); // head.next this is where we cross the middle
        }
        if(length == 2) {
            return new Result(head.val == head.next.val, head.next.next); // middle case, we cross the middle and also do comparison
        }

        Result result = isPalindromeRec(head.next, length - 2);

        if(!result.success) return new Result(false, null); // passing the failure up the stack

        // the actual comparision
        // the trick is here, we now can compare current head with results fo calls above which always contain nodes to the right from the middle
        if(head.val != result.node.val) return new Result(false, null); // mismatch, aborting
        // everything matched
        else return new Result(true, result.node.next); // passing up the stack the value from right to the one we just compared to
    }

    private static boolean isPalindromeWithStack(ListNode head) {
        ListNode temp = head;
        int size = 0;
        // instead I could use a trick of fast and slow pointer
        while(temp != null){
            size++;
            temp = temp.next;
        }
        if(size <= 1) return true;
        System.out.println(size);
        // initialisation
        boolean odd = (size % 2 == 1);
        int limit = size/2;
        ListNode revTemp = head;
        Stack<Integer> stack = new Stack<Integer>();
        // putting first half of the list on stack
        while(limit > 0){
            stack.push(revTemp.val);
            revTemp = revTemp.next;
            limit--;
        }
        // comparing reversed first half (aTemp pointer) with other half (bTemp pointer)
        ListNode bTemp = revTemp;
        if(odd) bTemp = bTemp.next; // skipping the element in the middle if odd number of elements
        while(!stack.isEmpty() && bTemp!=null){
            if(stack.pop()!= bTemp.val) return false;
            bTemp = bTemp.next; // moves to the end
        }
        return (stack.isEmpty() && bTemp == null);
    }

    private static boolean isPalindromeReverseAndCompare(ListNode head) {
        // checking the size
        ListNode temp = head;
        int size = 0;
        while(temp != null){
            size++;
            temp = temp.next;
        }
        if(size <= 1) return true;
        System.out.println(size);
        // initialisation
        boolean odd = (size % 2 == 1);
        int limit = size/2;
        ListNode revTemp = head;
        ListNode revPrev = null;
        // reversing first half of the list
        while(limit > 0){
            // imagine 1->2->3-> (1 revPrev, 2 revTemp, 3 newTemp), after this 1<-2 3->... (2 prev, 3 revTemp)
            ListNode newTemp = revTemp.next;
            revTemp.next = revPrev; // the actual reversing of the list
            revPrev = revTemp;
            revTemp = newTemp;
            limit--;
        }
        // comparing reversed first half (aTemp pointer) with other half (bTemp pointer)
        ListNode aTemp = revPrev;
        ListNode bTemp = revTemp;
        if(odd) bTemp = bTemp.next; // skipping the element in the middle if odd number of elements
        System.out.println(aTemp.val + ":" + bTemp.val);

        while(aTemp !=null && bTemp!=null){
            if(aTemp.val != bTemp.val) return false;
            aTemp = aTemp.next; // moves to the head
            bTemp = bTemp.next; // moves to the end
        }
        return true;
    }

    private static ListNode ls(List<Integer> ls) {
        ListNode head = null;
        ListNode prev = null;
        for(int i: ls){
            ListNode n = new ListNode(i);
            if(head ==null){
                head = n;
            } else {
                prev.next = n;
            }
            prev = n;
        }
        return head;
    }
}

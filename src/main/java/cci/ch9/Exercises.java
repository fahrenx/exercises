package cci.ch9;

import java.util.*;

import static org.assertj.core.api.Assertions.assertThat;


class Point{
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Point point = (Point) o;
        return x == point.x &&
                y == point.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    final int x;
    final int y;

    public Point(int x, int y){
        this.x = x;
        this.y = y;
    }
}

public class Exercises {

    private static int counter = 0;

    /**
     * find magic
     */
    public static void mainMagic(String[] args) {
        assertThat(findMagicLinear(new int[]{1,5,8, 10})).isFalse();
        assertThat(findMagicLinear(new int[]{-10, 0, 1, 2, 4})).isTrue();
        assertThat(findMagicLinear(new int[]{-10, 3, 3, 3})).isTrue();
        assertThat(findMagicLinear(new int[]{-10, 5, 5, 6})).isFalse();
    }

    public static boolean findMagicLinear(int[] a){
        int i=0;
        while(i < a.length){
            System.out.println(i);
            if(a[i] == i) return true;
            if(a[i] - i > a.length - i) return false;
            i++;
        }
        return false;
    }

    /**
     * robot
     */
    public static void mainRobot(String[] args) {
        assertThat(robotRec(0, 0, 2,2)).isEqualTo(2);
        assertThat(robotDp(2,2)).isEqualTo(2);

        Set<Point> blocked = new HashSet<>();
        blocked.add(new Point(1,1));
        blocked.add(new Point(0,1));
        blocked.add(new Point(2,1));

        assertThat(findPath(2,2, blocked, new ArrayList<>(), new HashMap<>())).isTrue();
    }

    public static boolean findPath(int i, int j, Set<Point> blocked, List<Point> path, Map<Point, Boolean> cache){
        if(i==0 && j==0) return true;
        else if(i<0 || j<0) return false;
        Point p = new Point(i, j);

        // checking if already visited
        Boolean cached = cache.get(p);
        if(cached != null){
            return cached;
        }

        boolean pathFound = false;
        if(!blocked.contains(new Point(i-1, j))) {
            pathFound = findPath(i - 1, j, blocked, path, cache); // go left
        }
        if(!pathFound && !blocked.contains(new Point(i, j-1))) {
            pathFound = findPath(i, j - 1, blocked, path, cache); // go up
        }
        if(pathFound) path.add(p);
        cache.put(p, pathFound);
        return pathFound;
    }


    public static int robotRec(int i, int j, int x, int y){
        System.out.println(++counter);
        if(i == y || j == x) return 0;
        if(i == y-1 && j == x-1) return 1;
        return robotRec(i+1, j, x, y) // going down
               + robotRec(i, j+1, x, y); // going left
    }

    public static int robotDp(int x, int y){
        int[][] dp = new int[2][x];
        for(int i=0; i < x; i++){
            dp[0][i] = 1;
        }
        for(int i=0; i < y-1; i++){
            dp[1][0] = dp[0][0];
            for(int j=1; j < x; j++) { // increase counters
                dp[1][j] = dp[1][j-1] + dp[0][j]; // sum of options from left and upper cell
            }
            for(int j=0; j < x; j++){
                dp[0][j] = dp[1][j]; // rewrite the rows
            }
        }
        return dp[1][x-1];
    }


    /**
     * STAIRS
     */
    public static void mainStairs(String[] args) {
        int n = 15;
        assertThat(stairs(n)).isEqualTo(5768);

        int[] cache = new int[n+1];
        for(int i=0; i<cache.length; i++) cache[i] = -1;
        counter = 0;
        assertThat(stairs2(n, cache)).isEqualTo(5768);
        assertThat(stairsDPNoArray(n)).isEqualTo(5768);

    }

    private static int stairsDPNoArray(int n) {
        int a = 1;
        int b = 0;
        int c = 0;
        int d = 0;
        for (int i = 0; i < n; i++) {
            b+=a;
            c+=a;
            d+=a;
            a = b;
            b = c;
            c = d;
            d = 0;
        }
        return a;
    }

    private static int stairsDP(int n){
        int[] dp = new int[n+3];
        dp[0] = 1;
        for(int i=0; i < n;i++){
            int curr = dp[i];
            dp[i+1] += curr;
            dp[i+2] += curr;
            dp[i+3] += curr;
        }
        return dp[n];
    }

    private static int stairs2(int n, int cache[]) {
        System.out.println(++counter);
        if(n == 0){
            return 1;
        }
        if(n < 0){
            return 0;
        }
        if(cache[n] != -1) return cache[n];
        int res = stairs2(n-1, cache) + stairs2(n-2, cache) + stairs2(n-3, cache);
        cache[n] = res;
        return res;
    }

    private static int stairs(int n) {
        System.out.println(++counter);
        if(n == 0){
            return 1;
        }
        if(n < 0){
            return 0;
        }
        return stairs(n-1) + stairs(n-2) + stairs(n-3);
    }


}

package cci.dpandrecursion;

import com.google.common.collect.ImmutableList;
import utils.markers.Recursion;
import utils.markers.Solution;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;


/**
 *
 *  Lessons learnt:
 *   - base case and build
 */
@Recursion
public class AllSubsetsCh9E4 {

    public static void main(String[] args) {
        ImmutableList<Integer> inputSet = ImmutableList.of(1, 2, 3);
        Collection<List<Integer>> subsets = allSubsets(inputSet);
        for (List<Integer> set : subsets ){
            System.out.println(set);
        }
        System.out.println("Number of subsets: "+subsets.size() + " (expected number : " + Math.pow(2, inputSet.size()) + " ) ");
    }

    /**
     * complexity of 2"n anyway as this is the number of subsets of set of size n
     */
    @Solution
    public static <E> Collection<List<E>> allSubsets(List<E> set){
        return allSubsets(set, 0);
    }

    private static <E> Collection<List<E>> allSubsets(List<E> elements, int index) {
        if(elements.size() == index){
            List<List<E>> subsets = new ArrayList<>();
            subsets.add(new ArrayList<>()); // empty set
            return subsets;
        } else {
            Collection<List<E>> sets = allSubsets(elements, index + 1);
            List<List<E>> allSubsets = new ArrayList<>();
            allSubsets.addAll(sets);
            E element = elements.get(index);
            for (List<E> set : sets){
                List<E> newSet = new ArrayList<>();
                newSet.addAll(set);
                set.add(element);
                allSubsets.add(newSet);
            }
            return allSubsets;
        }
    }

}




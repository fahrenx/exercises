package cci.dpandrecursion;

import utils.markers.Recursion;
import utils.markers.Solution;

/**
 * lessons learnt:
 * - brute force can be useful as a oracle , source of correct answers so it is good to start with it
 * - when you see sorted numbers you should immediately think about binary search
 * - watch out for one-off errors while doing binary search and similar splits, it is good to have catch-all-weirdness if at the beginning of
 *   function to be on the safe side, also be consistent about inclusive/exclusive range boundaries and remember about requirement of progress
 *   in recursion calls
 *
 */
@Recursion
public class MagicIndexCH9E3 {

    public static void main(String[] args) {
        System.out.println(magicIndexDistinctBinary(new int[]{-100, -22, 2}));
        System.out.println(magicIndexDistinctBinary(new int[]{0}));
        System.out.println(magicIndexDistinctBinary(new int[]{-100, -22, 1, 3}));
        System.out.println(magicIndexDistinctBinary(new int[]{-100, 200, 200, 200}));

        System.out.println(magicIndexNotDistinct(new int[]{-100, 3, 3, 3}));
        System.out.println(magicIndexNotDistinctBinary(new int[]{-100, 3, 3, 3}));


        System.out.println(magicIndexNotDistinct(new int[]{-100, 3, 5, 5, 5, 5}));
        System.out.println(magicIndexNotDistinctBinary(new int[]{-100, 3, 5, 5, 5, 5}));
        System.out.println(magicIndexNotDistinctBinary(new int[]{-100, 1, 1, 5, 100, 120}));


    }

    @Solution
    private static int magicIndexNotDistinct(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == i) {
                return i;
            } else if (arr[i] > i && (arr.length - i - 1) < (arr[i] - i)) { // optimization to stop search early
                return -1;
            }
        }
        return -1;
    }

    @Solution
    private static int magicIndexDistinct(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == i) {
                return i;
            } else if (arr[i] > i) {
                return -1;
            }
        }
        return -1;
    }

    /**
     * most tricky - binary-search-like solution on non-distinct values
     */
    @Solution
    private static int magicIndexNotDistinctBinary(int[] arr){
        return magicIndexNotDistinctBinary(arr, 0, arr.length - 1);
    }

    private static int magicIndexNotDistinctBinary(int[] arr, int start, int end) {
        if (end < start || start<0 || end > arr.length-1) {
            return -1;
        }
        if (start == end) {
            return (arr[start] == start) ? start : -1;
        }
        int centerIndex = start + (end - start) / 2;
        int centerValue = arr[centerIndex];
        if (centerIndex == centerValue) {
            return centerIndex;
        }
        // until now code above is the same like for the binary search on distinct values
        // we need to check both left and right side
        if(centerValue > centerIndex){
            // from left
            int resultFromLeft = magicIndexNotDistinctBinary(arr, start, centerIndex-1);
            // from right - first (centerValue-centerIndex) elements on the right will never be magic index so let's skip them
            int resultFromRight = magicIndexNotDistinctBinary(arr, start+ (centerValue-centerIndex), end);
            return Math.max(resultFromLeft, resultFromRight);
        } else {
            // from left
            int resultFromLeft = magicIndexNotDistinctBinary(arr, start, centerIndex-(centerIndex-centerValue));
            // from right - first (centerValue-centerIndex) elements on the right will never be magic index so let's skip them
            int resultFromRight = magicIndexNotDistinctBinary(arr, centerIndex+1, end);
            return Math.max(resultFromLeft, resultFromRight);
        }
    }

    /**
     * solution based on binary-search - assumption that all elements are distinct
     */
    @Solution
    private static int magicIndexDistinctBinary(int[] arr){
        return magicIndexDistinctBinary(arr, 0, arr.length-1);
    }

    private static int magicIndexDistinctBinary(int[] arr, int start, int end) {
        if (end < start) {
            return -1;
        }
        if (start == end) {
            return (arr[start] == start) ? start : -1;
        }

        int center = start + (end - start) / 2;
        if (arr[center] == center) {
            return center;
        } else if (arr[center] > center) {
            return magicIndexDistinctBinary(arr, start, center-1);
        }
        return magicIndexDistinctBinary(arr, center+1, end);
    }


}

package cci.dpandrecursion;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import utils.Performance;

import java.io.IOException;
import java.util.Arrays;

/**
 * lessons learnt:
 * - recursive fibonacci is embarrassingly slow
 * - dp solution is often very small modification on top of recursive one, so start with recursive and find DP optimization
 */
public class FibonacciDP {


    public static void main(String[] args) throws IOException, RunnerException {
        Options options = Performance.defaultJmhOptions(FibonacciDP.class);
        new Runner(options).run();
    }

    @Benchmark
    public void performanceTest(Blackhole bh) {
        bh.consume(fibonacciDP(1000));
    }

    public static void main2(String[] args) {
        int n = 50;
        System.out.println("it  :" + fibonacciIterative(n));
        System.out.println("dp  :" + fibonacciDP(n));
        System.out.println("rec :" + fibonacciRec(n));
    }

    private static long fibonacciDP(int n) {
        long[] results = new long[n + 1];
        Arrays.fill(results, -1);
        return fibonacciDP(n, results);
    }

    /**
     * timewise O(n) like iterative but much higher space complexity and constant factor
     * tp1000 : 75633 avg (20x slower)
     * tp100:1019586 avg
     * tp50: 2393018 avg (over 10^2 times slower than iterative)
     * tp20: 7861556 avg (35x slower than iterative)
     */
    public static long fibonacciDP(int n, long[] results) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        } else if (results[n] != -1) {
            return results[n];
        }
        results[n] = fibonacciDP(n - 1, results) + fibonacciDP(n - 2, results);
        return results[n];
    }

    /**
     * O(2^n)
     * tp50: N/A
     * tp20: 19314 avg
     */
    private static long fibonacciRec(int n) {
        if (n == 0) {
            return 0;
        } else if (n == 1) {
            return 1;
        }
        return fibonacciRec(n - 1) + fibonacciRec(n - 2);
    }





    /**
     * O(n)
     * tp1000: 1527054 avg
     * tp100:16162627 avg
     * tp50: 298034208 avg
     * tp20: 280416561 avg
     */
    private static long fibonacciIterative(int n) {
        if (n <= 1) {
            return n;
        }
        long prev = 0;
        long current = 1;
        for (int i = 2; i <= n; i++) {
            current = current + prev;
            prev = current - prev;
        }
        return current;
    }


}

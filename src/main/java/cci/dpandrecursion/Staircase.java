package cci.dpandrecursion;

import utils.markers.DynamicProgramming;
import utils.markers.Recursion;

import java.util.Arrays;

/**
 * 9.1
 * lessons learnt:
 * - complexity of recursion algorithms can be huge
 * - overflow starting as early as n = 37
 * - dynamic programming can be very effective in reducing time complexity and also easy to implement
 */
@Recursion
@DynamicProgramming
public class Staircase {


    public static void main(String[] args) {
        System.out.println(runningUpStairs(500));
    }

    public static long runningUpStairs(int n){
        long[] partialResults = new long[n+1];
        Arrays.fill(partialResults, -1);
        return runningUpStairs(n, partialResults);
    }

    /**
     * Time complexity O(3^n) if no caching of results and O(n) complexity when caching. Memory complexity actually in both cases as O(n)
     * as recursive calls are also very space efficient so cost of additional array for caching result is offset by savings in recursive calls
     */
    public static long runningUpStairs(int stepsAhead, long[] partialResults) {
        if (stepsAhead < 0) {
            return 0;
        } else if (stepsAhead <= 1) {
            return 1;
        } else if (partialResults[stepsAhead] != -1) {
            return partialResults[stepsAhead];
        }
        partialResults[stepsAhead] = runningUpStairs(stepsAhead - 1, partialResults) + runningUpStairs(stepsAhead - 2, partialResults) + runningUpStairs(stepsAhead - 3, partialResults);
        return partialResults[stepsAhead];
    }


}
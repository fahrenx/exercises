package cci.dpandrecursion;

import com.google.common.base.Preconditions;
import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Maps;
import utils.markers.Combinatorics;
import utils.markers.DynamicProgramming;
import utils.markers.Recursion;

import java.util.*;


/**
 * lessons learnt:
 * - very nice idea of representing path problem as combinatorics formula
 * - use you math knowledge, especially combinatorics - analytical solution can be much faster and super simple like in this case
 * - be careful with overflow - especially if you are around exponential series and factorials
 * - if you have long lists of arguments consider some instance fields etc.
 * - be careful with Collections.empty or Collection.singleton as they are unmodifiable and will  throw exceptions on add etc.
 * - when working with recursion visualize tree of calls and think about stack unrolling! without that it is just a gut feeling or wishful thinking
 * - 3 things - stop condition, progress and good accumulation of results
 * - keep it core skeleton small to be able to understand it , deleage boilerplate to helper methods if necessary
 * - solve simple sollution on paper or use sout on simple case to understand better your algorithm
 */
@DynamicProgramming
@Combinatorics
@Recursion
public class RobotCH9E2 {

    public static void main(String[] args) {
//        System.out.println(countPathsCombinatorics(11, 55, 0, 0));
//        System.out.println(countPathsDp(11, 55, 0, 0));
        List<List<Point>> paths = pathsOutsideOfSpots(10, 10, 0, 0, ImmutableSet.of());
        for (List<Point> path : paths) {
            System.out.println("path: " + path);
        }
        System.out.println("number of paths "+paths.size());

        System.out.println(countPathsRec(10, 10, 0, 0));
    }

    /**
     * Nice!
     * slight modification comparing to their solution - mine is returning more information and uses different value structures
     */
    public static List<List<Point>> pathsOutsideOfSpots(int x, int y, int xPos, int yPos, Set<Point> spots) {
        Map<Point, List<List<Point>>> partialResults = Maps.newHashMap();
        return pathsOutsideOfSpots2(x, y, xPos, yPos, spots, partialResults);
    }

    private static List<List<Point>> pathsOutsideOfSpots2(int x, int y, int posX, int posY, Set<Point> spots, Map<Point, List<List<Point>>> partialResults) {
        Point currentPoint = new Point(posX, posY);
        if (posX == x && posY == y) {
            return listsOfLists(currentPoint);
        } else if (posX > x || posY > y || spots.contains(currentPoint)) {
            return null;
        } else if (partialResults.containsKey(currentPoint)) {
            if (partialResults.get(currentPoint) == null) {
                return null;
            }
            return partialResults.get(currentPoint);
        }
        List<List<Point>> pathsFromPointDown = pathsOutsideOfSpots2(x, y, posX, posY + 1, spots, partialResults);
        List<List<Point>> pathsFromPointRight = pathsOutsideOfSpots2(x, y, posX + 1, posY, spots, partialResults);

        List<List<Point>> pathsFromCurrentPoint = new ArrayList<>();
        if (pathsFromPointDown != null) {
            pathsFromCurrentPoint.addAll(pathsFromPointDown);
        }
        if (pathsFromPointRight != null) {
            pathsFromCurrentPoint.addAll(pathsFromPointRight);
        }
        if (pathsFromPointRight == null && pathsFromPointDown == null) {
            partialResults.put(currentPoint, null);
            return null;
        }
        List<List<Point>> pathsStartingAtCurrentPoint = concatLists(list(currentPoint), pathsFromCurrentPoint);
        partialResults.put(currentPoint, pathsStartingAtCurrentPoint);
        return pathsStartingAtCurrentPoint;
    }

    private static List<Point> list(Point point) {
        ArrayList<Point> points = new ArrayList();
        points.add(point);
        return points;
    }

    private static List<List<Point>> listsOfLists(final Point currentPoint) {
        List<List<Point>> results = new ArrayList();
        ArrayList<Point> points = new ArrayList<Point>() {{
            add(currentPoint);
        }};
        results.add(points);
        return results;
    }


    private static <T> List<List<T>> concatLists(List<T> start, List<List<T>> listsToConcat) {
        List<List<T>> results = new ArrayList<>(listsToConcat.size());
        for (List<T> ending : listsToConcat) {
            LinkedList<T> newList = new LinkedList<>(start);
            newList.addAll(ending);
            results.add(newList);
        }
        return results;
    }

    /**
     * fast but problem with overflow
     */
    public static long countPathsCombinatorics(int x, int y, int xPos, int yPos) {
        return combinations(x + y, x);
    }

    private static long combinations(int n, int k) {
        Preconditions.checkArgument(n >= k);
        long nDivByN_k = 1;
        for (int i = n - k + 1; i <= n; i++) {
            nDivByN_k *= i;
            System.out.println(nDivByN_k);
        }
        long factorialK = 1;
        for (int i = 1; i <= k; i++) {
            factorialK *= i;
        }
        return nDivByN_k / factorialK;
    }

    /**
     * O(2^n)
     */
    public static long countPathsRec(int x, int y, int posX, int posY) {
        if (posX == x && posY == y) {
            return 1;
        } else if (posX > x || posY > y) {
            return 0;
        }
        return countPathsRec(x, y, posX, posY + 1) + countPathsRec(x, y, posX + 1, posY);
    }

    /**
     * O(n)
     */
    public static long countPathsDp(int x, int y, int posX, int posY) {
        long[][] results = new long[x + 1][y + 1];
        for (int i = 0; i <= x; i++) {
            Arrays.fill(results[i], -1);
        }
        return countPathsDp(x, y, posX, posY, results);
    }

    public static long countPathsDp(int x, int y, int posX, int posY, long[][] results) {
        if (posX == x && posY == y) {
            return 1;
        } else if (posX > x || posY > y) {
            return 0;
        } else if (results[posX][posY] != -1) {
            return results[posX][posY];
        }
        results[posX][posY] = countPathsDp(x, y, posX, posY + 1, results) + countPathsDp(x, y, posX + 1, posY, results);
        return results[posX][posY];
    }

    public static class Point {


        private final int x;
        private final int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            Point point = (Point) o;

            if (x != point.x) return false;
            if (y != point.y) return false;

            return true;
        }

        @Override
        public int hashCode() {
            int result = x;
            result = 31 * result + y;
            return result;
        }

        @Override
        public String toString() {
            return "Point{" +
                    "x=" + x +
                    ", y=" + y +
                    '}';
        }
    }

}

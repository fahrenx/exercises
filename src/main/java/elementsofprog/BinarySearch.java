package elementsofprog;

import java.util.Arrays;

import static org.junit.Assert.*;

public class BinarySearch {


  public static void main(String[] args) {
    System.out.println(Arrays.asList(1,2,2));

    assertEquals(binarySearch(new int[]{1, 3, 4, 5}, 3), 1);
    assertEquals(binarySearch(new int[]{1, 2, 3, 4}, 4), 3);
    assertEquals(binarySearch(new int[]{0, 2, 3, 4}, 0), 0);

    assertEquals(binarySearch(new int[]{1, 3, 4, 5, 6}, 4), 2);
    assertEquals(binarySearch(new int[]{0, 3, 3, 5, 8}, 0), 0);
    assertEquals(binarySearch(new int[]{0, 3, 3, 5, 8}, 7), -1);
    assertEquals(binarySearch(new int[]{0, 3, 3, 5, 8}, 11), -1);
    assertEquals(binarySearch(new int[]{0, 3, 3, 5, 8}, -4), -1);
    assertEquals(binarySearch(new int[]{0}, 0), 0);
    assertEquals(binarySearch(new int[]{0}, 3), -1);
    assertEquals(binarySearch(new int[]{1, 2}, 2), 1);
    assertEquals(binarySearch(new int[]{1, 2}, 1), 0);
    assertEquals(binarySearch(new int[]{1, 2}, 3), -1);
    assertEquals(binarySearch(new int[]{1, 2}, 0), -1);

    // leftmost
    assertEquals(binarySearchLeftmost(new int[]{1, 3, 4, 5, 6}, 4), 2);
    assertEquals(binarySearchLeftmost(new int[]{0, 3, 3, 5, 8}, 0), 0);
    assertEquals(binarySearchLeftmost(new int[]{0, 3, 3, 5, 8}, 11), -1);
    assertEquals(binarySearchLeftmost(new int[]{1, 2, 2, 2, 3, 3, 4}, 2), 1);
    assertEquals(binarySearchLeftmost(new int[]{1, 1, 3, 5, 9}, 1), 0);
    assertEquals(binarySearchLeftmost(new int[]{1, 2, 3, 5, 6, 6}, 6), 4);
    assertEquals(binarySearchLeftmost(new int[]{1, 2, 2}, 2), 1);
  }

  /**
   * returns index of element n in array or -1 if it is not in array
   */
  private static int binarySearch(int[] a, int n) {
    int left = 0;
    int right = a.length - 1;
    while (left <= right) {
      int mid = left + (right - left) / 2; // this to avoid overflow possible when you do just (left+right)/2
      if (a[mid] == n) {
        return mid;
      } else if (a[mid] > n) {
        right = mid - 1;
      } else {
        left = mid + 1;
      }
    }
    return -1;
  }

  /**
   * returns leftmost (i.e. first occurrence) index of element n in array or -1 if it is not in array
   */
  private static int binarySearchLeftmost(int[] a, int n) {
    int left = 0;
    int right = a.length - 1;
    while (left <= right) {
      int mid = left + (right - left) / 2;
      if (a[mid] == n) {
        if (left == right) return mid;
        else right = mid; // prune right side, and try to search from mid(including) to the left
      } else if (a[mid] > n) {
        right = mid - 1;
      } else {
        left = mid + 1;
      }
    }
    return -1;
  }


}

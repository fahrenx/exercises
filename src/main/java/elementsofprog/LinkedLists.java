package elementsofprog;

import org.assertj.core.api.Assertions;
import org.assertj.core.data.Offset;
import tcollections.TLinkedList;

import java.util.Optional;

public class LinkedLists {

  private static <T> TLinkedList<T> ls(T... elements) {
    TLinkedList<T> list = new TLinkedList<>();
    for (T e : elements) {
      list.add(e);
    }
    return list;
  }

  private static <T> void prettyPrint(TLinkedList.Node<T> head) {
    System.out.println(new TLinkedList<T>(head).prettyPrint());
  }

  private static <T> void prettyPrint(TLinkedList.Node<T> head, int limit) {
    System.out.println(new TLinkedList<T>(head, 100).prettyPrint(limit));
  }


  public static void main(String[] args) {
    testMerge();
    testCycles();
    testMedian();
    testFirstCommon1();
    testFirstCommon2();
    testFirstCommon3();
  }

  private static void testFirstCommon1(){
    TLinkedList.Node<Integer> nodeA1 = new TLinkedList.Node<>(10);
    TLinkedList.Node<Integer> nodeA2 = new TLinkedList.Node<>(20);
    TLinkedList.Node<Integer> nodeA3 = new TLinkedList.Node<>(21);
    TLinkedList.Node<Integer> nodeB1 = new TLinkedList.Node<>(20);
    TLinkedList.Node<Integer> nodeB2 = new TLinkedList.Node<>(20);

    nodeA1.setNext(nodeA2).setNext(nodeA3);
    nodeB1.setNext(nodeB2).setNext(nodeA2).setNext(nodeA3);

    Assertions.assertThat(findFirstCommon(nodeA1, nodeB1)).isEqualTo(nodeA2);
  }

  private static void testFirstCommon2(){
    TLinkedList.Node<Integer> nodeA1 = new TLinkedList.Node<>(10);
    TLinkedList.Node<Integer> nodeA2 = new TLinkedList.Node<>(20);
    TLinkedList.Node<Integer> nodeB1 = new TLinkedList.Node<>(20);
    TLinkedList.Node<Integer> nodeB2 = new TLinkedList.Node<>(20);
    TLinkedList.Node<Integer> nodeC3 = new TLinkedList.Node<>(21);

    nodeA1.setNext(nodeA2).setNext(nodeC3);
    nodeB1.setNext(nodeB2).setNext(nodeC3);

    Assertions.assertThat(findFirstCommon(nodeA1, nodeB1)).isEqualTo(nodeC3);
  }

  private static void testFirstCommon3(){
    TLinkedList.Node<Integer> nodeA1 = new TLinkedList.Node<>(10);
    TLinkedList.Node<Integer> nodeB1 = new TLinkedList.Node<>(20);

    Assertions.assertThat(findFirstCommon(nodeA1, nodeB1)).isEqualTo(null);
  }


  private static void testMedian() {
    testMedian1();
    testMedian2();
    testMedian3();
    testMedian4();
  }

  private static void testMedian1() {
    TLinkedList.Node<Integer> node1 = new TLinkedList.Node<>(10);
    TLinkedList.Node<Integer> node2 = new TLinkedList.Node<>(20);
    TLinkedList.Node<Integer> node3 = new TLinkedList.Node<>(21);
    TLinkedList.Node<Integer> node4 = new TLinkedList.Node<>(40);
    node1.setNext(node2).setNext(node3).setNext(node4).setNext(node1);
    prettyPrint(node1, 8);
    double median = findMedianInCircularList(node2);
    System.out.println(" Median: " + median);
    Assertions.assertThat(median).isCloseTo(20.5, Offset.offset(0.1));
  }

  private static void testMedian2() {
    TLinkedList.Node<Integer> node1 = new TLinkedList.Node<>(10);
    TLinkedList.Node<Integer> node2 = new TLinkedList.Node<>(10);
    node1.setNext(node2).setNext(node1);
    prettyPrint(node1, 8);
    double median = findMedianInCircularList(node2);
    System.out.println(" Median: " + median);
    Assertions.assertThat(median).isCloseTo(10.0, Offset.offset(0.1));
  }

  private static void testMedian3() {
    TLinkedList.Node<Integer> node1 = new TLinkedList.Node<>(10);
    TLinkedList.Node<Integer> node2 = new TLinkedList.Node<>(20);
    TLinkedList.Node<Integer> node3 = new TLinkedList.Node<>(30);
    node1.setNext(node2).setNext(node3).setNext(node1);
    prettyPrint(node2, 8);
    double median = findMedianInCircularList(node2);
    System.out.println(" Median: " + median);
    Assertions.assertThat(median).isCloseTo(20.0, Offset.offset(0.1));
  }

  private static void testMedian4() {
    TLinkedList.Node<Integer> node1 = new TLinkedList.Node<>(20);
    TLinkedList.Node<Integer> node2 = new TLinkedList.Node<>(20);
    TLinkedList.Node<Integer> node3 = new TLinkedList.Node<>(25);
    TLinkedList.Node<Integer> node4 = new TLinkedList.Node<>(30);
    node1.setNext(node2).setNext(node3).setNext(node4).setNext(node1);
    prettyPrint(node2, 8);
    double median = findMedianInCircularList(node2);
    System.out.println(" Median: " + median);
    Assertions.assertThat(median).isCloseTo(22.5, Offset.offset(0.1));
  }


  private static void testMerge() {
    prettyPrint(merge(ls(1, 2, 6).headNode(), ls(2, 3, 7).headNode()));
    prettyPrint(merge(ls(5).headNode(), ls(2, 3, 7).headNode()));
    prettyPrint(merge(LinkedLists.<Integer>ls().headNode(), ls(2, 3, 7).headNode()));
    prettyPrint(merge(ls(2, 3, 7).headNode(), LinkedLists.<Integer>ls().headNode()));
    prettyPrint(merge(LinkedLists.<Integer>ls().headNode(), LinkedLists.<Integer>ls().headNode()));
  }

  private static void testCycles() {
    testCycle1();
    testCycle2();
    testCycle3();
    testCycle4();
    testCycle5();
    testCycle6();
  }

  private static void testCycle1() {
    TLinkedList.Node<Integer> node1 = new TLinkedList.Node<>(1);
    TLinkedList.Node<Integer> node2 = new TLinkedList.Node<>(2);
    TLinkedList.Node<Integer> node3 = new TLinkedList.Node<>(3);
    TLinkedList.Node<Integer> node4 = new TLinkedList.Node<>(4);
    node1.setNext(node2).setNext(node3).setNext(node4).setNext(node1);
    prettyPrint(node1, 20);
    System.out.println("cycle :  " + findCycle(node1));
  }

  private static void testCycle2() {
    TLinkedList.Node<Integer> node1 = new TLinkedList.Node<>(1);
    TLinkedList.Node<Integer> node2 = new TLinkedList.Node<>(2);
    TLinkedList.Node<Integer> node3 = new TLinkedList.Node<>(3);
    node1.setNext(node2).setNext(node3).setNext(node2);
    prettyPrint(node1, 20);
    System.out.println("cycle :  " + findCycle(node1));
  }

  private static void testCycle3() {
    TLinkedList.Node<Integer> node1 = new TLinkedList.Node<>(1);
    TLinkedList.Node<Integer> node2 = new TLinkedList.Node<>(2);
    TLinkedList.Node<Integer> node3 = new TLinkedList.Node<>(3);
    node1.setNext(node2).setNext(node3);
    prettyPrint(node1, 20);
    System.out.println("cycle :  " + findCycle(node1));
  }

  private static void testCycle4() {
    TLinkedList.Node<Integer> node1 = new TLinkedList.Node<>(1);
    prettyPrint(node1, 20);
    System.out.println("cycle :  " + findCycle(node1));
  }

  private static void testCycle5() {
    TLinkedList.Node<Integer> node1 = new TLinkedList.Node<>(1);
    node1.setNext(node1);
    prettyPrint(node1, 20);
    System.out.println("cycle :  " + findCycle(node1));
  }

  private static void testCycle6() {
    TLinkedList.Node<Integer> node1 = new TLinkedList.Node<>(1);
    TLinkedList.Node<Integer> node2 = new TLinkedList.Node<>(2);
    TLinkedList.Node<Integer> node3 = new TLinkedList.Node<>(3);
    TLinkedList.Node<Integer> node4 = new TLinkedList.Node<>(4);
    TLinkedList.Node<Integer> node5 = new TLinkedList.Node<>(5);
    TLinkedList.Node<Integer> node6 = new TLinkedList.Node<>(6);
    node1.setNext(node2).setNext(node3).setNext(node4).setNext(node5).setNext(node6).setNext(node3);
    prettyPrint(node1, 20);
    System.out.println("cycle :  " + findCycle(node1));
  }

  /**
   * 7.1
   * comment: implementation changes depending if you want to have independent copy of merged lists
   * or you are ok with change in one of the list being reflected in merged list. But but but - exercise made it
   * quite clear!
   * poms: 3
   */
  static TLinkedList.Node<Integer> merge(TLinkedList.Node<Integer> a, TLinkedList.Node<Integer> b) {
    if (a == null && b == null) return null;
    TLinkedList.Node<Integer> currA = a;
    TLinkedList.Node<Integer> currB = b;
    TLinkedList.Node<Integer> currResult = null;

    if (currA == null) {
      return currB;
    } else if (currB == null) {
      return currA;
    } else if (currA.value() < currB.value()) {
      currResult = currA;
      currA = currA.next();
    } else {
      currResult = currB;
      currB = currB.next();
    }
    TLinkedList.Node<Integer> result = currResult;
    while (currA != null && currB != null) {
      if (currA.value() < currB.value()) {
        currResult.setNext(new TLinkedList.Node<>(currA.value()));
        currResult = currResult.next();
        currA = currA.next();
      } else {
        currResult.setNext(new TLinkedList.Node<>(currB.value()));
        currResult = currResult.next();
        currB = currB.next();
      }
    }
    if (currA != null) {
      currResult.setNext(currA);
    }
    if (currB != null) {
      currResult.setNext(currB);
    }
    return result;
  }

  /**
   * 7.2
   * comments: glad I didn't even explore memory intensive solution, good approach wth fast and slow pointer
   * but didn't know how to finish that one off (got beginning of cycle not just answer if has a cycle). I should
   * have draw more and play with variables. Key observation is that I can easily calculate length of cycle and
   * I can change speed of counters.
   * Also good thing I found the formula for calculating position which was good, just should have tried better as I mentioned
   * with playing with variables and trying a bit harder.
   * <p>
   * poms: 3
   */
  static <T> TLinkedList.Node<T> findCycle(TLinkedList.Node<T> head) {
    if (head == null) return head;
    TLinkedList.Node<T> slow = head;
    TLinkedList.Node<T> fast = head;
    // finding cycle
    boolean cycleFound = false;
    while (!cycleFound && fast != null && fast.next() != null) {
      fast = fast.next().next();
      slow = slow.next();
      if (fast == slow) {
        cycleFound = true;
      }
    }
    if (!cycleFound) return null;
    // finding cycle's length
    int cycleLength = 1;
    slow = slow.next();
    while (slow != fast) {
      slow = slow.next();
      cycleLength++;
    }
    // finding first element of the cycle
    // reset and shift of one pointer
    fast = head;
    slow = head;
    for (int i = 0; i < cycleLength; i++) {
      fast = fast.next();
    }
    // let them meet again,,
    while (fast != slow) {
      slow = slow.next();
      fast = fast.next();
    }
    return fast;
  }

  /**
   * 7.3
   * <p>
   * comment:
   * - naming thing - circular does not mean "with cycles" - it means - the entire list is a cycle
   * - when in complicated loop - focus very hard, don't do stupid errors, check:
   * continuity, off-by-one, forgotten updates, branching logic, main predicate, variable names, really the crucial part!
   * - not sure if I could up with corner case of all elements being the same - that is clever!
   * - trivial things like method names, testing, formatting may distract your from hard thinking about the problem - so
   * try to do first thinking on paper, outside of code! draw it, think it through, write exmamples on paper
   * poms: 2
   */
  public static double findMedianInCircularList(TLinkedList.Node<Integer> node) {
    if (node == null) return 0.0;
    TLinkedList.Node<Integer> curr = node;
    // find start of the list
    boolean allAreTheSame = true;
    boolean startFound = false;
    int length = 1;
    while (!startFound && curr.next() != node) {
      if (curr.next().value() < curr.value()) {
        allAreTheSame = false;
        startFound = true;
      } else if (curr.next().value() > curr.value()) {
        allAreTheSame = false;
        curr = curr.next();
      } else {
        curr = curr.next();
      }
      length++;
    }
    if (allAreTheSame) {
      return node.value();
    }
    TLinkedList.Node<Integer> start = curr.next();
    int shift = length / 2 - 1;
    while (shift-- > 0) {
      start = start.next();
    }
    if (length % 2 == 0) {
      System.out.println("even");
      return (start.value() + start.next().value()) / 2.0;
    } else {
      return start.next().value();
    }
  }

  /**
   * 7.4
   * comment
   * - if you consider problem on paper, you are not only have a chance of getting the right insight,
   * but also when writing it down in code, thanks to that separation of thinking step and writing step,
   * thoughts are better memorised and more likely being checked/implemented/considered when writing a code comparing
   * to brainstorm and chaotic mass of thought that is created when doing coding and deep thinking together
   * - I like elegant (similar to establishing invariant in code) always-true, non-obvious observations like:
   *  - if lists end with different node they have no element common or:
   *  - once lists converge, they cannot diverge again
   * Apart from sounding nice and simple, they can simplify thinking (e.g. removing certain branches/possibilities) as well as
   * prompt you to certain train of thoughts and towards certain solution. They are like precious signpost born from good thinking
   * that once discovered should be carefully analysed as they are most likely lead to the right solution.
   *
   * poms: 2
   *
   */
  public static <T> TLinkedList.Node<T> findFirstCommon(TLinkedList.Node<T> h1, TLinkedList.Node<T> h2) {
    if (h2 == null || h1 == null) return null;
    TLinkedList.Node<T> curr1 = h1;
    TLinkedList.Node<T> curr2 = h2;
    int length1 = 1;
    int length2 = 1;
    while (curr1.next() != null || curr2.next() != null) {
      if (curr1 == curr2) {
        return curr1;
      }
      if(curr1.next() != null) {
        curr1 = curr1.next();
        length1++;
      }
      if(curr2.next() != null) {
        curr2 = curr2.next();
        length2++;
      }
    }
    if (curr1 != curr2) {
      return null; // no common nodes
    }
    int diff = Math.abs(length2 - length1);
    if (diff == 0) return curr1; // should never happen as it would be caught in loop above, but just in case...
    curr1 = h1;
    curr2 = h2;
    if (length1 > length2) {
      curr1 = getNth(h1, diff);
    } else {
      curr2 = getNth(h2, diff);
    }
    while(curr1 != curr2){
      curr1 = curr1.next();
      curr2 = curr2.next();
    }
    return curr1;
  }


  /**
   * 7.5
   * comments:
   * - good high level considerations and reuse of 7.4 (and clever observations - e.g. if only one has a cycle
   * then they have no common element)
   * poms:
   */
  // TODO - it is mostly about reuse of previous that's why I skipped

  private static <T> TLinkedList.Node<T> getNth(TLinkedList.Node<T> h, int diff) {
    TLinkedList.Node<T> curr = h;
    while (diff > 0) {
      curr = curr.next();
      diff--;
    }
    return curr;
  }
}



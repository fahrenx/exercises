package elementsofprog;

import com.google.common.base.Preconditions;
import fundamental.BST;

import static org.assertj.core.api.Assertions.*;

import java.util.*;

class HanoiStack {

  private final LinkedList<Integer> internal = new LinkedList<Integer>();

  public void push(int a){
    Preconditions.checkArgument(internal.stream().noneMatch(el -> el < a), "Trying to push bigger element on top of smaller ones");
    internal.push(a);
  }

  public int pop(){
    return internal.pop();
  }

  public LinkedList<Integer> getInternal() {
    return internal;
  }
}

/**
 * Chapter 8 - stacks part
 */
public class Stacks {

  public static void main(String[] args) throws InterruptedException {
    testInOrderTraversalWithStack();
    testViewsOfTheSunset1();
    testViewsOfTheSunset2();
    testSortStack();
    testConvertToBasePath();
    // TODO    testHanoi();
  }


  public static void testConvertToBasePath() {
    assertThat(convertPathToBaseForm("ala/ma/kota")).isEqualTo("ala/ma/kota");
    assertThat(convertPathToBaseForm("ala/../kota")).isEqualTo("kota");
    assertThat(convertPathToBaseForm("../ma/kota")).isEqualTo("../ma/kota");

  }

  public static void testViewsOfTheSunset1() {
    // ordering in stack is sort of inverted comparing to append-only list- head of stack is first from left
    assertThat(viewsOfSunsetEastToWest(Arrays.asList(5,6,2,8,1))).isEqualTo(Arrays.asList(1,8));
    assertThat(viewsOfSunsetEastToWest(Arrays.asList(1,9,5,4,5))).isEqualTo(Arrays.asList(5,9));
    assertThat(viewsOfSunsetEastToWest(Arrays.asList(1,2,3,4,5))).isEqualTo(Collections.singletonList(5));
    assertThat(viewsOfSunsetEastToWest(Arrays.asList(3,2,1))).isEqualTo(Arrays.asList(1,2,3));
    assertThat(viewsOfSunsetEastToWest(new LinkedList<>())).isEmpty();
  }

  public static void testViewsOfTheSunset2() {
    assertThat(viewsOfSunsetWestToEast(Arrays.asList(5,6,2,8,1))).isEqualTo(Arrays.asList(8,6,5));
    assertThat(viewsOfSunsetWestToEast(Arrays.asList(1,9,5,4,5))).isEqualTo(Arrays.asList(9,1));
    assertThat(viewsOfSunsetWestToEast(Arrays.asList(1,2,3,4,5))).isEqualTo(Arrays.asList(5,4,3,2,1));
    assertThat(viewsOfSunsetWestToEast(Arrays.asList(3,2,1))).isEqualTo(Arrays.asList(3));
    assertThat(viewsOfSunsetWestToEast(new LinkedList<>())).isEmpty();
  }

  public static void testSortStack() {
    assertThat(sortStack(new LinkedList<>(Arrays.asList(8,4,5,2,6,9,3)))).isEqualTo(Arrays.asList(9, 8, 6, 5, 4, 3, 2));
    assertThat(sortStack(new LinkedList<>(Arrays.asList()))).isEmpty();
    assertThat(sortStack(new LinkedList<>(Arrays.asList(1,2,3)))).isEqualTo(Arrays.asList(3,2,1));
    assertThat(sortStack(new LinkedList<>(Arrays.asList(1,1,8, 5)))).isEqualTo(Arrays.asList(8, 5,1,1));
    assertThat(sortStack(new LinkedList<>(Arrays.asList(1)))).isEqualTo(Collections.singletonList(1));
  }

  private static void testInOrderTraversalWithStack() throws InterruptedException {
    BST tree = BST.create(20);
    tree.add(15);
    tree.add(17);
    tree.add(7);
    tree.add(1);
    tree.add(9);
    tree.add(16);
    tree.add(18);
    tree.add(25);
    tree.add(22);
    assertThat(printBstInOrderWithStack(tree.root)).isEqualTo(Arrays.asList(1, 7, 9, 15, 16, 17, 18, 20, 22, 25));
  }

  /**
   * 8.3
   * comments:
   * - main problem is the conversion from recursive solution to iterative one using stack, it seemed
   * easy at first but then, given that method contains a few calls and its order is important so
   * I used extra 'visited' set to keep track of stage of execution of given function call
   *
   */
  public static List<Integer> printBstInOrderWithStack(BST.Node root) throws InterruptedException {
    LinkedList<BST.Node> stack = new LinkedList<>();
    stack.push(root);
    Set<Integer> visited = new HashSet<Integer>();
    LinkedList<Integer> result = new LinkedList<>();
    while (!stack.isEmpty()) {
      BST.Node curr = stack.pop();
      System.out.println("popped:" + curr.getKey());
      if (!visited.contains(curr.getKey()) && curr.getLeft() != null) {
        stack.push(curr);
        visited.add(curr.getKey());
        stack.push(curr.getLeft());
      } else {
        System.out.println(">" + curr.getKey());
        result.add(curr.getKey());
        if (curr.getRight() != null) {
          stack.push(curr.getRight());
        }
      }
    }
    return result;
  }



  /**
   * 8.4
   * comments:
   * - struggling with enabling right type of thinking for this
   *
   * poms: 3
   */
  private static void testHanoi() {
    // TODO - can't find the good way of thinking about this!
    HanoiStack p1 = new HanoiStack();
    for (int i = 5; i > 0; i--) {
      p1.push(i);
    }
    HanoiStack p2 = new HanoiStack();
    HanoiStack p3 = new HanoiStack();

    p3.push(p1.pop());
    p2.push(p1.pop());
    p2.push(p3.pop());

    assertThat(p3.getInternal()).hasSameElementsAs(Arrays.asList(1,2,3,4,5));
  }


  /**
   * 8.6
   * comments:
   * - nice under-the-shower thinking, solved there
   * - quick
   * poms: 1
   */
  public static List<Integer> viewsOfSunsetEastToWest(List<Integer> heights) {
    LinkedList<Integer> withViews = new LinkedList<>();
    for(int i: heights){
      while(!withViews.isEmpty() && withViews.peek() <= i){
        withViews.pop();
      }
      withViews.push(i);
    }
    return withViews;
  }

  /**
   * 8.6.1 - variation - opposite direction - easy
   * poms: 1
   */
  public static List<Integer> viewsOfSunsetWestToEast(List<Integer> heights) {
    LinkedList<Integer> withViews = new LinkedList<>();
    for(int i: heights){
      if(withViews.isEmpty() || withViews.peek() < i){
        withViews.push(i);
      }
    }
    return withViews;
  }


  /**
   * 8.7
   * comments:
   *  - using stack when extra memory is not allowed is sneaky, but well
   *  - very interesting example of heavy usage of recursion to achieve the effect while keeping things
   *  very concise, even more interestingly - recursion on 2 levels - TODO study it carefully on paper
   *  - unlikely I would do it without looking at answers - my gap here - but once saw the answer I "got it"
   * poms: 2
   */
  public static LinkedList<Integer> sortStack(LinkedList<Integer> stack){
    if(!stack.isEmpty()) {
      int el = stack.pop();
      sortStack(stack);
      insert(stack, el);
    }
    return stack;
  }

  private static void insert(LinkedList<Integer> stack, int el) {
    if(stack.isEmpty() || stack.peek() <= el){
      stack.push(el);
    } else {
      int popped = stack.pop();
      insert(stack, el);
      stack.push(popped);
    }
  }

  /**
   * 8.8
   * comments:
   * - first draft which works on basic examples done very fast (after thinking in tube and shower)
   * - always think about edge cases TODO add tests for edge cases
   * poms: 2
   */
  public static String convertPathToBaseForm(String path){
    String[] elements = path.split("\\/");
    LinkedList<String> stack = new LinkedList<>();
    for(String e: elements){
      if(e.equals(".")){ //current dir
        // ignore
      } else if (e.equals("..")){ // parent dir
        if(!stack.isEmpty()) {
          stack.pop();
        } else {
          stack.push(e);
        }
      } else { // dir name
        stack.push(e);
      }
    }
    StringJoiner pathJoiner = new StringJoiner("/");
    Collections.reverse(stack); // TODO better?
    while(!stack.isEmpty()){
      pathJoiner.add(stack.pop());
    }
    return pathJoiner.toString();
  }
}

package elementsofprog;

import com.google.common.base.Preconditions;

import java.util.*;

// p96
public class Mnemonics {

  private static final Map<Integer, String[]> keypad = new HashMap<Integer, String[]>() {{
    put(1, new String[]{"1"});
    put(2, new String[]{"A", "B", "C"});
    put(3, new String[]{"D", "E", "F"});
    put(4, new String[]{"G", "H", "I"});
    put(5, new String[]{"J", "K", "L"});
    put(6, new String[]{"M", "N", "O"});
    put(7, new String[]{"P", "Q", "R", "S"});
    put(8, new String[]{"T", "U", "V"});
    put(9, new String[]{"W", "X", "Y", "Z"});
    put(0, new String[]{"0"});
  }};


  public static void main(String[] args) {
    Preconditions.checkArgument(mnemonics("0223").size() == 1*3*3*3);
    Preconditions.checkArgument(mnemonics("3778").size() == 3*4*4*3);
    Preconditions.checkArgument(mnemonics("01").size() == 1*1);
    Preconditions.checkArgument(mnemonics("0").size() == 1);
    Preconditions.checkArgument(mnemonics("1").size() == 1);
    Preconditions.checkArgument(mnemonics("").size() == 1);
  }

  private static List<String> mnemonics(String phoneNumber) {
    List<String> mnemonics = new LinkedList<>();
    mnemonicsRun(phoneNumber, 0, "", mnemonics);
    return mnemonics;
  }

  private static void mnemonicsRun(String phoneNumber, int position, String partialMnemonic, List<String> mnemonics) {
    if (position == phoneNumber.length()) {
      mnemonics.add(partialMnemonic);
    } else {
      int currNumber = Integer.valueOf(Character.toString(phoneNumber.charAt(position)));
      for (String c : keypad.get(currNumber)) {
        mnemonicsRun(phoneNumber, position + 1, partialMnemonic + c, mnemonics);
      }
    }
  }

}

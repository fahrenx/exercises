package elementsofprog;

import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkArgument;

//p98
public class RomanToDecimal {

  private static Map<Character, Integer> mapping = new HashMap<Character, Integer>() {{
    put('I', 1);
    put('V', 5);
    put('X', 10);
    put('L', 50);
    put('C', 100);
    put('D', 500);
    put('M', 1000);
  }};

  public static void main(String[] args) {
    checkArgument(romanToDec("V") == 5);
    checkArgument(romanToDec("I") == 1);
    checkArgument(romanToDec("II") == 2);
    checkArgument(romanToDec("IV") == 4);
    checkArgument(romanToDec("XVIII") == 18);
    checkArgument(romanToDec("XXXV") == 35);
    checkArgument(romanToDec("XLV") == 45);
    checkArgument(romanToDec("XXXXIIIII") == 45);
    checkArgument(romanToDec("VIII") == 8);
    checkArgument(romanToDec("XXXIV") == 34);
    checkArgument(romanToDec("XL") == 40);
    checkArgument(romanToDec("CMLXVI") == 966);
  }

  private static void isValidRoman(String num){



  }

  private static int romanToDec(String num) {
    System.out.println("IN:" + num);
    char[] numArr = num.toCharArray();
    int last = -1;
    int sum = 0;
    for (int i = numArr.length - 1; i >= 0; i--) {
      int curr = mapping.get(numArr[i]);
      if (curr < last) {
        sum -= curr;
      } else {
        sum += curr;
      }
      last = curr;
    }
    System.out.println("OUT:" + sum);
    return sum;
  }
}

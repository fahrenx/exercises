package elementsofprog;

import fundamental.BinaryTree;

import static org.assertj.core.api.Assertions.*;

import java.util.*;

/**
 * Chapter 8 continuation - queues part
 */
public class Queues {


  public static void main(String[] args) {
    testTreeTraversalBFS();
    testQueueOnArray();
  }

  private static void testTreeTraversalBFS() {
    BinaryTree tree = new BinaryTree(314);
    // TODO some nifty ways of building trees? this is cumbersome, maybe layer by layer?
    BinaryTree.Node leftSubtreeRoot = tree.root.addLeft(6);
    leftSubtreeRoot.addLeft(new BinaryTree.Node(271, 28, 0));
    leftSubtreeRoot.addRight(561).addRight(3).addLeft(17);

    BinaryTree.Node rightSubtreeRoot = tree.root.addRight(6);
    rightSubtreeRoot.addRight(271).addRight(28);
    rightSubtreeRoot.addLeft(2).addRight(new BinaryTree.Node(1, 401, 257)).getLeft().addRight(641);

    List<Integer> nodes = treeTraversalBFS(tree.root);

    assertThat(nodes).isEqualTo(Arrays.asList(314, 6, 6, 271, 561, 2, 271, 28, 0, 3, 1, 28, 17, 401, 257, 641));
  }

  /**
   * 8.9
   * comments:
   * - relatively easy but maybe a matter of practice and solving it too many times
   * - observations that this is BFS was crucial - the power of reusing known patterns
   * <p>
   * poms: 2
   */
  public static List<Integer> treeTraversalBFS(BinaryTree.Node root) {
    Queue<BinaryTree.Node> q = new LinkedList<>();
    q.add(root);
    LinkedList<Integer> results = new LinkedList<>();
    while (!q.isEmpty()) {
      BinaryTree.Node el = q.poll();
      System.out.println(el.key);
      results.add(el.key);
      if (el.left != null) q.add(el.left);
      if (el.right != null) q.add(el.right);
    }
    return results;
  }

  public static void testQueueOnArray(){
    QueueOnArray q = new QueueOnArray();
    for (int i = 0; i < 20; i++) {
      q.enqueue(i);
      assertThat(q.dequeue()).isEqualTo(i);
    }
    assertThat(q.isEmpty()).isTrue();
    q.clear();

    q.enqueue(1);
    q.enqueue(2);
    q.enqueue(3);
    assertThat(q.dequeue()).isEqualTo(1);
    assertThat(q.dequeue()).isEqualTo(2);
    assertThat(q.size()).isEqualTo(1);
    q.clear();


    for (int i = 0; i < 30; i++) {
      q.enqueue(i);
    }
    for (int i = 0; i < 10; i++) {
      q.dequeue();
    }
    assertThat(q.size()).isEqualTo(20);
    q.clear();
    // TODO test it further
  }

}


/**
 * 8.10
 *
 * comment:
 * - I like the concept of "computed" variable - term taken from Vue, but basically firstFreeindex() is here a computed variable
 * can be calculated every time, it is cheap, and it has clear dependency on other variable - I prefer that over maintaining it
 * as a separate variable that you need to remember to update very time - such approach simplifies things! you can
 * concentrate on a few key variable and rest is derived from them. If you decided that caching value is important, then you can always
 * change it and add it as a separate variable.
 * - all tests passed right away after writing this, pretty good
 * <p>
 * [0,1,2,...,n] - array
 * ^          ^
 * HEAD-----TAIL - queue
 */
// TODO shrinking
class QueueOnArray {

  private int[] a = new int[10];
  private int size = 0;
  private int head = 0;

  public void enqueue(int e) {
    expandIfNeeded();
    a[firstFreeIndex()] = e;
    size++;
  }

  public int dequeue() {
    if (isEmpty()) throw new NoSuchElementException("Queue is empty!");
    int res = a[head];
    size--;
    if (isEmpty()) { // queue has been emptied
      head = 0;
    } else {
      head++;
      shiftIfNeeded();
    }
    return res;
  }

  private void shiftIfNeeded() {
    if (head > 0.5 * a.length) {
      int j = 0;
      for (int i = head; i < head + size; i++) {
        a[j] = a[i];
        j++;
      }
      head = 0;
    }
  }

  private void expandIfNeeded() {
    if (size() > 0.75 * a.length) {
      int[] newArr = new int[a.length*2];
      int j=0;
      for (int i = head; i < head + size; i++) {
        newArr[j] = a[i];
        j++;
      }
      head = 0;
      a = newArr;
    }
  }

  public boolean isEmpty() {
    return size() == 0;
  }

  private int firstFreeIndex() {
    return head + size;
  }

  public int size() {
    return size;
  }

  public void clear(){
    a = new int[10];
    head = 0;
    size = 0;
  }
}


package utils;

public class CaterpillarMethod {

    public static boolean hasSumMine(int[] a, int n) {
        long sum;
        for (int i = 0; i < a.length; i++) {
            sum = a[i];
            if (a[i] == n) return true;
            for (int j = i + 1; j < a.length; j++) {
                sum += a[j];
                if (sum == n) {
                    return true;
                }
            }
        }
        return false;
    }

    public static boolean hasSumMineNonNegative(int[] a, int n) {
        long sum;
        for (int i = 0; i < a.length-1; i++) {
            sum = a[i];
            int j = i + 1;
            while (j < a.length && sum < n) {
                sum += a[j];
                j++;
            }
            if (sum == n) {
                return true;
            }
        }
        return false;
    }


    public static boolean hasSumNonNegative(int[] a, int n) {
        long sum = 0;
        int front = 0;
        for (int back = 0; back < a.length; back++) {
            // can I move front a bit further, is there room for that and can I do it without exceeding n?
            while (front < a.length && sum + a[front] <= n) {
                sum += a[front];
                front++;
            }
            if (sum == n) {
                return true;
            }
            sum-=a[back];
        }
        return false;
    }


}

package utils;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Optional;

public class Leaders {

    public static Optional<Integer> findLeaderBrute(int[] arr) {
        for (int i = 0; i < arr.length; i++) {
            int el = arr[i];
            int counter = 0;
            for (int j = 0; j < arr.length; j++) {
                if (arr[j] == el) {
                    counter++;
                }
            }
            if (counter > arr.length / 2) {
                return Optional.of(el);
            }
        }
        return Optional.empty();
    }

    public static Optional<Integer> findLeaderSort(int[] arr) {
        if (arr.length == 0) {
            return Optional.empty();
        }
        if (arr.length == 1) {
            return Optional.of(arr[0]);
        }
        int n = arr.length - 1;
        Arrays.sort(arr);
        // a bit of if conditions to save a full iteration, not sure if worth it
        if (arr.length % 2 == 1) {
            int midElement = arr[arr.length / 2];
            if (arr[0] == midElement || arr[n] == midElement) {
                return Optional.of(midElement);
            }
        } else {
            if (arr[0] == arr[arr.length / 2 + 1]) {
                return Optional.of(arr[0]);
            }

            if (arr[n] == arr[arr.length / 2 - 1]) {
                return Optional.of(arr[n]);
            }
        }
        return Optional.empty();
    }


    public static Optional<Integer> findLeaderLinear(int[] arr) {
        LinkedList<Integer> stack = new LinkedList<>();
        // TODO all values in stack are equal so we could just use two vars: size of stack and value
        for (int i = 0; i < arr.length; i++) {
            if (stack.isEmpty() || stack.peek() == arr[i]) {
                stack.push(arr[i]);
            } else {
                stack.pop();
            }
        }
        if (stack.isEmpty()) {
            return Optional.empty();
        }
        // we still need to check if value from stack is a leader
        int candidate = stack.pop();
        int counter = 0;
        for (int el : arr) {
            if (el == candidate) {
                counter++;
            }
        }
        return counter > arr.length / 2 ? Optional.of(candidate) : Optional.empty();
    }

}

package utils;

import java.util.Random;

public class Roulette {

    private static Random rand = new Random();

    public static int[] intArray(int length) {
        int[] arr = new int[length];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt();
        }
        return arr;
    }

    public static int[] intArray(int minLength, int maxLength, int minValue, int maxValue) {
        int[] arr = new int[randomIntInclusive(minLength, maxLength)];

        for (int i = 0; i < arr.length; i++) {
            arr[i] = rand.nextInt(randomIntInclusive(minValue, maxValue));
        }
        return arr;
    }

    public static int randomIntInclusive(int minValue, int maxValue) {
        return rand.nextInt(maxValue + 1 - minValue) + minValue;
    }
}

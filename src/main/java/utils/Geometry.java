package utils;

import java.awt.*;

public class Geometry {


    /**
     *            ------------(x22,y22)
     *            |                |
     * ---------------(x12,y12)    |
     * |          |      |         |
     * |         (x21,y21)----------
     * |                 |
     * (x11,y11)----------
     */
    public static long intersectionArea(int x11, int y11, int x12, int y12,
                                        int x21, int y21, int x22, int y22) {
        if(x21 > x12 || x22 < x11 || y21 > y12 || y22 < y11){ // no overlap
            return 0;
        }
        int xOverlap;
        if(x22 > x12) { // typical, second rectangle is to the right
            xOverlap = Math.max(0, x12-x21);
        } else {        // non-typical
            xOverlap = Math.max(0, x22-x11);
        }
        int shortestXLength = Math.min(x22-x21, x12-x11);   // case when one rectangle is inside another
        xOverlap = Math.min(xOverlap, shortestXLength);

        // TODO refactor to DRY?
        // complete symmetry
        int yOverlap;
        if(y22 > y12) {
            yOverlap = Math.max(0, y12-y21);
        } else {
            yOverlap = Math.max(0, y22-y11);
        }
        int shortestYLength = Math.min(y22 - y21, y12 - y11);
        yOverlap = Math.min(yOverlap, shortestYLength);     // case when one rectangle is inside another

        return xOverlap * yOverlap;
    }

    public static long intersectionArea(Point bottomLeft1, Point topRight1,
                                          Point bottomLeft2, Point topRight2) {
        return intersectionArea(bottomLeft1.x, bottomLeft1.y, topRight1.x, topRight1.y,
                bottomLeft2.x, bottomLeft2.y, topRight2.x, topRight2.y);
    }


}

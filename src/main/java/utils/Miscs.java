package utils;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class Miscs {

  public static void main(String[] args) {

    Set<Integer>[][] a = new Set[9][9];
    for(int i=0; i<a.length;i++){
      for(int j=0; j<a.length;j++){
        if(i!=1) a[i][j] = new HashSet<>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9));
      }
    }
    int b = 2;
    char v = (char)('0' + b);
    System.out.println(v);

  }

  public static String readFileAsString(String filename) throws IOException {
    byte[] encoded = Files.readAllBytes(Paths.get(filename));
    return new String(encoded);
  }

  public static String print2DArray(Object[] arr) {
    StringBuilder sb = new StringBuilder();

    for (int i = 0; i < arr.length; i++) {
      Class<?> clazz = arr[i].getClass();
      if (clazz == int[].class) {
        int[] a = (int[]) arr[i];
        for (int j = 0; j < a.length; j++) {
          sb.append(a[j]);
          if (j < a.length - 1) {
            sb.append(',');
          }
        }
      }
      if (clazz == char[].class) {
        int[] a = (int[]) arr[i];
        for (int j = 0; j < a.length; j++) {
          sb.append(a[j]);
          if (j < a.length - 1) {
            sb.append(',');
          }
        }
      }
      sb.append("\n");
    }
    return sb.toString();
  }

  public static List<String> splitStringIntoNChunks(String str, int n) {
    List<String> chunks = new ArrayList<>(n);
    List<Integer> chunkSizes = splitIntoNChunks(str.length(), n);
    System.out.println(chunkSizes);
    int start = 0;
    for (int i = 0; i < n; i++) {
      String stringChunk = str.substring(start, start + chunkSizes.get(i));
      chunks.add(stringChunk);
      start += chunkSizes.get(i);
    }
    return chunks;
  }

  private static List<Integer> splitIntoNChunks(int total, int n) {
    int div = total / n;
    int remainder = total % n;
    ArrayList<Integer> results = new ArrayList<>(n);
    for (int i = 0; i < n; i++) {
      if (i < remainder) {
        results.add(div + 1);
      } else {
        results.add(div);
      }
    }
    return results;
  }


}

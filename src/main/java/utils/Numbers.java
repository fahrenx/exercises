package utils;

import com.google.common.base.Preconditions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Numbers {

    public static int calculateNumberOfFactors(int n) {
        if (n == 1) return 1;
        long i = 0;
        int result = 2;
        while (i * i < n) {
            if (n % i == 0) {
                result += 2;
            }
            i++;
        }
        if (i * i == n) {
            result++;
        }

        return result;
    }

    public static List<Integer> calculateFactors(int n) {
        if (n == 1) return Collections.singletonList(n);
        List<Integer> results = new LinkedList<Integer>();
        results.add(1);
        results.add(n);
        int i = 0;
        while (i * i < n) {
            if (n % i == 0) {
                results.add(i);
                results.add(n / i);
            }
            i++;
        }
        if (i * i == n) {
            results.add(i);
        }
        return results;
    }

    /**
     * converts within 2-36 bases but only using digits, no letters
     */
    public static String changeBaseDigitsOnly(String number, int srcBase, int targetBase) {
        Preconditions.checkArgument(srcBase > 1 && srcBase <= 36 && targetBase > 1 && targetBase <= 36, "Only base withing [2-10] range can be used");
        if (number.equals("0") || srcBase == targetBase) {
            return number;
        }
        int dec;
        if (srcBase != 10) {
            throw new UnsupportedClassVersionError("Case where source base is different than 10 is not implemented yet!");
        } else {
            dec = Integer.parseInt(number);
        }
        List<Integer> digits = new ArrayList<>(32);
        while (dec > 0) {
            int digit = dec % targetBase; //it is a 'digit' in new base which actually may consist of two 0-9 digits
            digits.add(digit);
            dec /= targetBase;
        }
        // we need to reverse what we got
        StringBuilder sb = new StringBuilder();
        for (int i = digits.size() - 1; i >= 0; i--) {
            sb.append(digits.get(i));
        }
        return sb.toString();
    }


}

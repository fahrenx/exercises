package utils.trees;

public class NodeP {

    private final int key;
    private NodeP left;
    private NodeP right;
    private NodeP parent;

    public NodeP(int key, NodeP parent, NodeP left, NodeP right) {
        this.key = key;
        this.parent = parent;
        this.left = left;
        if(left!=null)
        this.left.parent = this;
        this.right = right;
        if(right!=null)
        this.right.parent = this;
    }

    public NodeP(int key, NodeP parent, int left, int right) {
        this(key, parent, new NodeP(left), new NodeP(right));
    }


    public NodeP(int key) {
        this(key, null, null, null);
    }

    public void left(NodeP node){
        this.left = node;
        node.parent = this;
    }

    public void right(NodeP node){
        this.right = node;
        node.parent = this;
    }

    public void parent(NodeP node){
        this.parent = node;
    }

    public int getKey() {
        return key;
    }

    public NodeP getLeft() {
        return left;
    }

    public NodeP getRight() {
        return right;
    }


    @Override
    public String toString() {
        return "NodeP{" +
                "key=" + key +
                '}';
    }

    public NodeP getParent() {
        return parent;
    }
}

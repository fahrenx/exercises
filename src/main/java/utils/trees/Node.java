package utils.trees;

public class Node {

    private final int key;
    private final Node left;
    private final Node right;

    public Node(int key, Node left, Node right) {
        this.key = key;
        this.left = left;
        this.right = right;
    }

    public Node(int key, int left, int right) {
        this.key = key;
        this.left = new Node(left);
        this.right = new Node(right);
    }


    public Node(int key) {
        this(key, null, null);
    }


    public int getKey() {
        return key;
    }

    public Node getLeft() {
        return left;
    }

    public Node getRight() {
        return right;
    }


    @Override
    public String toString() {
        return "Node{" +
                "key=" + key +
                '}';
    }

}

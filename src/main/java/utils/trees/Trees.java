package utils.trees;

import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * lessons learnth:
 * - space complexity of recursive in order traversal is O(h) or O(logn) as that much is needed to keep stack frames
 * - replacing recursion with iterative solution using stack for traversals but also in general is usually not trivial
 */
public class Trees {

    public static void main(String[] args) {
        Node root = new Node(5, new Node(2, 1, 3), new Node(7, 6, 8));
        assertThat(inOrderBstTraversalStack(root)).containsExactly(1, 2, 3, 5, 6, 7, 8);
    }

    public static List<Integer> inOrderBstTraversalRec(Node root) {
        LinkedList<Integer> results = new LinkedList<>();
        inOrderBstTraversalRec(root, results);
        return results;
    }

    private static void inOrderBstTraversalRec(Node root, List<Integer> results) {
        if (root == null) return;
        inOrderBstTraversalRec(root.getLeft(), results);
        results.add(root.getKey());
        System.out.println(root.getKey());
        inOrderBstTraversalRec(root.getRight(), results);
    }

    public static List<Integer> inOrderBstTraversalStack(Node root) {
        LinkedList<Integer> results = new LinkedList<>();
        LinkedList<Node> stack = new LinkedList<>();

        stack.push(root);
        while(!stack.isEmpty()) {
            Node temp = stack.pop();
            while (temp.getLeft() != null) {
                stack.push(temp.getLeft());
                temp = temp.getLeft();
            }
            stack.pop();
            System.out.println("print: " + temp.getKey());
            results.add(temp.getKey());
            System.out.println(stack);
            if (temp.getRight() != null) {
                stack.push(temp.getRight());
            }
        }
        return results;
    }


}

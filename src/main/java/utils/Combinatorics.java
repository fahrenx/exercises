package utils;


import java.util.*;

public class Combinatorics {


  public static void main(String[] args) {
    System.out.println(permutationsSimple("abcd"));
    System.out.println(permutationsNoCollector("abcd"));
    System.out.println(permutationsClever(Arrays.asList(1,2,3,4)));

    System.out.println("");
    for(int[] a: permutationsSimple(new int[]{1, 2, 3, 4})) {
      System.out.print(Arrays.toString(a));
    }
  }

  public static List<int[]> permutationsSimple(int[] arr) {
    List<List<Integer>> results = new LinkedList<>();
    // converting int[] into List<Integer>
    List<Integer> remaining = new ArrayList<>(arr.length);
    for (int i = 0; i < arr.length; i++) {
      remaining.add(arr[i]);
    }
    permutationsSimple(new LinkedList<>(), remaining, results);
    List<int[]> finalResults = new LinkedList<>();
    for (List<Integer> ls : results) {
      int[] a = new int[ls.size()];
      for (int i = 0; i < ls.size(); i++) {
        a[i] = ls.get(i);
      }
      finalResults.add(a);
    }
    return finalResults;
  }

  private static void permutationsSimple(List<Integer> intermediate, List<Integer> remaining, List<List<Integer>> results) {
    if (remaining.isEmpty()) {
      results.add(intermediate);
      return;
    }
    for (int i = 0; i < remaining.size(); i++) {
      List<Integer> newIntermediate = new ArrayList<>(intermediate);
      newIntermediate.add(remaining.get(i));
      List<Integer> newRemaining = new ArrayList<>(remaining);
      newRemaining.remove(i);
      permutationsSimple(newIntermediate, newRemaining, results);
    }
  }

  /**
   * Returns list with all permutationsSimple of input string
   */
  public static List<String> permutationsSimple(String str) {
    LinkedList<String> results = new LinkedList<>();
    permutationsSimple("", str, results);
    return results;
  }

  private static void permutationsSimple(String intermediate, String remaining, List<String> results) {
    if (remaining.isEmpty()) {
      results.add(intermediate);
      return;
    }
    for (int i = 0; i < remaining.length(); i++) {
      permutationsSimple(intermediate + remaining.charAt(i), remaining.substring(0, i) + remaining.substring(i + 1), results);
    }
  }

  /**
   * Same as permutationsSimple but uses return value in implementation rather than
   * collecting value using list passed by reference as parameter
   */
  public static List<String> permutationsNoCollector(String str) {
    return permutationsNoCollector("", str);
  }

  private static List<String> permutationsNoCollector(String intermediate, String remaining) {
    if (remaining.isEmpty()) {
      return Collections.singletonList(intermediate);
    }
    List<String> result = new LinkedList<String>();
    for (int i = 0; i < remaining.length(); i++) {
      result.addAll(permutationsNoCollector(intermediate + remaining.charAt(i),
        remaining.substring(0, i) + remaining.substring(i + 1)));
    }
    return result;
  }

  /**
   *
   * Clever permutations
   *  http://stackoverflow.com/questions/2920315/permutation-of-array
   */
   public static List<List<Integer>> permutationsClever(List<Integer> arr){
     List<List<Integer>> results = new LinkedList<>();
     permutationsClever(arr, 0, results);
     return results;
   }

  /**
   * do swaps between k and rest of the array on right and call recursively while swapping on the right
   */
  private static void permutationsClever(List<Integer> arr, int k, List<List<Integer>> results) {
    for(int i = k; i<arr.size(); i++) {
      Collections.swap(arr, i, k); // exchanging elements with one on the rights
      permutationsClever(arr, k+1, results); // k+1 - condition of progress
      Collections.swap(arr, k, i); // backtracking
    }
    if(k == arr.size()){
      results.add(new ArrayList<>(arr));
    }
  }
}

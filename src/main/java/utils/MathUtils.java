package utils;


import com.google.common.base.Preconditions;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class MathUtils {

  public static double log(double num, int base) {
    return Math.log(num) / Math.log(base);
  }

  public static long factorial(int num) {
    Preconditions.checkArgument(num > 0, "Cannot calculate factorial from negative number");
    long factorial = 1;
    for (int i = 2; i <= num; i++) {
      factorial *= i;
    }
    return factorial;
  }

  public static long combNoRep(int k, int n) {
    Preconditions.checkArgument(k <= n, "k cannot be greater than n");
    Preconditions.checkArgument(k > 0 && n > 0, "k and n have to be greater than 0");
    if (n == k) {
      return 1;
    }
    long factK = factorial(k);
    // (n-k+1)*...*n
    long numeratorAfterReduction = 1;
    for (int i = n - k + 1; i <= n; i++) {
      numeratorAfterReduction *= i;
    }
    return numeratorAfterReduction / factK;
  }

  /**
   * invariant - all generated combinations have ascending indices
   * Start with generating base combination [0,...,k] and now
   * tale that combination and iterating from end to start for each cell, try to increase value of that
   * cell in a way that does not violate invariant
   *
   * @param arr
   * @param k
   * @return
   */
  public static List<List<Integer>> generatorCombinations(int[] arr, int k) {
    List<List<Integer>> results = new LinkedList<>();
    int n = arr.length;
    List<Integer> baseComb = new ArrayList<>(k);
    for (int i = 0; i < k; i++) {
      baseComb.add(i);
    }
    results.add(baseComb);
    generateComb2(baseComb, results, n, k, k - 1);
    return results;
  }

  private static void generateComb2(List<Integer> baseComb, List<List<Integer>> results, int n, int k, int pos) {
    System.out.println("base" + baseComb);
    if (pos == -1) {
      return;
    }
    int limit;
    if (pos == k - 1) {
      limit = n - 1;  // must be withing n
    } else {
      limit = baseComb.get(pos + 1) - 1; // not bigger than next element
    }
    for (int j = pos+1; j <= limit; j++) {    // all possible values of j-th cell
      List<Integer> newComb = new ArrayList<>(baseComb);
      newComb.set(pos, j);
      results.add(newComb);
      generateComb2(newComb, results, n, k, pos - 1);   // given that base combination, shift to cell to the left and repeat
    }
    return;
  }


  public static long combNoRepOptimized(int k, int n) {
    Preconditions.checkArgument(k <= n, "k cannot be greater than n");
    Preconditions.checkArgument(k > 0 && n > 0, "k and n have to be greater than 0");
    if (n == k) {
      return 1;
    }
    k = Math.min(k, n - k); //optimization
    long numeratorAfterReduction = 1;
    for (long i = n - k + 1; i <= n; i++) {
      numeratorAfterReduction *= i;
    }
    long nonReducedDenominator = 1;
    for (long i = 2; i <= k; i++) {
      nonReducedDenominator *= i;
    }
    return numeratorAfterReduction / nonReducedDenominator;
  }

  public static int sumInRange(int start, int end) {
    int numOfElements = end - start + 1;
    if (numOfElements == 1) {
      return start;
    }
    int pair = start + end;
    int sum = (pair * (numOfElements / 2));
    if (numOfElements % 2 == 1) {
      int midEl = start + (numOfElements / 2);
      sum += midEl;
    }
    return sum;
  }


}

package utils;

import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

public class Performance {

    public static Options defaultJmhOptions(Class clazz){
        Options opt = new OptionsBuilder()
                .include(clazz.getSimpleName())
                .warmupIterations(5)
                .measurementIterations(5)
                .forks(1)
                .build();
        return opt;
    }

}

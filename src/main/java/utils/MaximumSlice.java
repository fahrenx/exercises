package utils;

public class MaximumSlice {

    /**
     * Maxending contains biggest possible sum from [0:i] all the time
     * see: https://codility.com/media/train/7-MaxSlice.pdf
     */
    public static long maximumSliceLinear(int[] a) {
        long maxEnding = 0;
        long maxSlice = 0;
        for (long el : a) {
            // shall I take this element or I will be better of zeroing
            maxEnding = Math.max(maxEnding + el, 0);
            maxSlice = Math.max(maxEnding, maxSlice);
        }
        return maxSlice;
    }


}

package utils;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.function.Function;

public class ContestIOController {

    // TODO how to reuse inNLines while providing wrapper for solvingFunctino
    public String processInput(String inputFilename, Function<String, String> solvingFunction, boolean outputFile) throws IOException {
        List<String> lines = readFile(inputFilename);

        int numberOfCases = Integer.parseInt(lines.get(0).trim());
        List<String> inputs = lines.subList(1, 1 + numberOfCases);
        StringBuilder sb = new StringBuilder();
        int inputNum = 1;
        for (String input : inputs) {
            String result = solvingFunction.apply(input);
            sb.append(String.format("Case #%d: ", inputNum)).append(result).append("\n");
            inputNum++;
        }
        String result = sb.toString();

        writeToFileIfRequested(inputFilename, outputFile, result);
        return result;
    }

    public String processInputMultipleLines(String inputFilename, Function<List<String>, String> solvingFunction, boolean outputFile) throws IOException {
        List<String> lines = readFile(inputFilename);

        int numberOfCases = Integer.parseInt(lines.get(0).trim());
        lines = lines.subList(1, lines.size());
        StringBuilder sb = new StringBuilder();
        int caseNum = 1;
        int currentLine = 0;
        for (int i=0; i<numberOfCases;i++) {
            int linesNumForCase = Integer.valueOf(lines.get(currentLine));
            String result = solvingFunction.apply(lines.subList(currentLine, currentLine+linesNumForCase));
            sb.append(String.format("Case #%d: ", caseNum)).append(result).append("\n");
            caseNum++;
            currentLine+=linesNumForCase;
        }
        String result = sb.toString();

        writeToFileIfRequested(inputFilename, outputFile, result);
        return result;
    }

    public String processInputInNLines(int n, String inputFilename, Function<List<String>, String> solvingFunction, boolean outputFile) throws IOException {
        List<String> lines = readFile(inputFilename);

        int numberOfCases = Integer.parseInt(lines.get(0).trim());
        lines = lines.subList(1, lines.size());
        StringBuilder sb = new StringBuilder();
        int caseNum = 1;
        int currentLine = 0;
        for (int i=0; i<numberOfCases;i++) {
            String result = solvingFunction.apply(lines.subList(currentLine, currentLine+n));
            sb.append(String.format("Case #%d: ", caseNum)).append(result).append("\n");
            caseNum++;
            currentLine+=n;
        }
        String result = sb.toString();

        writeToFileIfRequested(inputFilename, outputFile, result);
        return result;
    }

    private List<String> readFile(String inputFilename) throws IOException {
        Path path = Paths.get(Constants.OLD_INPUT_FILES_DIR, inputFilename);
        return Files.readAllLines(path);
    }

    private void writeToFileIfRequested(String inputFilename, boolean outputFile, String result) throws IOException {
        if(outputFile){
            String outputFilename = inputFilename.replace(".in",".out");
            Files.write(Paths.get(outputFilename), result.getBytes());
        }
    }


}

package euler;

import fun.algo.generators.Generators;

import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;


/**
 * Officially known as least common multiple problem
 */
public class SmallestMultiple {

    public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        // TestingPerformanceUtil.runPerformanceTestsForClass(SmallestMultiple.class);

        System.out.println(findSmallestMultipleUsingGcdRecursivelyWithSublisting(convert(Generators.generateNaturalNumberSerie(10))));


    }

    private static List<Long> convert(List<Integer> aList) {
        List<Long> result = new LinkedList<Long>();
        for(Integer i:aList){
            result.add(Long.valueOf(i));
        }
        return result;
    }

    public static void testFindSmallestMultiple() {
        findSmallestMultiple(Generators.generateNaturalNumberSerie(5));
        findSmallestMultiple(Generators.generateNaturalNumberSerie(10));
        findSmallestMultiple(Generators.generateNaturalNumberSerie(15));
    }

    public static void findSmallestMultipleUsingGcd() {
        findSmallestMultipleUsingGcd(Generators.generateNaturalNumberSerie(5));
        findSmallestMultipleUsingGcd(Generators.generateNaturalNumberSerie(10));
        findSmallestMultipleUsingGcd(Generators.generateNaturalNumberSerie(15));
    }


    /**
     * A kind of bruteforce approach. Inside the loop we are increasing the element by the value of the highest factor and check if this element is divisible by
     * all other elements. There is an assumption that the last element in elements list is the biggest one.
     */
    private static long findSmallestMultiple(List<Integer> factors){
        
        boolean found = false;
        int highestFactor = factors.get(factors.size()-1);
        int currentNumber = 0;
        while (!found) {
            currentNumber += highestFactor;

            int i=0;
            boolean stillLooking = true;
            while (i < factors.size() - 1 && stillLooking) {
                if (currentNumber % factors.get(i) != 0) {
                    stillLooking = false;
                }
                i++;
            }

            if (stillLooking) {
                found = true;
            }

        }
        return currentNumber;
    }

    /**
     * More clever approach - group numbers in pairs, finding a common divisor for them and divides by gcd
     */
    private static long findSmallestMultipleUsingGcd(List<Integer> factors) {
        long lastMultiple = getLeastCommonMultipleUsingGcd(factors.get(0), factors.get(1));
        for (int i = 2; i < factors.size() - 1; i++) {
            lastMultiple = getLeastCommonMultipleUsingGcd(lastMultiple, factors.get(i));
            // System.out.println("lcm for:" + lastMultiple + "," + factors.get(i) + " is " + lastMultiple);
        }
        return lastMultiple;
    }

    private static long findSmallestMultipleUsingGcdRecursivelyWithSublisting(List<Long> factors) {
        long lastMultiple = getLeastCommonMultipleUsingGcd(factors.get(0), factors.get(1));
        if (factors.size() == 2) {
            return lastMultiple;
        }
        LinkedList<Long> newList = new LinkedList<Long>();
        newList.add(lastMultiple);
        newList.addAll(factors.subList(2, factors.size()));
        return findSmallestMultipleUsingGcdRecursivelyWithSublisting(newList);
    }

    private static long getLeastCommonMultipleUsingGcd(long a, long b) {
        return (a * b) / greatestCommonDivisorEuclidean(a, b);
    }

    public static long greatestCommonDivisorEuclidean(long a, long b) {
        long first = a;
        long second = b;
        while (first != second) {
            long difference = Math.abs(first - second);
            second = Math.min(first, second);
            first = difference;
        }
        return first;
    }

}

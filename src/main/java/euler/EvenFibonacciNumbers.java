package euler;

public class EvenFibonacciNumbers {

    public static void main(String[] args) {

        long sum = sumOfEvenValuedFibonacciTerms(4000000);
        System.out.println(sum);
    }

    private static long sumOfEvenValuedFibonacciTerms(int valueLimit) {
        long sum = 0;
        long olderPreviousElement = 1;
        long previousElement = 1;
        long currentElement = 0;
        while (currentElement < valueLimit) {
            sum += currentElement;
            currentElement = previousElement + olderPreviousElement;
            olderPreviousElement = previousElement;
            previousElement = currentElement;
        }
        return sum;
    }

    static int fibonacciRecursive(int nth) {
        if (nth == 1) {
            return 1;
        } else if (nth == 2) {
            return 2;
        }
        return fibonacciRecursive(nth - 1) + fibonacciRecursive(nth - 2);
    }

    static long fibonacciIterativeint(int nth) {
        long first = 1;
        long second = 2;
        if (nth == 1)
            return 1;
        if (nth == 2)
            return 2;

        long next = 0;
        for (int i = 2; i < nth; i++) {
            next = first + second;
            first = second;
            second = next;
        }
        return next;
    }

}

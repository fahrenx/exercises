package euler;

import java.util.List;
import java.util.Set;

import fun.algo.combinatorics.CombinatoricsTODOmoreEffectiveClean;
import fun.algo.generators.Generators;

public class SumSquareDifference {

    public static void main(String[] args) {
        long diff = sumSquareDifference(100);
        System.out.println(diff);
        long sumSquareDifferenceSimple = sumSquareDifferenceSimple(100);
        System.out.println(sumSquareDifferenceSimple);
    }

    /**
     * Solution is based on the fact that differences between those two sums is equal to sum of all combinations of 2elements in n-elements set multiplied by 2
     */
    private static long sumSquareDifference(int n) {
        List<Integer> elements = Generators.generateNaturalNumberSerie(n);
        List<Set<Integer>> allCombinations = CombinatoricsTODOmoreEffectiveClean.getAllCombinationsRec(elements, 2);
        long difference = 0L;
        for (Set<Integer> combination : allCombinations) {
            long sumElement = 1;
            for (Integer el : combination) {
                sumElement *= el;
            }
            sumElement *= 2;
            difference += sumElement;
        }
        return difference;
    }

    /**
     * simplest, brute-force solution
     */
    private static long sumSquareDifferenceSimple(int n) {
        List<Integer> elements = Generators.generateNaturalNumberSerie(n);
        long sum = 0;
        long sumOfSquares = 0;
        for (Integer el : elements) {
            sum += el;
            sumOfSquares += el * el;
        }
        long squareOfTheSum = sum * sum;
        return Math.abs(sumOfSquares - squareOfTheSum);
    }



}

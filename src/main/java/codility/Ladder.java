package codility;

import java.util.Arrays;

public class Ladder {
    public static void main(String[] args) {
//        System.out.println(Arrays.toString(new Testing().solution(new int[]{4, 4, 5, 5, 1}, new int[]{3, 2, 4, 3, 1})));
    }

    public int[] solution(int[] A, int[] B) {
        if(A.length==1){
            return new int[]{1};
        }
        int[] ways = new int[A.length];
        // Map<Integer, Integer> rungsToIndex = new HashMap<Integer, Integer>();
        // Map<Integer, Integer> rungsToMod = new HashMap<Integer, Integer>();
        int maxMod = B[0];
        // int maxRungs = A[0];
        for(int i=1; i<A.length; i++){
            maxMod = Math.max(maxMod, B[i]);
            // maxRungs = Math.max(maxRungs, A[i]);
        }
        int mod = (int) Math.pow(2, maxMod);
        ways[0]=1%mod;
        ways[1]=2%mod;
        for(int i=2;i<A.length;i++){
            ways[i] = (ways[i-1]+ways[i-2]) % mod;
        }
        int[] results = new int[A.length];
        for(int i=0; i<A.length ; i++){
            int rungs = A[i];
            results[i] = ways[rungs-1]%((int)Math.pow(2, B[i]));
        }
        return results;
    }

}

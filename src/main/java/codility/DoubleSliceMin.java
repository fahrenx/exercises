package codility;

public class DoubleSliceMin {

  public static void main(String[] args) {
    System.out.println(new DoubleSliceMin().solution(new int[]{1, 2, 3, 4, 6, 2, 1}));
  }

  //TODO
  public int solution(int[] A) {
    int indexOfMin = findIndexOfMinOutsideOfBorders(A);
    int maxSlice = 0;
    int maxEnding = 0;
    for (int i = 1; i < A.length - 1; i++) {
      maxEnding = Math.max(maxEnding + A[i], 0);
      maxSlice = Math.max(maxSlice, maxEnding);
    }
    return 2;
  }

  private int findIndexOfMinOutsideOfBorders(int[] a) {
    int minVal = a[1];
    int min = 1;
    for (int i = 2; i < a.length - 1; i++) {
      if (a[i] < minVal) {
        minVal = a[i];
        min = i;
      }
    }
    return min;
  }


//  private int[] findSmallest(int[] A) {
//    int[] mins = indexesOfThreeFirstInAscendingValuesOrder(A);
//    int minVal = mins[0];
//    for (int i = 1; i < A.length; i++) {
//      if (A[i] < minVal) {
//        mins[2] = mins[1];
//        mins[1] = mins[0];
//        mins[0] = i;
//        minVal = A[i];
//      }
//    }
//    return mins;
//  }
//
//  private static int[] indexesOfThreeFirstInAscendingValuesOrder(int[] a) {
//    int[] results = new int[3];
//    int firstPairWinner = a[0] < a[1] ? 0 : 1;
//    int secondPairWinner = a[1] < a[2] ? 1 : 2;
//    if (firstPairWinner == secondPairWinner) {
//      results[0] = firstPairWinner;
//      if (a[0] < a[2]) {
//        results[1] = 0;
//        results[2] = 2;
//      } else {
//        results[1] = 2;
//        results[2] = 0;
//      }
//    } else {
//      if (a[firstPairWinner] < a[secondPairWinner]) {
//        results[0] = firstPairWinner;
//        results[1] = secondPairWinner;
//      } else {
//        results[0] = secondPairWinner;
//        results[1] = firstPairWinner;
//      }
//      results[2] = 3 - firstPairWinner - secondPairWinner;
//    }
//    return results;
//  }

}

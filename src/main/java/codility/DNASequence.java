package codility;// you can also use imports, for example:
// you can use System.out.println for debugging purposes, e.g.
// System.out.println("this is a debug message");

public class DNASequence {

    // 3,3
    // 1
    // 4,3
    // 3,1
    // 1

    public int[] solution(String S, int[] P, int[] Q) {
        char[] arr = S.toCharArray();
        int[] aOcc = new int[arr.length];
        int[] cOcc = new int[arr.length];
        int[] gOcc = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if(i==0){
                aOcc[i]=0;
                cOcc[i]=0;
                gOcc[i]=0;
            } else {
                aOcc[i]=aOcc[i-1];
                cOcc[i]=cOcc[i-1];
                gOcc[i]=gOcc[i-1];
            }
            if (arr[i] == 'A') {
                aOcc[i]++;
            } else if (arr[i] == 'C') {
                cOcc[i]++;
            } else if (arr[i] == 'G') {
                gOcc[i]++;
            }

        }

        int[] result = new int[P.length];
        for (int i = 0; i < P.length; i++) {
            if (numOfOccs(aOcc, P[i], Q[i]) > 0) {
                result[i] = 1;
            } else if (numOfOccs(cOcc, P[i], Q[i]) > 0) {
                result[i] = 2;
            } else if (numOfOccs(gOcc, P[i], Q[i]) > 0) {
                result[i] = 3;
            } else {
                result[i] = 4;
            }
        }
        return result;

    }

    private int numOfOccs(int[] occ, int start, int end) {
        int occsBefore = start > 0 ? occ[start - 1] : 0;
        return occ[end] - occsBefore;
    }


}

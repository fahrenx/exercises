package codility;

import java.util.Arrays;
import java.util.Comparator;


public class NumberOfDiscsIntersections {


    public static void main(String[] args) {
        int a = 2147483647;
        long b = (long)a+1;
        System.out.println(b);
    }

        public int solution(int[] A) {
            if(A.length<2){
                return 0;
            }
            Disc[] discs= new Disc[A.length];
            for(int i=0;i<A.length;i++){
                discs[i] = new Disc(i-A[i],i+A[i]);
            }
            Arrays.sort(discs, new Comparator<Disc>() {
                @Override
                public int compare(Disc o1, Disc o2) {
                    return Integer.compare(o1.start, o2.start);
                }
            });
            int total = 0;

            //looks quadratic to me...
            for(int i=0;i<discs.length-1;i++){
                int j=i+1;
                while(j<discs.length && discs[i].end>=discs[j].start){
                    total++;
                    if(total>10000000){
                        return -1;
                    }
                    j++;
                }
            }
            return total;
        }


        public static class Disc {
            public final int start;
            public final int end;

            public Disc(int start, int end){
                this.start = start;
                this.end = end;
            }

        }

    }

package codility;

import java.util.LinkedList;

public class Brackets {


    public static void main(String[] args) {
        Brackets b = new Brackets();

        System.out.println(b.solution(""));
    }

    public int solution(String S) {
        if(S.isEmpty()){
            return 1;
        }
        LinkedList<Character> stack = new LinkedList<Character>();
        for(char ch:S.toCharArray()){
            if(ch == '(' || ch == '{' || ch=='['){
                stack.push(ch);
            } else if (ch == ')') {
                Character first = stack.pollFirst();
                if(first==null || !first.equals('(')){
                    return 0;
                }
            } else if (ch == ']') {
                Character first = stack.pollFirst();
                if(first==null || !first.equals('[')){
                    return 0;
                }
            } else if (ch == '}') {
                Character first = stack.pollFirst();
                if(first==null || !first.equals('{')){
                    return 0;
                }
            }
        }
        if(stack.isEmpty()){
            return 1;
        }
        return 0;

    }


}

package codility;

import java.util.*;

public class CountDistinctSlices {

    public static final int MAX = 1000000000;
    public static void main(String[] args) {
        System.out.println(new CountDistinctSlices().solution(6, new int[]{3, 4, 5, 5, 2}));
    }
    public int solution(int M, int[] A) {
        Set<Integer> set = new HashSet<Integer>();
        int front = 0;
        int result = 0;
        for (int back = 0; back < A.length; back++) {
            // can I move front a bit further, is there room for that and can I do it without adding duplicates?
            while (front < A.length && !set.contains(A[front])) {
                set.add(A[front]);
                front++;
                result++;
                if(result>MAX){
                    return MAX;
                }
            }
            // trim from front and front back, count all results and at the end leave it with trimmed by one element in front
            set.remove(A[back]);
            if(!set.isEmpty()) {
                result++;
                if(result>MAX){
                    return MAX;
                }
                int frontTemp = front-1;
                while(set.size()>1){
                    set.remove(A[frontTemp]);
                    result++;

                    frontTemp--;
                    if(result>MAX){
                        return MAX;
                    }
                }
                // restore what you removed
                for(int i = frontTemp+1; i<front;i++){
                    set.add(A[i]);
                }
            }
        }
        return result;
    }

}

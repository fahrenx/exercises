package codility;

import java.util.LinkedList;
import java.util.Optional;

public class EquiLeaders {

    public static void main(String[] args) {
        System.out.println(new EquiLeaders().solution(new int[]{4, 3, 4, 4, 4, 2})); //2
        System.out.println(new EquiLeaders().solution(new int[]{1,1,2,1,3,1})); //3
    }

    public int solution(int[] A) {
        Optional<Integer> leaderFound = findLeaderLinear(A);
        if(!leaderFound.isPresent()){
            return 0;
        }
        int leader = leaderFound.get();
        int leaderOccs = 0;
        for(int el:A){
            if(el == leader) {
                leaderOccs++;
            }
        }
        int equi = 0;
        int counter = 0;
        for(int i=0;i<A.length-1;i++){
            if(A[i] == leader){
                counter++;
            }
            System.out.println(counter+":"+i);
            // i=1 == size of left part  and (A.length-i) is size of right part
            if(counter>(i+1)/2 && (leaderOccs-counter)>(A.length-i-1)/2){
                equi++;
            }
        }
        return equi;
    }

    private Optional<Integer> findLeaderLinear(int[] arr) {
        LinkedList<Integer> stack = new LinkedList<>();
        // TODO all values in stack are equal so we could just use two vars: size of stack and value
        for (int i = 0; i < arr.length; i++) {
            if (stack.isEmpty() || stack.peek() == arr[i]) {
                stack.push(arr[i]);
            } else {
                stack.pop();
            }
        }
        if (stack.isEmpty()) {
            return Optional.empty();
        }
        // we still need to check if value from stack is a leader
        int candidate = stack.pop();
        int counter = 0;
        for (int el : arr) {
            if (el == candidate) {
                counter++;
            }
        }
        return counter > arr.length / 2 ? Optional.of(candidate) : Optional.empty();
    }

}

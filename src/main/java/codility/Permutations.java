package codility;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class Permutations {

    public static void main(String[] args) {
        Permutations test = new Permutations();
        assertThat(test.solution(new int[]{1, 2, 3, 4, 5, 6})).isEqualTo(1);
        assertThat(test.solution(new int[]{1, 2, 3, 4, 6})).isEqualTo(0);
        assertThat(test.solution(new int[]{2})).isEqualTo(0);
        assertThat(test.solution(new int[]{1})).isEqualTo(1);
    }


    public int solution(int[] A) {
        Set<Integer> elements = new HashSet<>();
        for (int el : A) {
            if (el < 1 || el > A.length) {
                return 0;
            }
            if (elements.contains(el)) {
                return 0;
            }
            elements.add(el);
        }
        return 1;
    }

}

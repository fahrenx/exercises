package codility;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

public class SmallFrog {


    public static void main(String[] args) {
//        assertThat(new SmallFrog().solution(5, new int[]{1, 3, 1, 4, 2, 3, 5, 4})).isEqualTo(6);
//        assertThat(new SmallFrog().solution(4, new int[]{1,2,3,4,5})).isEqualTo(3);
//        assertThat(new SmallFrog().solution(1, new int[]{1})).isEqualTo(0);
        int[]  arr = new int[]{1, 3, 1, 4, 2, 3, 5, 4};
        arr[1]++;
        System.out.println(Arrays.toString(arr));
    }






        public int solution(int X, int[] A) {
            if(A.length < X) return -1;
            Set<Integer> remaining = new HashSet<Integer>();
            for(int i=1; i<=X;i++){
                remaining.add(i);
            }
            for(int i=0;i<A.length;i++){

                if(remaining.remove(A[i]) && remaining.isEmpty()){
                    return i;
                }
            }
            return -1;
        }


}

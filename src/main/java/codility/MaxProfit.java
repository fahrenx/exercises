package codility;

public class MaxProfit {

    //0,600, 1000, 899, 0
    public int solution(int[] A) {
        if(A.length<2){
            return 0;
        }
        int maxEnding = 0;
        int currentPrice = A[0];
        int maxProfit = 0;
        for(int i=1;i<A.length; i++){
            if(A[i]-currentPrice>0){
                maxEnding = A[i] - currentPrice;
            } else {
                maxEnding = 0;
                currentPrice = A[i];
            }
            maxProfit = Math.max(maxProfit, maxEnding);
        }
        return maxProfit;
    }
    
}

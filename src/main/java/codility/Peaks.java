package codility;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class Peaks {

    public static void main(String[] args) {
//        System.out.println(new Peaks().solution(new int[]{1, 21, 3, 2, 5, 1, 3,2}));
        System.out.println(new Peaks().solution(new int[]{1,3,1,1,2,1,1,3,1,1,3,1}));


    }

    public int solution(int[] A) {
        if(A.length<3) return 0;
        int peaksNum = 0;
        int j=1;
        int[] peaks = new int[A.length];
        while(j<A.length-1){
            if(A[j] > A[j-1] && A[j] > A[j+1]){
                peaksNum++;
            }
            peaks[j] = peaksNum;
            j++;
        }
        peaks[A.length-1] = peaks[A.length-2];
        if(peaksNum == 0) return 0;
        if(peaksNum == 1) return 1;
        List<Integer> factors = calculateFactorsAsc(A.length);
        for(int f: factors){
            int blocks = A.length/f;
            if(blocks>peaksNum){  // we cannot afford to have more blocks than total number of peaks
                continue;
            } else {
                int i=1;
                int peaksSoFar = 0;
                // any new blocks to check? current block has at least one peak? enough peaks left for new blocks?
                while( i<=blocks && peaks[i*f-1] > peaksSoFar && peaksNum - peaks[i*f-1] >= blocks-i){
                    peaksSoFar = peaks[i*f-1];
                    i++;
                }
                if(i>blocks){
                    return blocks;
                }
            }
        }
        return 0;
    }

    private List<Integer> calculateFactorsAsc(int n) {
        if (n == 1) return Collections.singletonList(n);
        List<Integer> results = new LinkedList<Integer>();
        results.add(1);
        int i = 2;
        while (i * i < n) {
            if (n % i == 0) {
                results.add(i);
            }
            i++;
        }
        int squareShift = 0;
        if (i * i == n) {
            results.add(i);
            squareShift=1;
        }
        // to be sure that it is ascending
        int initialPos = results.size()-1-squareShift;
        for(int k=initialPos; k>=0; k--) {
            results.add(n/results.get(k));
        }
        System.out.println(results);
        return results;
    }

}

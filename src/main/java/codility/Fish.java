package codility;

import java.util.*;

// you can use System.out.println for debugging purposes, e.g.
// System.out.println("this is a debug message");

public class Fish {
    // 3,3,1,1,1

    public int solution(int[] A, int[] B) {
        int eaten = 0;
        LinkedList<Integer> goingDownstream = new LinkedList<Integer>();
        for( int i=0 ; i < A.length; i++){
            int weight = A[i];
            if(B[i] ==0 ){ //coming up, fight
                boolean alive = true;
                while(!goingDownstream.isEmpty() && alive){
                    eaten++;
                    int opponentWeight = goingDownstream.pop();
                    if(opponentWeight>weight){
                        goingDownstream.push(opponentWeight);
                        alive = false;
                    }
                }
            } else { //coming down, just add to queue
                goingDownstream.push(weight);
            }

        }
        return A.length - eaten;

    }
}
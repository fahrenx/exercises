package codility;


import java.util.Arrays;


import static org.assertj.core.api.Assertions.assertThat;

public class MaxProductOfThree {


    public static void main(String[] args) {

        MaxProductOfThree e = new MaxProductOfThree();
        assertThat(e.solution(new int[]{2,3,5,6,7})).isEqualTo(5 * 6 * 7);
        assertThat(e.solution(new int[]{-2,0,-4,6,7})).isEqualTo(8*7);


    }

    public int solution(int[] A) {
        int n = A.length;
        if(A.length<3){
            return -1;
        }
        if(A.length==3){
            return A[0]*A[1]*A[2];
        }
        Arrays.sort(A);
//        quicksort(A, 0, n - 1);
        int maxEl = A[n-1];
        if( maxEl < 0 ){
            return maxEl*A[n-2]*A[n-3];
        }else{
            int rest = Math.max(A[0]*A[1],A[n-2]*A[n-3]); // two smallest can be better if they are negative
            return rest*maxEl;
        }
    }

    private void quicksort(int [] a, int start, int end){
        if(start<0 || end>a.length-1 || end<=start){
            return;
        }
        int pivot = a[start+(end-start)/2];
        int i = start;
        int j = end;
        while(i<j){
            while(a[i]<pivot){
                i++;
            }
            while(a[j]>pivot){
                j--;
            }
            int temp = a[i];
            a[i] = a[j];
            a[j] = temp;
            i++;
            j--;
        }
        quicksort(a, start, j);
        quicksort(a, i, end);
    }

}
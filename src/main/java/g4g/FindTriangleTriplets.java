package g4g;

import java.util.Arrays;

/**
 * http://www.geeksforgeeks.org/find-number-of-triangles-possible/
 */
public class FindTriangleTriplets {


  public static void main(String[] args) {
    int[] arr = new int[]{10, 21, 22, 100, 101, 200, 300};
    System.out.println(countTriplets(arr));
  }

  public static int countTriplets(int[] arr) {
    int count = 0;
    Arrays.sort(arr);
    for (int i = 0; i < arr.length - 2; i++) {
      int k = i+2;
      for (int j = i+1; j < arr.length - 1; j++) {
//        int k = j;
        int num = 0;
        while (k < arr.length && arr[i] + arr[j] > arr[k]) {
          num++;
          k++;
        }
        System.out.println(String.format("%d, %d, %d",i, j,num));
        count += k-j-1;
      }
    }
    return count;
  }


}

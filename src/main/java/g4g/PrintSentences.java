package g4g;

import java.util.LinkedList;
import java.util.List;

/**
 * http://www.geeksforgeeks.org/recursively-print-all-sentences-that-can-be-formed-from-list-of-word-lists/
 */
public class PrintSentences {


  public static void main(String[] args) {
    String[][] input = {{"you", "we"},
            {"have", "are"},
            {"sleep", "eat", "drink"}};
    System.out.println(printIndirectly(input));
  }


  public static void printDirectly(String[][] words) {
    printDirectly(words, 0, "");
  }

  private static void printDirectly(String[][] words, int currPos, String sentence) {
    if (currPos == words.length) {
      System.out.println(sentence);
      return;
    }
    for (String word : words[currPos]) {
      printDirectly(words, currPos + 1, sentence + " " + word);
    }
  }

  private static List<String> printIndirectly(String[][] words) {
    List<String> result = new LinkedList<>();
    for (int i = 0; i < words[0].length; i++) {
      String[] output = new String[words.length];
      printIndirectly(words, 0, i, output);

      StringBuilder sb = new StringBuilder();
      for (int j = 0; j < output.length - 1; j++) {
        sb.append(output[j]).append(",");
      }
      sb.append(output[output.length-1]);
      result.add(sb.toString());
    }
    return result;
  }

  private static void printIndirectly(String[][] words, int pos, int wordIndex, String[] output) {
    if(pos == words.length){
      return;
    }
    output[pos]= words[pos][wordIndex];
    for (int i = 0; i < words[pos].length; i++) {
      printIndirectly(words, pos+1, i, output);
    }
  }

}

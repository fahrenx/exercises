package g4g;

import utils.Miscs;

/**
 * http://www.geeksforgeeks.org/given-matrix-o-x-find-largest-subsquare-surrounded-x/
 */
public class LargestSubsquareSurroundedWithX {


  public static void main(String[] args) {
    char[][] matAns4 = {{'X', 'O', 'X', 'X', 'X', 'X'},
            {'X', 'O', 'X', 'O', 'X', 'X'},
            {'X', 'X', 'X', 'O', 'O', 'X'},
            {'O', 'X', 'X', 'X', 'X', 'X'},
            {'X', 'X', 'X', 'O', 'X', 'O'},
            {'O', 'O', 'X', 'O', 'O', 'O'},
    };
    char[][] matAns3 = { {'X', 'O', 'X', 'X', 'X'},
            {'X', 'X', 'X', 'X', 'X'},
            {'X', 'X', 'O', 'X', 'O'},
            {'X', 'X', 'X', 'X', 'X'},
            {'X', 'X', 'X', 'O', 'O'},
    };
    System.out.println(solve(matAns3));
  }

  private static int solve(char[][] m) {
    int[][] hor = new int[m.length][m.length];
    int[][] ver = new int[m.length][m.length];
    for (int i = 0; i < m.length; i++) {
      int prevHor = 0;
      for (int j = 0; j < m[i].length; j++) {
        if (m[i][j] == 'X') {
          hor[i][j] = prevHor + 1;
          if (i > 0) {
            ver[i][j] = ver[i - 1][j] + 1;
          } else {
            ver[i][j] = 1;
          }

        } else {
          hor[i][j] = 0;
          ver[i][j] = 0;
        }
        prevHor = hor[i][j];


      }
    }
    System.out.println(Miscs.print2DArray(hor));
    System.out.println(Miscs.print2DArray(ver));
    int max = 0;
    for (int i = m.length - 1; i >= 0; i--) {
      for (int j = m[i].length - 1; j >= 0; j--) {
        int streak = Math.min(hor[i][j], ver[i][j]);
        while (streak > max) {
          boolean upperBorderCheck = hor[i - streak+1][j] >= streak;
          boolean leftBorderCheck = ver[i][j - streak+1] >= streak;
          if (upperBorderCheck && leftBorderCheck) {
            max = streak;
          }
          streak--;
        }
      }
    }
    return max;
  }
}

package topcoder.medium;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import utils.Performance;

/**
 * lessons learnt:
 * - so many different way of approaching the problem, comment may help to remind about the way you imagined the problem at given point of time
 */
public class RectangularGrid {

    public static void main2(String[] args) {
        System.out.println(countRectangles(3, 3)); //22
        System.out.println(countRectangles(592, 964)); //81508708664
    }

    @Benchmark
    public void performanceTest(Blackhole bh) {
        bh.consume(countRectangles(3, 3));
        bh.consume(countRectangles(592, 964));
    }

    public static void main(String[] args) throws RunnerException {
        Options options = Performance.defaultJmhOptions(RectangularGrid.class);
        new Runner(options).run();
    }

    public static long countRectangles(int width, int height) {
        long rectangles = 0;
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                // all rectangles with left top corner on (j,i)
                int widthRange = width - i;
                int heightRange = height - j;
                int squares = Math.min(widthRange, heightRange);
                rectangles += widthRange * heightRange - squares;
            }
        }
        return rectangles;
    }

}

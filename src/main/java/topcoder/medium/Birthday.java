package topcoder.medium;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * SRM 151
 *
 * 2.5 poms
 *
 * lessons learnt
 * - read problem description carefully (I switched months with days)
 * - funny thing - you have to subtract one from month, typical off-by-one error
 * - interesting problems with birthday-current calculation, we had to make sure by some adjustments that -x (birthday already was this year) will be greater than any
 *      positive number but e.g -254 must remain smaller after this adjustment -240. Absolute function does not help, what we had to to is to add to all negatives
 *      a number which is greater or equal to maximum possible positive number in this calculation, so we are adding 365, but it could be any >= 361 (11*30+31)
 *      Think about it like some kind of a circle on which you can only go into one direction (ahead)
 * - we assumed that months have 30 days and it is nice simplification and it works as long as we are consistent (look above), it could be even 100
 * - sorting solves a lot of problems:) interesting solution based on lexicographical order
 */
public class Birthday {


    public static void main(String[] args) {
        System.out.println(getNext("06/17", new String[]{"02/17 Wernie", "10/12 Stefan"})); // "10/12"
        System.out.println(getNext("02/17", new String[]{"02/17 Wernie", "10/12 Stefan"})); // "02/17"
        System.out.println(getNext("06/17", new String[]{"10/12 Stefan"})); // "10/12"
        System.out.println(getNext("12/24", new String[]{"10/12 Stefan"})); // "10/12"
        System.out.println(getNext("01/02", new String[]{"02/17 Wernie",
                        "10/12 Stefan",
                        "02/17 MichaelJordan",
                        "10/12 LucianoPavarotti",
                        "05/18 WilhelmSteinitz"})
        ); //  "02/17"
    }

    // written fast
    public static String getNext2(String date, String[] birthdays) {
        if (birthdays.length == 1) {
            return stripName(birthdays[0]);
        }
        int currentDay = convertToDays(date);
        int daysToClosestBirthday = Integer.MAX_VALUE;
        String closestBirthday = stripName(birthdays[0]);
        for (String birthday : birthdays) {
            int diff = convertToDays(birthday) - currentDay;
            if (diff < 0) diff += 365;
            if (daysToClosestBirthday > diff) {
                daysToClosestBirthday = diff;
                closestBirthday = birthday;
            }
        }
        return stripName(closestBirthday);
    }

    /**
     * Written later, based on ordering and lexicographical order.
     * Quite nice and compact.
     */
    public static String getNext(String date, String[] birthdays) {
        List<String> dates = new ArrayList<>(birthdays.length+1);
        dates.add(date);
        for(String b:birthdays){
            dates.add(stripName(b));
        }
        Collections.sort(dates);
        for (int i = 0; i < dates.size(); i++) {
            if(dates.get(i).equals(date)){
                if(i<dates.size()-1){ // is there any date ahead?
                    return dates.get(i+1);
                } else {
                    return dates.get(i-1);
                }
            }
        }
        return dates.get(0);
    }


    private static int convertToDays(String str) {
        if (str.length() > 5) {
            str = stripName(str);
        }
        int months = Integer.parseInt(str.substring(0, 2));
        int days = Integer.parseInt(str.substring(3, 5));
        return (months-1) * 30 + days;
    }

    private static String stripName(String str) {
        return str.substring(0, 5);
    }

}

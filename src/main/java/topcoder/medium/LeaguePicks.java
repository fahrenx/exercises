package topcoder.medium;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * SRM 152
 * <p>
 * lessons learnt:
 * - too much of trial and error, prepare it better on paper and then write it once and be fully convinced about solution!
 * - off by one errors! it was type of task where it was extremely important
 */
public class LeaguePicks {

    public static void main(String[] args) {
        System.out.println(Arrays.toString(returnPicks(3, 6, 15))); // { 3,  10,  15 }
        System.out.println(Arrays.toString(returnPicks(1, 1, 10))); // { 1,  2,  3,  4,  5,  6,  7,  8,  9,  10 }
        System.out.println(Arrays.toString(returnPicks(1, 2, 39))); // { 1,  4,  5,  8,  9,  12,  13,  16,  17,  20,  21,  24,  25,  28,  29,32,  33,  36,  37 }
        System.out.println(Arrays.toString(returnPicks(5, 11, 100))); // { 5,  18,  27,  40,  49,  62,  71,  84,  93 }
    }


    public static int[] returnPicks(int position, int friends, int picks) {
        List<Integer> results = new ArrayList<>();
        int pickNumber = position;
        int round = 0;
        int distFromPosToEndAndBack = (position - 1) * 2 + 1;
        int distFromPosToBeginAndBack = (friends - position) * 2 + 1;
        do {
            results.add(pickNumber);
            round++;
            if (round % 2 == 0) { // going to the original beginning of queue and coming back
                pickNumber += distFromPosToEndAndBack;
            } else {              // going to the original end of queue and coming back
                pickNumber += distFromPosToBeginAndBack;
            }
        } while (pickNumber <= picks);
        int[] arr = new int[results.size()];
        for (int i = 0; i < results.size(); i++) {
            arr[i] = results.get(i);
        }
        return arr;
    }

}

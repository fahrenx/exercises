package topcoder.medium;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import utils.Performance;

/**
 * SRM 157
 * lessons learnt:
 *  - parsing input and operations on string can be time consuming and optimized
 *  - split is rather slow, if you know precise format, substring can be much faster
 *  - parseInt is faster than valueOf - valueOf wraps parseInt and box to Integer, parseInt just returns primitive
 */
public class ExerciseMachine {

    public static void main2(String[] args) throws InterruptedException {
        Thread.sleep(10000);
        System.out.println(getPercentages("00:30:00")); // 99
      //  System.out.println(getPercentages("00:14:10")); // 49
        Thread.sleep(10000);

    }

    @Benchmark
    public void performanceTest(Blackhole bh) {
        bh.consume(getPercentages("00:30:00"));
        bh.consume(getPercentages("00:14:10"));
    }

    public static void main(String[] args) throws RunnerException {
        Options options = Performance.defaultJmhOptions(ExerciseMachine.class);
        new Runner(options).run();
    }

    /**
     * split:   1194100
     * no split:1818462 substring instead, 50% better!
     * parseInt instead of valueOf - few percents of gain
     */
    public static int getPercentages(String time) {
//        String[] times = time.split(":");
//        int totalSecs = Integer.parseInt(times[0]) * 3600 + Integer.parseInt(times[1]) * 60 + Integer.parseInt(times[2]);
//        int totalSecs = Integer.parseInt(time.substring(0, 2)) * 3600 + Integer.parseInt(time.substring(3, 5)) * 60 + Integer.parseInt(time.substring(6, 8));
        int totalSecs = Integer.valueOf(time.substring(0, 2)) * 3600 + Integer.valueOf(time.substring(3, 5)) * 60 + Integer.valueOf(time.substring(6,8));
        int percentages = 0;
        for (int i = 1; i < 100; i++) {
            if ((i * totalSecs) % 100 == 0) {
                percentages++;
            }
        }
        return percentages;
    }


}

package topcoder.medium;

/**
 * SRM 149
 * lessons learnt:
 * - Math.max/min is very useful
 * - when problem is already solved it is good to spend a while to simplify/refactor solution and make it more readable
 * - again, splitting problems into smaller - like base conversion, helps
 * from implementing base conversion:
 * - corner cases (0) and optimization (the same base) has to be taken into account
 * - it is good to take a look at jdk implementations of stuff - like Integer.toString
 * - StringBuilder uses char buffer internally and it has constructor that can set capacity so to avoid array resizing
 * - nice trick - given int size (2^31-1) and minimum base of 2 (base cannot be one!) then max length of converted cannot be greater than 32
 *  or 33 if sign included
 *
 */
public class BigBurger {


    public static void main(String[] args) {
        System.out.println(maxWait(new int[]{3, 3, 9}, new int[]{2, 15, 14})); // 11
        System.out.println(maxWait(new int[]{182}, new int[]{11})); // 0
        System.out.println(maxWait(new int[]{2, 10, 11}, new int[]{3, 4, 3})); // 3
        System.out.println(maxWait(new int[]{2, 10, 12}, new int[]{15, 1, 15})); // 7
    }

    public static int maxWait(int[] arrival, int[] service) {
        int n = arrival.length;
        int lastFinish = arrival[0];
        int maxWait = 0;
        for (int i = 0; i < n; i++) {
            int timeOfGivingOrder = Math.max(arrival[i], lastFinish);
            int waitingInQueue = timeOfGivingOrder -  arrival[i];
            lastFinish = timeOfGivingOrder + service[i];
            maxWait = Math.max(maxWait, waitingInQueue);
        }
        return maxWait;
    }


}

package topcoder.medium;

import java.util.HashMap;
import java.util.Map;

/**
 * SRM 148
 * lessons learnt:
 * - read problem description more carefully
 */
public class CeyKaps {


    public static void main(String[] args) {
        System.out.println(decipher("ABCDE", new String[]{"A:B", "B:C", "C:D", "D:E", "E:A"})); // AEBCD
        System.out.println(decipher("IHWSIOTCHEDMYKEYCAPSARWUND", new String[]{"W:O", "W:I"})); // WHOSWITCHEDMYKEYCAPSAROUND
    }

    public static String decipher(String typed, String[] switches) {
        Map<Character, Character> mapping = new HashMap<>();
        for (String s : switches) {
            char from = s.charAt(0);
            char to = s.charAt(2);
            char prevFromMapping = mapping.getOrDefault(from, from);
            mapping.put(from, mapping.getOrDefault(to, to));
            mapping.put(to, prevFromMapping);
        }
        char[] intended = new char[typed.length()];
        for (int i = 0; i < typed.length(); i++) {
            char currChar = typed.charAt(i);            //TODO  OPTIMIZE THIS BY REVERTING MAP!!!
            intended[i] = currChar;
            for(Map.Entry<Character, Character> en: mapping.entrySet()){
                if(en.getValue()==currChar){
                    intended[i] = en.getKey();
                    break;
                }
            }
        }
        return new String(intended);

    }


//    public static String decipher(String typed, String[] switches) {
//        int[] offsets = new int['Z' - 'A' + 1]; // array of the length of alphabet, stores offsets
//        for (String s : switches) {
//            char charFrom = s.charAt(0);
//            char charTo = s.charAt(2);
//            offsets[charFrom - 'A'] = charTo - charFrom;
//        }
//        char[] deciphered = new char[typed.length()];
//        for (int i = 0; i < typed.length(); i++) {
//            int currChar = typed.charAt(i);
//            deciphered[i] = (char) (currChar + offsets[currChar - 'A']);
//        }
//        return new String(deciphered);
//    }

}

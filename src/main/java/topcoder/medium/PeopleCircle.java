package topcoder.medium;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import utils.Performance;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * SRM 147
 * lessons learnt:
 *  - solution based on char[] can be quite elegant
 *  - initialization of char
 *  - Arrays.fill
 *  - clever usage of queue: q.add(q.poll) to simulate circle
 *  - if sufficient, choose arrays as they tend to be the fastest structure  - more complex value structure usually introduce significant overhead
 */
public class PeopleCircle {

    public static void main2(String[] args) {
        System.out.println(order(5, 3, 2)); //MFMFMFMM
        System.out.println(order(7, 3, 1)); //FFFMMMMMMM
        System.out.println(order(25, 25, 1000)); //MMMMMFFFFFFMFMFMMMFFMFFFFFFFFFMMMMMMMFFMFMMMFMFMMF
    }

    public static void main(String[] args) throws RunnerException {
        Options options = Performance.defaultJmhOptions(PeopleCircle.class);
        new Runner(options).run();
    }

    @Benchmark
    public void performanceTest(Blackhole bh) {
        bh.consume(order(5, 3, 2));
        bh.consume(order(7, 3, 1));
        bh.consume(order(25, 25, 1000));
    }

    /**
     * tp: 26201
     */
    public static String order1(int numMales, int numFemales, int k) {
        // '-1' means removed -> means place for a female
        int[] circle = new int[numMales + numFemales];
        int placesMarked = 0;
        int cursor = 0;
        int gapFromLast = 0;
        while (placesMarked < numFemales) {
            if (circle[cursor] == 0) {
                gapFromLast++;
                if (gapFromLast == k) {
                    circle[cursor] = -1;
                    gapFromLast = 0;
                    placesMarked++;
                }
            } else {
                // place already assigned to female removed previously, do nothing, gap not increased
            }
            // move forward
            cursor++;
            if (cursor == circle.length) {
                cursor = 0;
            }
        }
        StringBuilder sb = new StringBuilder();
        for (int v : circle) {
            if (v == -1) {
                sb.append("F");
            } else {
                sb.append("M");
            }
        }
        return sb.toString();
    }

    /**
     * solution based on char array, similar performance
     */
    public static String order(int numMales, int numFemales, int k) {
        // 'F' means place for a female
        char[] circle = new char[numMales + numFemales];
        Arrays.fill(circle, 'M');
        int placesMarked = 0;
        int cursor = 0;
        int gapFromLast = 0;
        while (placesMarked < numFemales) {
            if (circle[cursor] == 'M') {
                gapFromLast++;
                if (gapFromLast == k) {
                    circle[cursor] = 'F';
                    gapFromLast = 0;
                    placesMarked++;
                }
            } else {
                // place already assigned to female removed previously, do nothing, gap not increased
            }
            // move forward
            cursor++;
            if (cursor == circle.length) {
                cursor = 0;
            }
        }
        return new String(circle);
    }


    /**
     * from DoublePointer
     * based on queue, good one but rather slow:
     * tp: 4489, so about 5 times slower than mine
     */
    public static String order3(int numMales, int numFemales, int k){
        int n = numMales + numFemales;
        char[] circle = new char[n];
        Arrays.fill(circle, 'M');
        Queue<Integer> q = new LinkedList<>(); // queue of indices
        for (int i = 0; i < n; i++) {
            q.add(i);
        }
        for(int i =0; i<numFemales; i++){
            for (int j = 0; j < k-1; j++) {
                q.add(q.poll());    // move first k-1 from head to the end
            }
            circle[q.poll()] = 'F';
        }
        return new String(circle);

    }


}

package topcoder.medium;

/**
 * SRM 154
 * <p>
 * lessons learnt
 * - nice trick to implement transition from A-N to N-Z and from N-Z to A-M : add 13 and modulo 26 (plus normalization by subtracting 'A')
 *   it became clear and simple if written on paper
 * - Character.isUpperCase/isDigit/isLowerCase
 */
public class SuperRot {

    public static void main(String[] args) {
        System.out.println(decoderMoreClever("Uryyb 28")); //Hello 73
        System.out.println(decoderMoreClever("GbcPbqre")); //"TopCoder"
        System.out.println(decoderMoreClever(""));
        System.out.println(decoderMoreClever("0123456789")); //"5678901234"
        System.out.println(decoderMoreClever("NnOoPpQqRr AaBbCcDdEe")); //"AaBbCcDdEe NnOoPpQqRr"
        System.out.println(decoderMoreClever("Gvzr vf 54 71 CZ ba Whyl 4gu bs gur lrne 7558 NQ")); //"Time is 09 26 PM on July 9th of the year 2003 AD"
        System.out.println(decoderMoreClever("Gur dhvpx oebja sbk whzcf bire n ynml qbt")); //"The quick brown fox jumps over a lazy dog"
    }

    public static String decoder(String message) {
        char[] decoded = new char[message.length()];
        char[] messageArr = message.toCharArray();
        for (int i = 0; i < message.length(); i++) {
            char ch = messageArr[i];
            if (ch == ' ') {
                decoded[i] = messageArr[i];
            } else if (ch >= 'A' && ch <= 'M') {
                decoded[i] = (char) (ch + 13);
            } else if (ch >= 'N' && ch <= 'Z') {
                decoded[i] = (char) (ch - 13);
            } else if (ch >= 'a' && ch <= 'm') {
                decoded[i] = (char) (ch + 13);
            } else if (ch >= 'n' && ch <= 'z') {
                decoded[i] = (char) (ch - 13);
            } else if (ch >= '0' && ch <= '4') {
                decoded[i] = (char) (ch + 5);
            } else if (ch >= '5' && ch <= '9') {
                decoded[i] = (char) (ch - 5);
            } else {
                throw new IllegalArgumentException("Illegal character found:" + ch);
            }
        }
        return new String(decoded);
    }

    public static String decoderMoreClever(String message) {
        char[] decoded = new char[message.length()];
        char[] messageArr = message.toCharArray();
        for (int i = 0; i < message.length(); i++) {
            char ch = messageArr[i];
            if (Character.isUpperCase(ch)) {
                decoded[i] = (char) ((ch - 'A' + 13) % 26 + 'A');
            } else if (Character.isLowerCase(ch)) {
                decoded[i] = (char) ((ch - 'a' + 13) % 26 + 'a');
            } else if (Character.isDigit(ch)) {
                decoded[i] = (char) ((ch - '0' + 5) % 10 + '0');
            } else {
                decoded[i] = ch;
            }
        }
        return new String(decoded);
    }
}

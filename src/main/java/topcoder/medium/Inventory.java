package topcoder.medium;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * SRN 153
 * - 4 poms
 *
 * lessons learnt:
 * - nice trick with rounding up int division
 * - incorrectly interpreted sentence about rounding - only the final result shoule be rounded

 * DOUBLE IMPRECISION - watch out for it and notice that it can be accumulated easily
 * - double precision needs to be used then it is better to keep subresults also in double rather than rounding them at each stage
 * - way how you do calculations, how they are logically divided, what is the order of operations like sum, multiplication, division
 * can have large impact on final result due to potential different rounding errors in each case
 * - it touches the problem of "numerical stability"
 * - prefer setScale over round when using BigDecimal as round starts counting from left-most NON-ZERO number of the exact result
 * - interesting ROUND_HALF_EVEN - recommended when used on sequence of numbers to minimize round-up errors
 * - BigDecimal may throw all sorts of exceptions, example here : http://stackoverflow.com/questions/4591206/arithmeticexception-non-terminating-decimal-expansion-no-exact-representable
 * - MathContext object
 */

public class Inventory {

    /**
     * epsilon recommended in description of the task
     */
    public static final double EPSILON = Math.pow(10, -9);

    public static void main(String[] args) {
        System.out.println(monthlyOrder(new int[]{75, 120, 0, 93}, new int[]{24, 30, 0, 30})); // 103
        System.out.println(monthlyOrder(new int[]{1115, 7264, 3206, 6868, 7301}, new int[]{1, 3, 9, 4, 18})); // 36091
        System.out.println(monthlyOrder(new int[]{8773}, new int[]{16})); // 16450
    }

    public static int monthlyOrder(int[] sales, int[] daysAvailable) {
        double totalPotentialSales = 0;
        int monthsSkipped = 0;
        for (int i = 0; i < sales.length; i++) {
            if (sales[i] == 0) {
                monthsSkipped++;
                continue;
            }
            totalPotentialSales += sales[i] * 30.0 / (double) daysAvailable[i];
        }
        int monthsConsidered = sales.length - monthsSkipped;
        return (int) Math.ceil((totalPotentialSales / (double) monthsConsidered) - EPSILON);
    }

    /**
     * solution based on big decimal, again, incorrect result for case #2 or #3 depending on rounding technique
     */
    public static int monthlyOrderBigDecimal(int[] sales, int[] daysAvailable) {
        BigDecimal totalPotentialSales = new BigDecimal(0);
        int monthsSkipped = 0;
        for (int i = 0; i < sales.length; i++) {
            if (daysAvailable[i] == 0) {
                monthsSkipped++;
                continue;
            }
            totalPotentialSales = totalPotentialSales.add(new BigDecimal(sales[i] * 30.0).divide(new BigDecimal(daysAvailable[i]), 10, RoundingMode.CEILING));

        }
        int monthsConsidered = sales.length - monthsSkipped;
        BigDecimal result = totalPotentialSales.divide(new BigDecimal(monthsConsidered), 0, RoundingMode.CEILING);
        return result.intValue();
    }

    /**
     * oddly, I cannot produce correct results for all cases due to rounding
     */
    public static int monthlyOrderIntArithmetic(int[] sales, int[] daysAvailable) {
        int totalPotentialSales = 0;
        int monthsSkipped = 0;
        for (int i = 0; i < sales.length; i++) {
            if (sales[i] == 0) {
                monthsSkipped++;
                continue;
            }
            totalPotentialSales += intRoundUpDiv(sales[i] * 30, daysAvailable[i]);
        }
        int monthsConsidered = sales.length - monthsSkipped;
        return intRoundUpDiv(totalPotentialSales, monthsConsidered);
    }

    /**
     * assumptino - divident and divisor are positive
     */
    private static int intRoundUpDiv(int divident, int divisor) {
        divident += divisor - 1;
        return divident / divisor;
    }

}

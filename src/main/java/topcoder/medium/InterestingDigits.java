package topcoder.medium;


import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import utils.Performance;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * SRM 150
 * <p>
 * lessons learnt:
 * - small error in boolean expression in loop can be very damaging
 * - denormalization can be good for performance (but it hinders reusability and readability) - like conversion to base and summing up digit but
 *  also it is a matter of not calculating what is not needed for given task
 * - THE SOLUTION THAT IS BASED ON THE FACT THAT ALL INTERESTING NUMBERS FOR GIVEN BASE MEET ONE SPECIFIC CONDITION (!) :
 *   (base-1) % interesting_number == 0
 *   just blew my mind.. this is what is called "judo solution". I could have observed that from answers. Is there a proper math evidence/intuition for that?
 */
public class InterestingDigits {

    public static void main2(String[] args) {
        System.out.print(Arrays.toString(digits(10))); // 3,9
        System.out.print(Arrays.toString(digits(9))); // 2, 4, 8
        System.out.print(Arrays.toString(digits(26))); // 5, 25
        System.out.print(Arrays.toString(digits(30))); // 29
    }

    public static void main(String[] args) throws IOException, RunnerException {
        Options options = Performance.defaultJmhOptions(InterestingDigits.class);
        new Runner(options).run();
    }

    @Benchmark
    public void performanceTest(Blackhole bh) {
        bh.consume(digits(10));
        bh.consume(digits(9));
        bh.consume(digits(26));
        bh.consume(digits(30));
    }

    /**
     * tp: 1282
     * tp after denormalization: 32044 (over 25x faster!)
     */
    public static int[] digits(int base) {
        List<Integer> iDigits = new ArrayList<>();
        // 0,1 are trivial and cannot belong to group of interesting numbers
        for (int currDigit = 2; currDigit < base; currDigit++) {
            boolean multiplication = true;
            int number = currDigit;
            while (multiplication && number < 1000) {
//                String numAsString = changeBaseDigitsOnlyWithSeparators(number, base);
//                String[] digitsInNumber = numAsString.split(";");
////                System.out.println(Arrays.toString(digitsInNumber)+ " current digit "+currDigit+" current number "+number);
//                int digitsSum = 0;
//                for (String d : digitsInNumber) {
//                    digitsSum += Integer.parseInt(d);
//                }

                multiplication = convertToBaseAndSumUpDigits(number, base) % currDigit == 0; // does condition still hold?
                number += currDigit; // take next multiplication
            }
            if (multiplication) {
                iDigits.add(currDigit);
            }
        }
        int[] result = new int[iDigits.size()];
        for (int i = 0; i < result.length; i++) {
            result[i] = iDigits.get(i);
        }
        return result;
    }

    private static String changeBaseDigitsOnlyWithSeparators(int number, int targetBase) {
        List<Integer> digits = new ArrayList<>(32);
        while (number > 0) {
            int digit = number % targetBase; //it is a 'digit' in new base which actually may consist of two 0-9 digits
            digits.add(digit);
            number /= targetBase;
        }
        // we need to reverse what we got
        StringBuilder sb = new StringBuilder();
        for (int i = digits.size() - 1; i >= 0; i--) {
            sb.append(digits.get(i)).append(";");
        }
        return sb.toString();
    }

    private static int convertToBaseAndSumUpDigits(int number, int targetBase) {
        int sum = 0;
        while (number > 0) {
            int digit = number % targetBase; //it is a 'digit' in new base which actually may consist of two 0-9 digits
            sum+=digit;
            number /= targetBase;
        }
        return sum;
    }



}

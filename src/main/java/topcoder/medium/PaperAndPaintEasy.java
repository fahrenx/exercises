package topcoder.medium;

/**
 * SRM 441
 * lessons learnt:
 * - good to split problems into smaller ones, create auxiliary algorithms, utils
 * - use real objects to help your imagination and simulate behaviour of algorithm
 * - while implementing intersection - better to do a lot of if's and analyse case by case than oversmart yourself
 * and simplify formulas quickly only to find out later that they don't cover some simple cases
 * - while implementing intersection - writing tests can be great way of checking corner cases for algorithms, no doubt
 * - it is extremely important to check out your algorithm on a few different examples
 */
public class PaperAndPaintEasy {

    // TODO fix me - case when folding over edge...and some others
    public static void main(String[] args) {
//        System.out.println(computeArea(5, 6, 2, 2, 1, 1, 3, 2)); // 21
//        System.out.println(computeArea(12, 12, 7, 3, 3, 1, 6, 2)); // 124
//        System.out.println(computeArea(3, 13, 1, 0, 1, 8, 2, 12)); // 35
        System.out.println(computeArea(4, 5, 4, 0, 0, 0, 1, 1)); // 19
//        System.out.println(computeArea(4, 8, 3, 0, 1, 1, 3, 2)); // 30
    }

    public static long computeArea(int width, int height, int xfold, int cnt, int x1, int y1, int x2, int y2) {
        long paintedSquareArea = (x2 - x1) * (y2 - y1);
        long totalArea = width * height;

        long intersectionWithFolded = intersectionArea(xfold, 0, xfold + xfold, height, xfold + x1, y1, xfold + x2, y2);
        long areaPaintedByVerticalFold = paintedSquareArea * cnt;

        long paintedArea = paintedSquareArea + areaPaintedByVerticalFold + (intersectionWithFolded * (cnt + 1));

        return totalArea - paintedArea;
    }

    public static long intersectionArea(int x11, int y11, int x12, int y12,
                                        int x21, int y21, int x22, int y22) {
        if (x21 > x12 || x22 < x11 || y21 > y12 || y22 < y11) { // no overlap
            return 0;
        }
        int xOverlap;
        if (x22 > x12) { // typical, second rectangle is to the right
            xOverlap = Math.max(0, x12 - x21);
        } else {        // non-typical
            xOverlap = Math.max(0, x22 - x11);
        }
        int shortestXLength = Math.min(x22 - x21, x12 - x11);   // case when one rectangle is inside another
        xOverlap = Math.min(xOverlap, shortestXLength);

        // TODO refactor to DRY?
        // complete symmetry
        int yOverlap;
        if (y22 > y12) {
            yOverlap = Math.max(0, y12 - y21);
        } else {
            yOverlap = Math.max(0, y22 - y11);
        }
        int shortestYLength = Math.min(y22 - y21, y12 - y11);
        yOverlap = Math.min(yOverlap, shortestYLength);     // case when one rectangle is inside another

        return xOverlap * yOverlap;
    }


}

package topcoder.small;

//SRM 144 DIV 2
public class Time {


    public static void main(String[] args) {
        System.out.println(whatTime(86399));
    }

    public static String whatTime(int secsFromMidnight){
        return secsFromMidnight/3600+":"+secsFromMidnight/60%60+":"+secsFromMidnight%60;
    }

}

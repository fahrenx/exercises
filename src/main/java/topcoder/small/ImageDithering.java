package topcoder.small;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import utils.Performance;

import java.io.IOException;
import java.util.HashSet;
import java.util.Set;


/**
 * SRM 145
 * lessons learnt:
 *  - blackhole is needed, jmh is very useful
 *  - boolean array is a nice trick and worked much faster in this case
 *  - HashSet does not build underlying table eagerly
 *  - initial capacity for given size has to be calculated with formula cap/load_fac +1
 *  - reference-like optimizations as I suspected are already done by compiler
 *  - readability != performance
 *  - description can be long but actual problem can be super easy
 *  - it is good to learn at other people's code - new trick learn and also noticed better naming
 */
public class ImageDithering {


    public static void main2(String[] args) {
        int result = count("BW", new String[]{"AAAAAAAA",
                "ABWBWBWA",
                "AWBWBWBA",
                "ABWBWBWA",
                "AWBWBWBA",
                "AAAAAAAA"});
        System.out.println(result);
    }

    public static void main(String[] args) throws IOException, RunnerException {
        Options options = Performance.defaultJmhOptions(ImageDithering.class);
        new Runner(options).run();
    }

    @Benchmark
    public void performanceTest(Blackhole bh) {
        bh.consume(
                ImageDithering.count("ACEGIKMOQSUWY",
                        new String[]{"ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWX",
                                "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWX",
                                "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWX",
                                "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWX",
                                "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWX",
                                "ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWX"})
        );
        bh.consume(
                ImageDithering.count("BW", new String[]{"AAAAAAAA",
                        "ABWBWBWA",
                        "AWBWBWBA",
                        "ABWBWBWA",
                        "AWBWBWBA",
                        "AAAAAAAA"})
        );
    }


    /**
     * mine
     */
    public static int count(String dither, String[] picture) {
        Set<Character> ditherSet = new HashSet<>((int) (dither.length() / .75f) + 1);
        for (char c : dither.toCharArray()) {
            ditherSet.add(c);
        }
        int result = 0;
        for (String el : picture) {
            for (char c : el.toCharArray()) {
                if (ditherSet.contains(c)) {
                    result++;
                }
            }
        }
        return result;
    }

    /**
     * copied, slower than mine
     */
    public static int count2(String dithered, String[] screen) {
        int result = 0;
        int width = screen[0].length();
        int height = screen.length;
        CharSequence chr;
        for (int i = 0; i < height; i++) {
            for (int j = 0; j < width; j++) {
                chr = String.valueOf(screen[i].charAt(j));
                if (dithered.contains(chr)) {
                    result++;
                }
            }
        }
        return result;
    }


    /**
     * copied, 5x faster than mine, clever!
     */
    public static int count3(String dithered, String[] screen) {
        boolean dither[] = new boolean[26];
        for (int i = 0; i < dithered.length(); i++) {
            dither[dithered.charAt(i) - 'A'] = true;
        }
        int result = 0;
        for (int i = 0; i < screen.length; i++) {
            for (int j = 0; j < screen[i].length(); j++) {
                if(dither[screen[i].charAt(j)-'A']){
                    result++;
                }
            }
        }
        return result;
    }


}

package topcoder.small;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;

import java.io.IOException;
import java.util.HashMap;

/**
 * lessons learnt:
 *  - trick for shorter and maybe even nicer code when finding max in a loop: Math.max(max, sums[i]); instead of classic if
 *  - introducing map when array is sufficient can lead to 10x slower code
 *  - use iterator over array if you don't need the index, so much nicer code
 *  - extending array a bit to avoid subtraction for correcting indexing can be a good tradeoff
 *  - use first element as initial max/min
 */
public class YahtzeeScore {

    public static void main2(String[] args) {
        System.out.println(maxPoints(new int[]{6, 4, 1, 1, 3}));
        System.out.println(maxPoints(new int[]{5, 3, 5, 3, 3}));
    }

    public static void main(String[] args) throws IOException, RunnerException {
        Options opt = new OptionsBuilder()
                .include(ImageDithering.class.getSimpleName())
                .warmupIterations(5)
                .measurementIterations(5)
                .forks(1)
                .build();
        new Runner(opt).run();
    }

    @Benchmark
    public void performanceTest(Blackhole bh) {
        bh.consume(maxPoints(new int[]{6, 4, 1, 1, 3}));
        bh.consume(maxPoints(new int[]{5, 3, 5, 3, 3}));
    }

    /**
     * mine
     * tp:324104, after max=sums[0] similar (301286)
     * after extending array still similar
     */
    public static int maxPoints(int[] toss) {
        int[] sums = new int[7];
        for (int i : toss) {
            sums[i] += i;
        }
        int max = sums[1];
        for (int i = 2; i < sums.length; i++) {
            Math.max(max, sums[i]);
        }
        return max;
    }

    /**
     * mine - on map, bad
     * tp: 308956, almost ten times slower than on array
     */
    public static int maxPoints2s(int[] toss) {
        HashMap<Integer, Integer> sums = new HashMap<>();
        for (int i = 0; i < toss.length; i++) {
            if (sums.containsKey(toss[i])) {
                sums.put(toss[i], sums.get(toss[i]) + toss[i]);
            } else {
                sums.put(toss[i], toss[i]);
            }
        }
        int max = -1;
        for (int i : sums.values()) {
            if (i > max) {
                max = i;
            }
        }
        return max;
    }


}

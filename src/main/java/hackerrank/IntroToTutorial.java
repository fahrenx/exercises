package hackerrank;


import java.util.Scanner;

public class IntroToTutorial {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int val = scanner.nextInt();
        int size = scanner.nextInt();
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }
        System.out.println(solve(arr, val));
    }

    public static int solve(int[] arr, int val){
        for (int i = 0; i < arr.length; i++) {
            if(arr[i] == val) return i;
        }
        return -1;
    }


}

package hackerrank;

import java.util.Scanner;

public class InsertionSort1 {


    public static void main2(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] arr = new int[size];
        for (int i = 0; i < size; i++) {
            arr[i] = scanner.nextInt();
        }
        solve(arr);
    }

    public static void main(String[] args) {
        solve(new int[]{8,9,10,12,13,1});
    }

    public static void solve(int[] ar) {
        if (ar.length == 0) {
            return;
        }
        int val = ar[ar.length - 1];
        boolean found = false;
        int j = ar.length - 2;
        while (!found && j>=0)   {
            ar[j + 1] = ar[j];
            if (ar[j] < val) {
                ar[j+1] = val;
                found = true;
            }
            printArray(ar);
            j--;
        }
        if(!found){
            ar[0] = val;
            printArray(ar);
        }
    }

    private static void printArray(int[] arr) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            sb.append(arr[i]);
            if (i != arr.length - 1) {
                sb.append(" ");
            }
        }
        System.out.println(sb.toString());
    }


}

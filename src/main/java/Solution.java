import java.io.ByteArrayInputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;

class Solution2 {

    /**
     *
     * @param args
     * @throws Exception
     */
    public static void main(String args[] ) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        Scanner sc = new Scanner( new ByteArrayInputStream("aa    bb".getBytes(StandardCharsets.UTF_8)));
        if(sc.hasNextLine()){
            String[] input = sc.nextLine().split("\\s+");
            sort(input);
        } else {
            System.out.print("");
        }
    }

    public static void sort(String[] input){
        if(input.length == 0) {
            System.out.println("");
            return;
        }
        // splitting
        List<Integer> numbers = new ArrayList<>();
        List<String> words = new ArrayList<>();
        for(String el: input){
            if(isNumber(el)){
                numbers.add(Integer.valueOf(el));
            } else {
                words.add(el);
            }
        }
        // sorting
        Collections.sort(numbers);
        Collections.sort(words);
        // merging & printing
        Iterator<Integer> nIt = numbers.iterator();
        Iterator<String> wIt = words.iterator();
        // since we only need to print out the results, no need to modify input structure in place or create a new one with final results
        // this way we can save some memory (at some cost of readability)
        for (int i=0; i< input.length; i++) {
            if (isNumber(input[i])) {
                System.out.print(nIt.next());
            } else {
                System.out.print(wIt.next());
            }
            if(i< input.length-1) {
                System.out.print(" ");
            }
        }

    }

    private static boolean isNumber(String el){
        // check if it is a number - due to assumptions stated (a-z for word)- it is enough to check only first character
        // TODO double check no possibility of empty word
        char firstChar = el.charAt(0);
        return Character.isDigit(firstChar) || firstChar == '-';
    }
}
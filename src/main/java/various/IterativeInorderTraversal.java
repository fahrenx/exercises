package various;


import java.util.LinkedList;

public class IterativeInorderTraversal {

    public static void main(String[] args) {
        Node root = new Node(10,
                new Node(4, 1, 5),
                new Node(12, 11, 18));
        Node invalidTreeRoot = new Node(10,
                new Node(4, 1, 5),
                new Node(-1, 11, 18));
//
//    traverseInOrder(root);
        traverseInOrderIteratively(root);
        System.out.println("---");
        System.out.println(findKSmallestInBST(root, 2));
        System.out.println(validateBST(invalidTreeRoot));
    }

    private static void traverseInOrder(Node root) {
        if (root == null) return;
        traverseInOrder(root.left);
        System.out.println(root.key);
        traverseInOrder(root.right);
    }

    private static void traverseInOrderIteratively(Node root) {
        LinkedList<Node> stack = new LinkedList<>();
        while (root != null || !stack.isEmpty()) {
            // go down the left subtree
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            // print current
            root = stack.pop();
            System.out.println(root.key);
            // prepare for processing left subtree of right subtree root
            root = root.right;
        }
    }

    // simple modification of iterative in-order traversal
    private static int findKSmallestInBST(Node root, int k) {
        LinkedList<Node> stack = new LinkedList<>();
        while (root != null || !stack.isEmpty()) {
            // go down the left subtree
            while (root != null) {
                stack.push(root);
                root = root.left;
            }
            // print current
            root = stack.pop();
            if (--k == 0) return root.key;
            // prepare for processing left subtree of right subtree root
            root = root.right;
        }
        return -1;
    }


    private static boolean validateBST(Node root) {
        return validateBSTRec(root, null, null);
    }

    private static boolean validateBSTRec(Node root, Node min, Node max) {
        if (root == null) return true;
        if ((min != null && root.key < min.key) || (max != null && root.key > max.key)) return false;
        return validateBSTRec(root.left, min, root) && validateBSTRec(root.right, root, max);
    }

    public static class Node {
        public final int key;
        public Node left;
        public Node right;

        public Node(int key) {
            this.key = key;
        }

        public Node(int key, int left, int right) {
            this.key = key;
            this.left = new Node(left);
            this.right = new Node(right);
        }

        public Node(int key, Node left, Node right) {
            this.key = key;
            this.left = left;
            this.right = right;
        }
    }
}

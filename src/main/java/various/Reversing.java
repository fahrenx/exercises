package various;

public class Reversing {

    static String reverse(String s){
        if(s.length()<=1) return s;
        char[] a = s.toCharArray();
        for(int i=0; i< a.length/2; i++){
            char temp = a[i];
            a[i] = a[a.length-1-i];
            a[a.length-1-i] = temp;
        }
        return new String(a);
    }

    public static void main(String[] args) {
        System.out.println(reverse("abc"));
    }
}

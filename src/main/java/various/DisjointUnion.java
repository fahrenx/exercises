package various;

import sun.jvm.hotspot.utilities.Assert;

import java.util.HashMap;
import java.util.Map;

public class DisjointUnion {

    private final Map<Integer, Element> m = new HashMap<>();

    private static class Element {
        int id;
        int rank;
        Element parent;

        Element(int id, int rank, Element parent){
            this.id = id;
            this.rank = rank;
            this.parent = parent;
        }

        Element(int id, int rank){
            this(id, rank, null);
        }
    }

    public void makeSet(int id){
        m.put(id,  new Element(id, 0));
    }

    public Element findSet(int id) {
        Element el = m.get(id);
        while(el.parent!=null) {
            el = el.parent;
        }
        return el;
    }

    public boolean union(int a, int b) {
        Element setA = findSet(a);
        Element setB = findSet(b);
        if(setA == setB) return false; // already merged
        // union by rank
        if(setA.rank < setB.rank)  setA.parent = setB;
        else setB.parent= setA;
        return true;
    }

    public static void main(String[] args) {
        DisjointUnion du = new DisjointUnion();
        du.makeSet(1);
        du.makeSet(2);
        du.makeSet(3);
        du.makeSet(4);
        du.union(1, 2);
        Assert.that(du.findSet(1) == du.findSet(2), "1 and 2 belong to the same set");
        Assert.that(du.findSet(1) != du.findSet(3), "1 and 3 belong to different set");
        Assert.that(du.findSet(3) != du.findSet(4), "3 and 4 belong to different set");
    }
}

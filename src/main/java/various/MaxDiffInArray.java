package various;

import java.util.Arrays;

// Given an array,find the maximum j – i such that arr[j] > arr[i]
public class MaxDiffInArray {

  public static void main(String[] args) {
    int[] a = {6, 5, 4, 3, 2, 1};
    System.out.println(findMaxFaster(a));
    System.out.println(findMax(a));

  }


  private static int findMaxFaster(int[] a) {
    int[] lMin = new int[a.length];
    int[] rMax = new int[a.length];

    int min = a[0];
    lMin[0] = min;
    for(int i=1;i<a.length;i++) {
      min = Math.min(min, a[i]);
      lMin[i] = min;
    }


    int max = a[a.length - 1];
    rMax[0] = max;
    for(int i=a.length - 1;i >= 0; i--) {
      max = Math.max(a[i], max);
      rMax[i] = max;
    }
    System.out.println(Arrays.toString(lMin));
    System.out.println(Arrays.toString(rMax));

    int i=0;
    int j=1;
    int maxDiff = 0;
    // i and j start both on left most position, next to each other
    // i moves steady to the right, j starts tries to move to the right too
    while(maxDiff < a.length -1 - i){
      if(i == 0 || lMin[i] < lMin[i-1]) { // min just got smaller, chance to move j to the right, otherwise don't bother
        while (j < a.length && rMax[j] > lMin[i]) {
          maxDiff = Math.max(j-i, maxDiff);
          j++;
        }
      }
      i++;
    }
    return maxDiff;
  }

  // Given an array,find the maximum j – i such that arr[j] > arr[i]
  private static int findMax(int[] a) {
    int i=0;
    int max = 0;
    while(i < a.length-1 && a.length-1-i > max){
      for(int j=i+1; j<a.length; j++){
        if(a[j] > a[i]){
          max = Math.max(j-i, max);
        }
      }
      i++;
    }
    return max;
  }

}

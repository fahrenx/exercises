package various;

import org.assertj.core.api.Assertions;

public class Search {

    public static void main(String[] args) {
        int[] a = new int[]{1,2,3,4,5};
        int[] b = new int[]{1,10};
        Search search = new Search();
        Assertions.assertThat(search.findMedianSortedArrays(a, b)).isGreaterThan(-10);

    }

    public double findMedianSortedArrays(int[] nums1, int[] nums2) {
        double m1 = median(nums1);
        double m2 = median(nums2);

        int m1n2index = binarySearch(nums2, m1, 0, nums2.length);
        int m2n1index = binarySearch(nums1, m2, 0, nums1.length);
        System.out.println("medians: " + m1 + " ; " + m2);

        System.out.println("medians positions - m1 in n2: " + m1n2index + " ; m2 in n1 : " + m2n1index);


        // let's assume m2 always is merged into m1 (it is symmetrical but easier to decide on one)

        int newElementsOnRight = nums2.length - m1n2index;
        int newElementsOnLeft = nums2.length - newElementsOnRight;
        int m1Shift = newElementsOnLeft - newElementsOnRight;


        return -1;
    }

    double median(int[] a){
        if(a.length == 0) return 0;
        int mid = a.length / 2;
        if(a.length % 2 == 0) {
            return (((double)(a[mid] + a[mid-1])) / 2.0);
        } else return a[mid];
    }

    int binarySearch(int[] a, double num, int start, int end){
        if(end <= start) return start;
        int mid = start + (end-start)/2;
        System.out.println( start + " : "  + end + " : " + mid);
        if(num == a[mid]) return mid;
        else if(num > a[mid]){
            return binarySearch(a, num, mid+1, end);
        } else {
            return binarySearch(a, num, start, mid - 1);
        }
    }
}

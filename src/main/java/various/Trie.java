package various;

import org.assertj.core.api.Assertions;

import java.util.*;

public class Trie {

    Node root = new Node('-', false, -1);

    static class Node{
        Map<Character, Node> next = new HashMap<>();
        char c;
        boolean isWord;
        int index;

        public Node(char c, boolean isWord, int index, Map<Character, Node> next) {
            this.c = c;
            this.isWord = isWord;
            this.index = index;
            this.next = next;
        }

        public Node(char c, boolean isWord, int index) {
            this.c = c;
            this.isWord = isWord;
            this.index = index;
        }
    }

    public boolean exists(String str){
        return findExact(str) != null;
    }

    // returns null if no exact match found
    public Node findExact(String str){
        int i=0;
        Node n = root;
        while(i < str.length() && (n = n.next.get(str.charAt(i))) != null) i++;
        if(n!=null && n.isWord) return n;
        else return null;
    }

    // returns list of indexes
    public List<Integer> findByPrefix(String str){
        int i=0;
        Node n = root;
        while(i < str.length() && (n = n.next.get(str.charAt(i))) != null) i++;
        if(n==null){
            return new ArrayList<>();
        } else {
            return findAllStartingAt(n);
        }
    }

    private List<Integer> findAllStartingAt(Node n) {
        List<Integer> ls = new LinkedList<>();
        if(n.isWord) ls.add(n.index);
        Collection<Node> nextElements = n.next.values();
        for(Node ne: nextElements){
            ls.addAll(findAllStartingAt(ne));
        }
        return ls;
    }

    public boolean add(String str, int index) {
        int i=0;
        Node n = root;
        Node prev = root;
        // searching for the place
        while(i < str.length() && (n = n.next.get(str.charAt(i))) != null){
            prev = n;
            i++;
        }
        if(i == str.length()) { // the last letter was reached
            boolean existed = n.isWord;
            n.isWord = true;
            return !existed;
        }
        // didn't exist
        prev.next.put(str.charAt(i), createNodeChain(str.substring(i), index));
        return true;
    }

    private boolean remove(String str){
        Node node = findExact(str);
        if(node == null) {
            return false;
        } else {
            node.isWord = false;
            return true;
        }
    }

    private Node createNodeChain(String s, int index){
        // going from last to first letter
        int j=s.length() - 1 ;
        Node n = new Node(s.charAt(j), true, index);
        j--;
        while(j>=0){
            Node newNode = new Node(s.charAt(j--), false, -1);
            newNode.next.put(n.c, n);
            n = newNode;
        }
        return n;
    }


    public static void main(String[] args) {
        Trie trie = new Trie();
        // basics - adding and checking if exists
        trie.add("kat", 1);
        trie.add("katar", 2);
        trie.add("kasia", 3);

        Assertions.assertThat(trie.exists("kat")).isTrue();
        Assertions.assertThat(trie.exists("katar")).isTrue();
        Assertions.assertThat(trie.exists("kasia")).isTrue();
        Assertions.assertThat(trie.exists("katapulta")).isFalse();

        // test of adding multiple times same thing
        Assertions.assertThat(trie.add("katapulta", 4)).isTrue();
        Assertions.assertThat(trie.add("katapulta", 4)).isFalse();
        Assertions.assertThat(trie.exists("katapulta")).isTrue();

        // test of removal
        Assertions.assertThat(trie.remove("kat")).isTrue();
        Assertions.assertThat(trie.remove("kat")).isFalse();
        Assertions.assertThat(trie.remove("bak")).isFalse();

        Assertions.assertThat(trie.exists("kat")).isFalse();
        Assertions.assertThat(trie.exists("katar")).isTrue();
        Assertions.assertThat(trie.exists("kasia")).isTrue();
        Assertions.assertThat(trie.exists("katapulta")).isTrue();

        // find by prefix
        Assertions.assertThat(trie.findByPrefix("ka")).hasSize(3);
    }
}

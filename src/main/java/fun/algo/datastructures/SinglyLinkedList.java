package fun.algo.datastructures;

public class SinglyLinkedList<E> {

    public Node<E> head;

    public SinglyLinkedList() {
    }

    public SinglyLinkedList(Node<E> head) {
        this.head = head;
    }

    public static class Node<E> {

        public E value;
        public Node<E> next;

        public Node(E value) {
            this(value, null);
        }

        public Node(E value, Node<E> next) {
            this.value = value;
        }

        @Override
        public String toString() {
            return "Node[" + value + "] ";
        }

    }

    public void append(Node<E> newNode) {
        if (head == null) {
            head = newNode;
        } else {
            Node<E> node = head;
            while (node.next != null) {
                node = node.next;
            }
            node.next = newNode;
        }
    }

    @Override
    public String toString() {
        if (head == null) {
            return "empty " + super.toString();
        }


        StringBuilder sb = new StringBuilder();
        sb.append("{ ");
        Node<E> node = head;
        while (node != null) {
            sb.append(node);
            sb.append(" ");
            node = node.next;
        }
        sb.append("}");
        return sb.toString();
    }
}

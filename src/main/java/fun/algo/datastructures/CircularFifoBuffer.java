package fun.algo.datastructures;

import java.util.NoSuchElementException;

// implementation based on stacks - for fun - could be better directly on array
public class CircularFifoBuffer<E> {

    Stack<E> stack = new Stack<E>();
    final int size;

    public CircularFifoBuffer(int size) {
        this.size = size;
        stack = new Stack<E>(size);
    }

    public void add(E node) {
        if (stack.getNumberOfElements() >= size) {
            Stack<E> tempStack = new Stack<E>(size);
            while (!stack.isEmpty()) {
                tempStack.push(stack.pop());
            }
            tempStack.pop();
            while (!tempStack.isEmpty()) {
                stack.push(tempStack.pop());
            }
        }
        stack.push(node);
    }

    public E pop() {
        return stack.pop();
    }

    public E getKthLast(int kth) {
        assert (kth != 0);

        if (kth > stack.getNumberOfElements()) {
            throw new NoSuchElementException();
        }

        Stack<E> tempStack = new Stack<E>(size);
        int i = 1;
        while (i < kth && !stack.isEmpty()) {
            tempStack.push(stack.pop());
            i++;
        }
        E element = stack.pop();
        stack.push(element);
        while (i > 1 && !tempStack.isEmpty()) {
            stack.push(tempStack.pop());
            i--;
        }
        return element;
    }
}

package fun.algo.datastructures;

import java.util.NoSuchElementException;

public class Stack<E> {

    private static final int DEFAULT_SIZE = 10;
    private static final int RESIZE_FACTOR = 2;
    private E[] elements;
    private int indexOfTopElement;

    public Stack() {
        this(DEFAULT_SIZE);
    }

    public Stack(int size) {
        // TODO I know that something is wrong with that but I am not sure what exactly it is
        this.elements = (E[]) new Object[size];
        indexOfTopElement = -1;
    }

    public void push(E element) {
        if (indexOfTopElement == elements.length - 1) {
            E[] newElementsArray = (E[]) new Object[elements.length * RESIZE_FACTOR];
            System.arraycopy(element, 0, newElementsArray, 0, elements.length);
        }
        indexOfTopElement++;
        elements[indexOfTopElement] = element;
    }

    public E pop() {
        if (isEmpty()) {
            throw new NoSuchElementException();
        }
        E element = elements[indexOfTopElement];
        elements[indexOfTopElement] = null;
        indexOfTopElement--;
        return element;
    }

    public boolean isEmpty() {
        return indexOfTopElement == -1;
    }

    public int getNumberOfElements() {
        return indexOfTopElement + 1;
    }

}

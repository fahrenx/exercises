package fun.algo.datastructures;

public class Printers {

    public static void print2Darray(int[][] array) {
        System.out.println();
        for (int y = 0; y < array[0].length; y++) {
            for (int x = 0; x < array.length; x++) {
                System.out.printf("%5d", array[x][y]);
            }
            System.out.println();
        }
    }

}

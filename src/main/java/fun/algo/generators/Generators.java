package fun.algo.generators;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import fun.algo.datastructures.SinglyLinkedList;

public class Generators {

    /*
    int[][] arrayCopy = new int[3][3];
    for (int i = 0; i < array.length; i++) {
        System.arraycopy(array[i], 0, arrayCopy[i], 0, array[i].length);
    }
    int[][] arrayClone = array.clone();
    for (int i = 0; i < arrayClone.length; i++) {
        arrayClone[i] = array[i].clone();
    }
    */

    public static int[][] generateIntArrayWithRandomElements(int rowsNumber, int columnsNumber, int maxValue) {
        Random random = new Random();
        int[][] array = new int[columnsNumber][rowsNumber];
        for (int x = 0; x < array.length; x++) {
            for (int y = 0; y < array[x].length; y++) {
                array[x][y] = random.nextInt(maxValue+1);
            }
        }
        return array;
    }

    public static int[][] generateIntArrayWithRepeatedNumber(int rowsNumber, int columnsNumber, int number) {
        int[][] array = new int[columnsNumber][rowsNumber];
        for (int x = 0; x < array.length; x++) {
            for (int y = 0; y < array[x].length; y++) {
                array[x][y] = number;
            }
        }
        return array;
    }

    public static <E> SinglyLinkedList<E> generateSinglyLinkedList(Class<E> clazz, int length) throws InstantiationException, IllegalAccessException {
    
        SinglyLinkedList<E> singlyLinkedList = new SinglyLinkedList<E>();
        SinglyLinkedList.Node<E> head = new SinglyLinkedList.Node<E>(clazz.newInstance(), null);
        singlyLinkedList.head = head;
        SinglyLinkedList.Node<E> prev = head;
        for (int i = 0; i < length; i++) {
            SinglyLinkedList.Node<E> newNode = new SinglyLinkedList.Node<E>(clazz.newInstance(), null);
            prev.next = newNode;
            prev = newNode;
        }
        return singlyLinkedList;
    }

    // TODO somehow merge with second method
    public static <E> SinglyLinkedList<E> generateSinglyLinkedList(List<E> values) {
        SinglyLinkedList<E> singlyLinkedList = new SinglyLinkedList<E>();
        SinglyLinkedList.Node<E> head = new SinglyLinkedList.Node<E>(values.get(0), null);
        singlyLinkedList.head = head;
        SinglyLinkedList.Node<E> prev = head;
        for (int i = 1; i < values.size(); i++) {
            SinglyLinkedList.Node<E> newNode = new SinglyLinkedList.Node<E>(values.get(i), null);
            prev.next = newNode;
            prev = newNode;
        }
        return singlyLinkedList;
    }

    public static List<Integer> generateNaturalNumberSerie(int max) {
        List<Integer> result = new LinkedList<Integer>();
        for (int i = 1; i <= max; i++) {
            result.add(i);
        }
        return result;
    }


}

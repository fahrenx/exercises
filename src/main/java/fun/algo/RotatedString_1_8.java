package fun.algo;

public class RotatedString_1_8 {

    public static void main(String[] args) {
        System.out.println(isRotated("waterbottle", "erbottlewat"));
    }

    public static boolean isRotated(String str1, String str2) {
        if (str1.length() != str2.length()) {
            return false;
        }
        if (str1.equals(str2)) {
            return true;
        }
        String rolledOutString = str1 + str1.substring(0, str1.length() - 1);
        return rolledOutString.contains(str2);// substring() given in the exercise description
    }

}

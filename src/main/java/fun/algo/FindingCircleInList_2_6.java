package fun.algo;

import fun.algo.datastructures.SinglyLinkedList;

public class FindingCircleInList_2_6 {

    public static void main(String[] args) {

    }

    public <E> SinglyLinkedList.Node<E> getNodeAtTheBeginningOfCircle(SinglyLinkedList.Node<E> listHead) {

        SinglyLinkedList.Node<E> runner = listHead;
        SinglyLinkedList.Node<E> currentNode = listHead;
        do {
            currentNode = currentNode.next;
            runner = runner.next.next;
        } while (runner == currentNode);
        return runner;
    }

}

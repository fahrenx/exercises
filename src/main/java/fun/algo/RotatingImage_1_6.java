package fun.algo;

import fun.algo.datastructures.Printers;

public class RotatingImage_1_6 {

    private static final int IMAGE_SIZE = 9;

    public static void main(String[] args) {
        
        int[][] image = createImage(IMAGE_SIZE);
        Printers.print2Darray(image);
        int[][] rotatedImage = rotateImage(image);
        Printers.print2Darray(rotatedImage);
    }


    private static int[][] createImage(int imageSize) {
        int[][] image = new int[imageSize][IMAGE_SIZE];
        int value = 1;
        for (int y = 0; y < image.length; y++) {
            for (int x = 0; x < image.length; x++) {
                image[x][y] = value;
                value++;
            }
        }
        return image;
    }

    private static int[][] rotateImage(int[][] image) {
        if (image.length <= 1)
            return image;

        for (int layer = 0; layer < image.length / 2; layer++) {
            int temp;
            for (int j = 0 + layer; j < image.length - 1 - layer; j++) {
                temp = image[image.length - 1 - layer][j];
                image[image.length - 1 - layer][j] = image[j][layer];
                image[j][layer] = temp;
            }
            
            for (int j = 0 + layer; j < image.length - 1 - layer; j++) {
                temp = image[image.length - 1 - layer - j][image.length - 1 - layer];
                image[image.length - 1 - layer - j][image.length - 1 - layer] = image[j][layer];
                image[j][layer] = temp;
            }

            for (int j = 0 + layer; j < image.length - 1 - layer; j++) {
                temp = image[layer][image.length - 1 - layer - j];
                image[layer][image.length - 1 - layer - j] = image[j][layer];
                image[j][layer] = temp;
            }

        }

        return image;
    }

}

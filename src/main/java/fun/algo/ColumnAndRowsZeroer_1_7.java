package fun.algo;

import java.util.HashSet;
import java.util.Set;

import fun.algo.datastructures.Printers;
import fun.algo.generators.Generators;


public class ColumnAndRowsZeroer_1_7 {

    public static void main(String[] args) {
        int[][] array = Generators.generateIntArrayWithRandomElements(3, 3, 5);
        int[][] arrayCopy = new int[3][3];
        for (int i = 0; i < array.length; i++) {
            System.arraycopy(array[i], 0, arrayCopy[i], 0, array[i].length);
        }

        Printers.print2Darray(array);
        int[][] result = zeroColumnsAndRowsUsingSets(array);
        Printers.print2Darray(result);
        int[][] result2 = zeroColumnsAndRowsUsingAdditionalArray(arrayCopy);
        Printers.print2Darray(result2);
    }
    
    public static int[][] zeroColumnsAndRowsUsingSets(int[][] array) {

        Set<Integer> rows = new HashSet<Integer>();
        Set<Integer> columns = new HashSet<Integer>();

        for (int x = 0; x < array.length; x++) {
            for (int y = 0; y < array[x].length; y++) {
                if (array[x][y] == 0) {
                    columns.add(x);
                    rows.add(y);
                }
            }
        }
        for (int rowNumber : rows) {
            for (int x = 0; x < array.length; x++) {
                array[x][rowNumber] = 0;
            }
        }
        for (int columnNumber : columns) {
            for (int y = 0; y < array[columnNumber].length; y++) {
                array[columnNumber][y] = 0;
            }
        }

        return array;
    }
    
    public static int[][] zeroColumnsAndRowsUsingAdditionalArray(int[][] array) {
        int[][] result = Generators.generateIntArrayWithRepeatedNumber(array[0].length, array.length, -1);
        for (int x = 0; x < array.length; x++) {
            for (int y = 0; y < array[x].length; y++) {
                if (array[x][y] == 0) {
                    for (int i = 0; i < array[x].length; i++) {
                        result[x][i] = 0;
                    }
                    for (int i = 0; i < result.length; i++) {
                        result[i][y] = 0;
                    }

                }
            }
        }
        for (int x = 0; x < result.length; x++) {
            for (int y = 0; y < result[x].length; y++) {
                if (result[x][y] == -1) {
                    result[x][y] = array[x][y];
                }
            }
        }

        return result;
    }


}

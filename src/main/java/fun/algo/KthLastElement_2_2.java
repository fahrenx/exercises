package fun.algo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

import fun.algo.datastructures.SinglyLinkedList;
import fun.algo.datastructures.CircularFifoBuffer;
import fun.algo.generators.Generators;

public class KthLastElement_2_2 {

    public static void main(String[] args) throws InstantiationException, IllegalAccessException {

        List<Integer> values = new ArrayList<Integer>(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10));
        SinglyLinkedList<Integer> singlyLinkedListed = Generators.generateSinglyLinkedList(values);

        SinglyLinkedList.Node<Integer> kthLastElement = getKthLastElement(singlyLinkedListed, 4);
        System.out.println(kthLastElement);
        SinglyLinkedList.Node<Integer> kthLastElement2 = getKthLastElement2(singlyLinkedListed, 4);
        System.out.println(kthLastElement2);
        SinglyLinkedList.Node<Integer> kthLastElement3 = getKthLastElement3(singlyLinkedListed, 4);
        System.out.println(kthLastElement3);
    }

    /**
     * complicated solution - using CircularFifoBuffer 2.2
     */
    private static <E> SinglyLinkedList.Node<E> getKthLastElement(SinglyLinkedList<E> list, int k) {
        CircularFifoBuffer<SinglyLinkedList.Node<E>> lastElements = new CircularFifoBuffer<SinglyLinkedList.Node<E>>(k);
        SinglyLinkedList.Node<E> node = list.head;
        int size = 0;
        while (node != null) {
            lastElements.add(node);
            size++;
            node = node.next;
        }
        if (size < k) {
            return null;
        }
        return lastElements.getKthLast(k);
    }

    /**
     * simplified solution - better time and memory efficiency 2.2
     */
    private static <E> SinglyLinkedList.Node<E> getKthLastElement2(SinglyLinkedList<E> list, int k) {
        SinglyLinkedList.Node<E> node = list.head;
        SinglyLinkedList.Node<E> rememberedNode = node;
        int size = 0;
        while (node != null) {
            size++;
            node = node.next;
            if (size >= k && node != null) {
                rememberedNode = rememberedNode.next;
            }
        }
        if (size >= k) {
            return rememberedNode;
        } else {
            throw new NoSuchElementException();
        }
    }

    /**
     * 2.2 - even simpler - counter based
     */
    private static <E> SinglyLinkedList.Node<E> getKthLastElement3(SinglyLinkedList<E> list, int k) {
        SinglyLinkedList.Node<E> node = list.head;
        int size = 0;
        while (node != null) {
            size++;
            node = node.next;
        }
        if (size >= k) {
            node = list.head;
            while (size > k) {
                node = node.next;
                size--;
            }
            return node;
        } else {
            throw new NoSuchElementException();
        }
    }


}



package fun.algo.sorting;

import java.util.Arrays;

public class BasicSortings {

    public static void main(String[] args) {
        int[] array = new int[] { 1, 5, 2, 10, 2, 2, 5, 8 };
        insertionSort(array);
        System.out.println(Arrays.toString(array));
        int[] array2 = new int[] { 1, 5, 2, 10, 2, 2, 5, 8 };
        selectionSort(array2);
        System.out.println(Arrays.toString(array2));
        int[] array3 = new int[] { 1, 5, 2, 10, 2, 2, 5, 8 };
        bubbleSort(array3);
        System.out.println(Arrays.toString(array3));
    }

    /**
     * analogy to cards is perfect, remember also that this algorithm, as usual in case of sorting algorithms, enlarge sorted part with each iterations
     * 
     * @param array
     */
    private static void insertionSort(int[] array) {
        for (int current = 1; current < array.length; current++) {
            for (int beingCompared = 0; beingCompared < current; beingCompared++) {
                if (array[current] < array[beingCompared]) {
                    int temp = array[current];
                    for (int k = current; k > beingCompared; k--) { // moving already sorted part to the right, preparing space for new element
                        array[k] = array[k - 1];
                    }
                    array[beingCompared] = temp;
                }
            }
        }
    }

    /**
     * analogy to cards is perfect, remember also that this algorithm, as usual in case of sorting algorithms, enlarge sorted part with each iterations
     * 
     * @param array
     */
    private static void selectionSort(int[] array) {
        for (int i = 0; i < array.length - 1; i++) {
            int minIndex = i;
            for (int j = i + 1; j < array.length; j++) {
                if (array[j] < array[minIndex]) {
                    minIndex = j;
                }
            }
            int temp = array[i];
            array[i] = array[minIndex];
            array[minIndex] = temp;
        }
    }

    /**
     * analogy to cards is perfect, remember also that this algorithm, as usual in case of sorting algorithms, enlarge sorted part with each iterations
     * 
     * @param array
     */
    private static void bubbleSort(int[] array) {
        boolean swap = false;
        do {
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    int temp = array[i + 1];
                    array[i + 1] = array[i];
                    array[i] = temp;
                }
            }
        } while (swap);
    }

}

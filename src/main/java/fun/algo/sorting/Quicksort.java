package fun.algo.sorting;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Quicksort {

    public static void main(String[] args) {
        Integer[] arr = new Integer[] { 100, 123, 3, 422, 123 };

        // quicksort(arr, 0, 4);
        // System.out.println(Arrays.toString(arr));

        List<Integer> result = quicksort2(Arrays.asList(arr));
        for (Integer i : result) {
            System.out.println(i);
        }
    }

    /**
     * nice, but this version is very tricky in terms of off-by-one errors - be very careful it is difficult to learn it by heart
     */
    public static <T extends Comparable<T>> void quicksort(T[] array, int startIndexIncluded, int lastIndexIncluded) {

        T pivot = array[startIndexIncluded + ((lastIndexIncluded - startIndexIncluded) / 2)];
        int i = startIndexIncluded;
        int j = lastIndexIncluded;
        while (i < j) {
            while (array[i].compareTo(pivot) < 0) {
                i++;
            }
            while (array[j].compareTo(pivot) > 0) {
                j--;
            }
            if (i <= j) {
                T temp = array[j];
                array[j] = array[i];
                array[i] = temp;
                i++;
                j--;
            }
        }
        System.out.println(Arrays.toString(array) + " i=" + i + " j=" + j + " pivot=" + pivot + " startIndex=" + startIndexIncluded + " lastindex="
                + lastIndexIncluded);
        if (j > startIndexIncluded) {
            quicksort(array, startIndexIncluded, j);
        }
        if (i < lastIndexIncluded) {
            quicksort(array, i, lastIndexIncluded);
        }

    }

    public static <T extends Comparable<T>> List<T> quicksort2(List<T> list) {

        if (list.size() <= 1) {
            return list;
        }

        List<T> less =new LinkedList<T>();
        List<T> greater =new LinkedList<T>();
        T pivot = list.get(new Random().nextInt(2));
        for(T element :list){
            if (element.compareTo(pivot) >= 0) {
                greater.add(element);
            } else {
                less.add(element);
            }
        }
        System.out.println(greater);
        System.out.println(less);

        ArrayList<T> result = new ArrayList<T>(quicksort2(less));
        result.addAll(quicksort2(greater));
        return result;
    }

}

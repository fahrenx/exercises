package fun.algo.codeeval;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;


public class ReverseWords {

    public static void main(String[] args) throws IOException {
        File file = new File(args[0]);
        BufferedReader in = new BufferedReader(new FileReader(file));
        String line;
        StringBuilder sb = new StringBuilder();
        while ((line = in.readLine()) != null) {
            String[] lineArray = line.split("\\s");
            if (lineArray.length > 0) {
                for (int i = lineArray.length - 1; i >= 0; i--) {
                    sb.append(lineArray[i]).append(" ");
                }
                sb.append("\n");
            }
        }
        System.out.print(sb.toString());
    }

/*
    public static void main2(String[] args) throws IOException, IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        TestingPerformanceUtil.runPerformanceTestsForClass(ReverseWords.class);
    }

    // method: test( runs: 100 ) avg time: 0.24875745999999999 ms
    // decreased to: method: test( runs: 100 ) avg time: 0.20523435 ms
    @TestPerformance(100)
    public static void test() throws IOException {
        main2(new String[] { "reverseWords.txt" });
    }
*/

}

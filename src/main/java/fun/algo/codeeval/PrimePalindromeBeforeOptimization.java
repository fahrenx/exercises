package fun.algo.codeeval;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class PrimePalindromeBeforeOptimization {

    public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {

        // System.out.println(getNumberOfDigits(999));
    }

    public static void testFindBiggestPalindrome() {
        findBiggestPrimePalindrome(1000);
    }

    public static int findBiggestPrimePalindrome(int maxValue) {

        List<Integer> palindromeNumbers = generatePalindromeNumbers(getNumberOfDigits(maxValue - 1));
        for (Integer p : palindromeNumbers) {
            if (isPrime(p)) {
                return p;
            }
        }
        return 0;
    }

    private static boolean isPrime(int number) {
        for (int i = 2; i * i <= number; i++) {
            if (number % i == 0)
                return false;
        }
        return true;
    }

    public static List<Integer> generatePalindromeNumbers(int numberOfDigits) {
        List<Integer> palindromes = new LinkedList<Integer>();
        int numberOfFactors = 0;
        if( numberOfDigits %2 == 0 ){
            numberOfFactors = numberOfDigits/2;
        }
        else{
            numberOfFactors = numberOfDigits/2+1;
        }
        int maxFactorsPattern = (int) Math.pow(10, numberOfFactors) - 1;
        int minFactorsPattern = (int) Math.pow(10, numberOfFactors - 1) + 1;

        for (int i = maxFactorsPattern; i >= minFactorsPattern; i--) {
            palindromes.add(createPalindromeNumber(numberOfDigits, convertIntToArray(i)));
        }
        return palindromes;
    }


    private static int createPalindromeNumber(int length, int... factors) {
        assertCorrectNumberOfFactors(length, factors);
        int[] palindromeNumber = new int[length];
        int currentNestingLevel = 0;
        int maxIndex = length - 1;
        for (int i = 0; i < factors.length - 1; i++) {
            palindromeNumber[currentNestingLevel] = factors[i];
            palindromeNumber[maxIndex - currentNestingLevel] = factors[i];
            currentNestingLevel++;
        }
        if (length % 2 == 0) {
            palindromeNumber[length / 2] = factors[factors.length - 1];
            palindromeNumber[length / 2 - 1] = factors[factors.length - 1];
        } else {
            palindromeNumber[length / 2] = factors[factors.length - 1];
        }
        // System.out.println(Arrays.toString(palindromeNumber));
        int result = convertArrayToInt(palindromeNumber);
        return result;
    }

    private static void assertCorrectNumberOfFactors(int length, int[] factors) {
        if (length % 2 == 0) {
            if (factors.length * 2 != length) {
                throw new IllegalArgumentException();
            }
        } else {
            if (factors.length * 2 != length + 1) {
                throw new IllegalArgumentException();
            }
        }
    }

    private static int convertArrayToInt(int[] palindromeNumber) {
        int result = 0;
        for (int i = 0; i < palindromeNumber.length; i++) {
            int multiplicationFactor = (int) Math.pow(10, (palindromeNumber.length - 1 - i));
            result += palindromeNumber[i] * multiplicationFactor;
        }
        return result;
    }

    private static int[] convertIntToArray(int number) {
        List<Integer> result = new LinkedList<Integer>();
        while(number>0){
            result.add(number % 10);
            number /= 10;
        }
        Collections.reverse(result);
        Integer[] arr = result.toArray(new Integer[0]);
        int[] resultArray = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            resultArray[i] = arr[i];
        }
        return resultArray;
    }


    public static int getNumberOfDigits(int number) {
        int numberOfdigits = 0;
        while (number >= 1) {
            number = number / 10;
            numberOfdigits++;
        }
        return numberOfdigits;
    }

}

package fun.algo.codeeval;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class MaxSumOfSubarray {

    private static final List<Integer> SAMPLE1 = Arrays.asList(-10, 2, 3, -2, 0, 5, -15);
    private static final List<Integer> SAMPLE2 = Arrays.asList(2, 3, -2, -1, 10);
    private static final List<Integer> SAMPLE3 = Arrays.asList(2, 3, -2, -1, 10,-10, 2, 3, -2, 0, 5, -15,-10, 2, 3, -2, 0, 5, -15,-10, 2, 3, -2, 0, 5, -15,-10, 2, 3, -2, 0, 5, -15);
    private static final List<Integer> SAMPLE4 = Arrays.asList(2, 3, -2, -1, 10,-10, 2, 3, -2, 0, 5, -15,-10, 2, 3, -2, 0, 5, -15,-10, 2, 3, -2, 0, 5, -15,-10, 2, 3, -2, 0, 5, -15,
            2, 3, -2, -1, 10,-10, 2, 3, -2, 0, 5, -15,-10, 2, 3, -2, 0, 5, -15,-10, 2, 3, -2, 0, 5, -15,-10, 2, 3, -2, 0, 5, -15);
    
    private static long iterationCounterForDynamic = 0;
    private static long iterationCounterForBruteForce = 0;

    public static void main2(String[] args) throws NumberFormatException, IOException {
        BufferedReader bufferedReader = IOUtil.getBufferedReader("sumofsubarray");
        List<List<Integer>> input = IOUtil.getNonemptyLinesAsIntegersSequences(bufferedReader, ",");
        List<Integer> result = new LinkedList<Integer>();
        List<Integer> result2 = new LinkedList<Integer>();

        for (List<Integer> seq : input) {
            result.add(getMaxSumOfSubarrayDynamic(seq));
            result2.add(getMaxSumOfSubarrayBruteforce(seq));
        }
        IOUtil.print(result);
        IOUtil.print(result2);
    }
    
    public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
        System.out.println(iterationCounterForDynamic);
        System.out.println(iterationCounterForBruteForce);
        System.out.println("brute/dyn ratio:" + (double) iterationCounterForBruteForce / (double) iterationCounterForDynamic);
    }

    public static void testDynamic(){
        getMaxSumOfSubarrayDynamic(SAMPLE1);
        getMaxSumOfSubarrayDynamic(SAMPLE2);
        getMaxSumOfSubarrayDynamic(SAMPLE3);
        getMaxSumOfSubarrayDynamic(SAMPLE4);
    }

    public static void testBruteforce() {
        getMaxSumOfSubarrayBruteforce(SAMPLE1);
        getMaxSumOfSubarrayBruteforce(SAMPLE2);
        getMaxSumOfSubarrayBruteforce(SAMPLE3);
        getMaxSumOfSubarrayBruteforce(SAMPLE4);
    }
    /**
     * solution that uses dynamic programming approach
     */
    public static int getMaxSumOfSubarrayDynamic(List<Integer> seq) {
        int[][] results = new int[seq.size()][seq.size()]; // first index indicates starting pos, second index indicates subarr length
        int max = seq.get(0);
        int sum = 0;
        for (int i = 0; i < seq.size(); i++) {
            sum += seq.get(i);
            results[0][i] = sum;
            if (sum > max) {
                max = sum;
            }
            iterationCounterForDynamic++;
        }
        for (int i = 1; i < seq.size(); i++) {
            for (int j = 0; j < seq.size() - i; j++) {
                int previousElement = 0;
                if (j == 0) {
                    previousElement = 0;
                } else {
                    previousElement = results[i][j - 1];
                }
                results[i][j] = previousElement + (results[i - 1][j + 1] - results[i - 1][j]);
                if (results[i][j] > max) {
                    max = results[i][j];
                }
                iterationCounterForDynamic++;
            }
        }
        return max;
    }

    /**
     * brute-force solution
     */
    public static int getMaxSumOfSubarrayBruteforce(List<Integer> seq) {
        int max = seq.get(0);
        for (int i = 0; i < seq.size(); i++) {
            for (int j = 0; j < seq.size() - i; j++) {
                int currentSum = 0;
                for (int k = i; k < i + j + 1; k++) {
                    currentSum += seq.get(k);
                    iterationCounterForBruteForce++;
                }
                if (currentSum > max) {
                    max = currentSum;
                }

            }

        }
        return max;
    }

}

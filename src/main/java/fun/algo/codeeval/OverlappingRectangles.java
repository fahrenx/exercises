package fun.algo.codeeval;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class OverlappingRectangles {

    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader bufferedReader = IOUtil.getBufferedReader("overlappingRectangles");
        List<List<Integer>> input = IOUtil.getNonemptyLinesAsIntegersSequences(bufferedReader, ",");
        List<Boolean> result = new LinkedList<Boolean>();
        for (List<Integer> seq : input) {
            result.add(areOverlapping(seq));
        }
        for (boolean overlapping : result) {
            if (overlapping)
                System.out.println("True");
            else
                System.out.println("False");
        }
    }

    private static boolean areOverlapping(List<Integer> seq) {

        int xStartA = seq.get(0);
        int xEndA = seq.get(2);
        int yStartA = seq.get(3);
        int yEndA = seq.get(1);

        int xStartB = seq.get(4);
        int xEndB = seq.get(6);
        int yStartB = seq.get(7);
        int yEndB = seq.get(5);

        boolean overlappedOnX = areOverlapping(xStartA, xEndA, xStartB, xEndB);
        boolean overlappedOnY = areOverlapping(yStartA, yEndA, yStartB, yEndB);

        return overlappedOnX && overlappedOnY;
    }

    private static boolean areOverlapping(int xStartA, int xEndA, int xStartB, int xEndB) {
        // previously I had this below but I simplified that
        // if (xStartA >= xStartB && xStartA <= xEndB || xEndA >= xStartB && xEndA <= xEndB) {
        if (xEndA >= xStartB && xStartA <= xEndB) {
            return true;
        } else {
            return false;
        }

    }

}

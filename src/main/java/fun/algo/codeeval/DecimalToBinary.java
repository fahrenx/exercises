package fun.algo.codeeval;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class DecimalToBinary {

    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader bufferedReader = IOUtil.getBufferedReader("decimalToBinary");
        List<Integer> input = IOUtil.getNonemptyLinesAsIntegers(bufferedReader);
        List<String> result = new LinkedList<String>();
        for (Integer i : input) {
            result.add(convertBinarArrayToString(convertIntToBinaryArrayDividing(i)));
        }
        IOUtil.print(result);

    }

    private static String convertBinarArrayToString(int[] binaryArray) {
        StringBuilder sb = new StringBuilder();
        for (int i : binaryArray) {
            sb.append(i);
        }
        return sb.toString();
    }

    public static int[] convertIntToBinaryArrayDividing(int number) {
        if (number == 0) {
            return new int[] { 0 };
        }
        List<Integer> bits = new LinkedList<Integer>();
        while (number > 0) {
            bits.add(number % 2);
            number /= 2;
        }
        int[] res = new int[bits.size()];
        int index = 0;
        for (Integer b : bits) {
            res[res.length - 1 - index] = b;
            index++;
        }
        return res;
    }

}

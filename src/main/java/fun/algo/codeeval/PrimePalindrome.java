package fun.algo.codeeval;

import java.lang.reflect.InvocationTargetException;


/**
 * 
 * method: testFindBiggestPalindrome( runs: 80 ) avg time: 0.0684547125 ms which is about 18 times faster than unoptimized version
 */
public class PrimePalindrome {

    public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, InterruptedException {
        // System.out.println(getNumberOfDigits(999));
        // Thread.sleep(20000000);
    }

    public static void testFindBiggestPalindrome() {
        findBiggestPrimePalindrome(999);
    }

    private static boolean isPrime(int number) {
        int squareRoot = (int) Math.sqrt(number);
        for (int i = 2; i <= squareRoot; i++) {
            if (number % i == 0)
                return false;
        }
        return true;
    }

    public static long findBiggestPrimePalindrome(int maxValue) {

        int numberOfDigits = getNumberOfDigits(maxValue);
        int numberOfFactors = 0;
        if (numberOfDigits % 2 == 0) {
            numberOfFactors = numberOfDigits / 2;
        } else {
            numberOfFactors = numberOfDigits / 2 + 1;
        }
        int maxFactorsPattern = (int) Math.pow(10, numberOfFactors) - 1;
        int minFactorsPattern = (int) Math.pow(10, numberOfFactors - 1) + 1;

        for (int i = maxFactorsPattern; i >= minFactorsPattern; i--) {
            int number = createPalindromeNumber(numberOfDigits, convertIntToArray(i));
            if (number < maxValue) {
                if (isPrime(number)) {
                    return number;
                }
            }
        }
        return -1;
    }

    private static int createPalindromeNumber(int length, int... factors) {
        assertCorrectNumberOfFactors(length, factors);
        int[] palindromeNumber = new int[length];
        int currentNestingLevel = 0;
        int maxIndex = length - 1;
        for (int i = 0; i < factors.length - 1; i++) {
            palindromeNumber[currentNestingLevel] = factors[i];
            palindromeNumber[maxIndex - currentNestingLevel] = factors[i];
            currentNestingLevel++;
        }
        if (length % 2 == 0) {
            palindromeNumber[length / 2] = factors[factors.length - 1];
            palindromeNumber[length / 2 - 1] = factors[factors.length - 1];
        } else {
            palindromeNumber[length / 2] = factors[factors.length - 1];
        }
        int result = convertArrayToInt(palindromeNumber);
        return result;
    }

    private static void assertCorrectNumberOfFactors(int length, int[] factors) {
        if (length % 2 == 0) {
            if (factors.length * 2 != length) {
                throw new IllegalArgumentException();
            }
        } else {
            if (factors.length * 2 != length + 1) {
                throw new IllegalArgumentException();
            }
        }
    }

    private static int convertArrayToInt(int[] palindromeNumber) {
        int result = 0;
        for (int i = 0; i < palindromeNumber.length; i++) {
            int multiplicationFactor = (int) Math.pow(10, (palindromeNumber.length - 1 - i));
            result += palindromeNumber[i] * multiplicationFactor;
        }
        return result;
    }

    private static int[] convertIntToArray(int number) {
        int[] resultArray = new int[getNumberOfDigits(number)];
        int i = 0;
        while (number > 0) {
            resultArray[resultArray.length - 1 - i] = number % 10;
            number /= 10;
            i++;
        }
        return resultArray;
    }

    public static int getNumberOfDigits(int number) {
        int numberOfdigits = 0;
        while (number >= 1) {
            number = number / 10;
            numberOfdigits++;
        }
        return numberOfdigits;
    }

}

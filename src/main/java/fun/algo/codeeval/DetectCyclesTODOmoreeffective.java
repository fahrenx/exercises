package fun.algo.codeeval;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class DetectCyclesTODOmoreeffective {

    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader in = IOUtil.getBufferedReader("detectingCycles");
        List<List<Integer>> input = IOUtil.getNonemptyLinesAsIntegersSequences(in);
        List<List<Integer>> result = new ArrayList<List<Integer>>();
        for (List<Integer> seq : input) {
            List<Integer> cycle = findCycle(seq);
            result.add(cycle);
        }
        IOUtil.print(result);
    }

    /*
     * brute-force method. 
     * My definition of the cycle: at least two digits long, repeated at least twice and the last repetition is fully contained in sequence
     * In this solutions for each starting position, beginning with zero, we are checking if we can find cycle of all different lengths
     */
    private static List<Integer> findCycle(List<Integer> seq) {
        List<Integer> data = new LinkedList<Integer>();
        if (seq.size() < 4) {
            throw new IllegalArgumentException(); // cannot really say if there is a cycle in a sequence that is shorter
        }
        for (int startingPos = 0; startingPos < seq.size() - 4; startingPos++) {
            for (int cycleLenght = 2; cycleLenght <= (seq.size() - startingPos) / 2; cycleLenght++) {
                List<Integer> pattern = new LinkedList<Integer>();
                for (int i = startingPos; i < startingPos + cycleLenght; i++) {
                    pattern.add(seq.get(i));
                }
                boolean isCycle = true;
                for (int i = startingPos + cycleLenght; i < seq.size(); i++) {
                    if (!seq.get(i).equals(pattern.get((i - startingPos) % cycleLenght))) {
                        isCycle = false;
                    }
                }
                if (isCycle) {
                    return pattern;
                }
            }
        }

        return data;
    }

}

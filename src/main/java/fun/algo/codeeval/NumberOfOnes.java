package fun.algo.codeeval;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class NumberOfOnes {

    public static void main(String[] args) throws IOException {
        BufferedReader in = IOUtil.getBufferedReader("numberofones");
        List<Integer> ints = IOUtil.getNonemptyLinesAsIntegers(in);
        List<Integer> results = new LinkedList<Integer>();
        for (int anInt : ints) {
            results.add(countOnes(anInt));
        }
        IOUtil.print(results);
    }

    public static int countOnes(int number) {
        int counter = 0;
        while (number > 0) {
            int i = 0;
            while (Math.pow(2, i) <= number) {
                i++;
            }
            i--;
            number -= (int) Math.pow(2, i);
            counter++;
        }
        return counter;
    }

}

package fun.algo.codeeval.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

class ReaderSample {
    public static void main(String[] args) throws IOException {
        BufferedReader in = IOUtil.getBufferedReader(args[0]);
    }
}

/**
 * utility class to abstract reading and printing value
 */
public class IOUtil {

    public static BufferedReader getBufferedReader(String filename) throws FileNotFoundException {
        File file = new File(filename);
        return new BufferedReader(new FileReader(file));
    }

    public static int getIntValueFromFirstLine(BufferedReader in) throws IOException {
        String line = in.readLine();
        line = line.trim();
        return Integer.valueOf(line);
    }

    public static List<String> getNonemptyLinesAsTrimmedStrings(BufferedReader in) throws IOException {
        List<String> result = new LinkedList<String>();
        String line = null;
        while ((line = in.readLine()) != null) {
            line = line.trim();
            if (line.length() > 0) {
                result.add(line);
            }
        }
        return result;
    }

    public static List<String> getNonemptyLinesAsNonTrimmedStrings(BufferedReader in) throws IOException {
        List<String> result = new LinkedList<String>();
        String line = null;
        while ((line = in.readLine()) != null) {
            if (line.length() > 0) {
                result.add(line);
            }
        }
        return result;
    }
    public static <T> List<T> getNonemptyLinesWithCustomProcessing(BufferedReader in, LineProcessor<T> lineProcessor) throws IOException {
        List<T> result = new LinkedList<T>();
        String line = null;
        while ((line = in.readLine()) != null) {
            line = line.trim();
            if (line.length() > 0) {
                result.add(lineProcessor.process(line));
            }
        }
        return result;
    }


    public static List<Integer> getNonemptyLinesAsIntegers(BufferedReader in) throws NumberFormatException, IOException {
        List<Integer> result = new LinkedList<Integer>();
        String line = null;
        while ((line = in.readLine()) != null) {
            line = line.trim();
            if (line.length() > 0) {
                result.add(Integer.valueOf(line));
            }
        }
        return result;
    }

    public static List<List<Integer>> getNonemptyLinesAsIntegersSequences(BufferedReader in) throws NumberFormatException, IOException {
        return getNonemptyLinesAsIntegersSequences(in, "\\s");
    }

    public static List<List<Integer>> getNonemptyLinesAsIntegersSequences(BufferedReader in, String delimiter) throws NumberFormatException, IOException {
        List<List<Integer>> result = new LinkedList<List<Integer>>();
        String line = null;
        while ((line = in.readLine()) != null) {
            line = line.trim();
            String[] lineArray = line.split(delimiter);
            if (lineArray.length > 0) {
                List<Integer> intSequence = new LinkedList<Integer>();
                for (String s : lineArray) {
                    if (s.length() > 0) {
                        intSequence.add(Integer.valueOf(s.trim()));
                    }
                }
                if (intSequence.size() > 0) {
                    result.add(intSequence);
                }
            }
        }
        return result;
    }

    public static <T> void print(List<T> data) {
        print(data, " ");
    }

    public static <T> void print(List<T> data, boolean separatedBySpaceInLine) {
        if (separatedBySpaceInLine) {
            print(data, " ");
        } else {
            print(data, "");
        }
    }

    public static <T> void print(List<T> data, String inLineDelimiter) {
        StringBuilder sb = new StringBuilder();
        for (T line : data) {
            if (line instanceof Collection) {
                Collection col = (Collection) line;
                Iterator it = col.iterator();
                while (it.hasNext()) {
                    sb.append(it.next().toString());
                    if (it.hasNext()) {
                        sb.append(inLineDelimiter);
                    }
                }
            } else {
                sb.append(line);
            }
            sb.append("\n");
        }
        System.out.println(sb.toString());
    }

    public static interface LineProcessor<T> {

        public T process(String line);

    }

}


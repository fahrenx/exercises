package fun.algo.codeeval;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class TrailingString {

    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader bufferedReader = IOUtil.getBufferedReader("trailingString");
        List<List<String>> input = IOUtil.getNonemptyLinesWithCustomProcessing(bufferedReader, new TrailingStringLineProcessor());
        List<Integer> result = new LinkedList<Integer>();
        IOUtil.print(input, false);
        for (List<String> seq : input) {
            result.add(convertBooleanToInt(isSecondStringTheEndingOfTheFirstOne(seq)));
        }
        IOUtil.print(result);

    }

    private static int convertBooleanToInt(boolean aBoolean) {
        return aBoolean ? 1 : 0;
    }

    private static boolean isSecondStringTheEndingOfTheFirstOne(List<String> seq) {
        String firstString = seq.get(0);
        String secondString = seq.get(1);
        int firstStringSize = firstString.length();
        int i = secondString.length() - 1;

        while (i >= 0 && secondString.charAt(i) == firstString.charAt(firstStringSize - 1 - ((secondString.length() - 1) - i))) {
            i--;
        }
        if (i == -1) {
            return true;
        } else {
            return false;
        }
    }

    private static class TrailingStringLineProcessor implements IOUtil.LineProcessor<List<String>> {

        public List<String> process(String line) {
            List<String> result = new LinkedList<String>();
            String[] split = line.split(",");
            for (String s : split) {
                result.add(s.trim());
            }
            return result;
        }

    }

}

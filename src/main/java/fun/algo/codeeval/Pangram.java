package fun.algo.codeeval;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class Pangram {

    private static final String NULL_MARKER = "NULL";
    private final static int ENGLISH_ALPHABET_LENGTH = 26;

    public static void main(String[] args) throws IOException {
        BufferedReader in = IOUtil.getBufferedReader("pangrams");
        List<String> input = IOUtil.getNonemptyLinesAsTrimmedStrings(in);
        List<List<Character>> results = new LinkedList<List<Character>>();
        for (String sentence : input) {
            results.add(getMissingCharsForPangram(sentence));
        }
        printResults(results);
    }

    private static void printResults(List<List<Character>> results) {
        for (List<Character> missingCharsList : results) {
            if (missingCharsList.isEmpty()) {
                System.out.println(NULL_MARKER);
            } else {
                for (char ch : missingCharsList) {
                    System.out.print(ch);
                }
                System.out.println();
            }
            
        }
    }

    public static List<Character> getMissingCharsForPangram(String sentence) {
        String sentenceToProcess = sentence.toLowerCase();
        boolean[] occArr = new boolean[ENGLISH_ALPHABET_LENGTH];
        for (char ch : sentenceToProcess.toCharArray()) {
            if (ch >= 'a' && ch <= 'z') {
                int index = (ch - 'a') % ENGLISH_ALPHABET_LENGTH;
                occArr[index] = true;
            }
        }
        ArrayList<Character> result = new ArrayList<Character>();
        for (int i = 0; i < ENGLISH_ALPHABET_LENGTH; i++) {
            if(occArr[i]==false){
                result.add(new Character((char) ('a' + i)));
            }
        }
        return result;
    }

}

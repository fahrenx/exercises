package fun.algo.codeeval;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class ArrayAbsurdity {

    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader bufferedReader = IOUtil.getBufferedReader("arrayAbsurdity");
        List<List<Integer>> input = IOUtil.getNonemptyLinesWithCustomProcessing(bufferedReader, new ArrayAbsudityLineProcessor());
        List<Integer> result = new LinkedList<Integer>();
        for (List<Integer> seq : input) {
            result.add(getDuplicatedEntry(seq));
        }
        IOUtil.print(result);

    }
    
    private static int getDuplicatedEntry(List<Integer> seq) {
        int highestElement = seq.size()-2; //N-2
        int numberOfElementsInSum = seq.size()-1; //N-1
        // sum of elemenst from 0 to N-2 (look at second part - it is a trick with mod to optionally add the middle element)
        int expectedSum = (0 + highestElement) * (numberOfElementsInSum / 2) + (numberOfElementsInSum % 2) * (highestElement / 2);
        int actualSum = 0;
        for(Integer i:seq){
            actualSum+=i;
        }
        return actualSum - expectedSum;
    }

    private static class ArrayAbsudityLineProcessor implements IOUtil.LineProcessor<List<Integer>> {

        public List<Integer> process(String line) {
            List<Integer> result = new LinkedList<Integer>();
            int semicolonOccurrenceIndex = line.indexOf(";");
            // int arrayLength = Integer.valueOf(line.substring(0,semicolonOccurrenceIndex ).trim());
            // result.add(arrayLength);
            String[] split = line.substring(semicolonOccurrenceIndex + 1).split(",");
            for(String s:split){
                result.add(Integer.valueOf(s.trim()));
            }
            return result;
        }
        
    }
    
}

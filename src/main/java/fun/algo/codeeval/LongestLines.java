package fun.algo.codeeval;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.TreeSet;

public class LongestLines {

    public static void main2(String[] args) throws IOException {
        File file = new File(args[0]);
        BufferedReader in = new BufferedReader(new FileReader(file));
        String line;
        int N = Integer.valueOf(in.readLine());
        TreeSet<Line> longest = findNLongestLinesInFile(in, N);
        printLongestLines(longest);
    }

    private static void printLongestLines(TreeSet<Line> longest) {
        for (Line longestLine : longest.descendingSet()) {
            System.out.println(longestLine.getContent());
        }
    }

    private static TreeSet<Line> findNLongestLinesInFile(BufferedReader in, int N) throws IOException {
        TreeSet<Line> longest = new TreeSet<Line>();
        String line;
        while ((line = in.readLine()) != null) {
            processLine(N, line, longest);
        }
        return longest;
    }

    private static void processLine(int N, String line, TreeSet<Line> longest) {
        line = line.trim();
        if (line.length() > 0) {
            if (longest.size() == N) {
                if (line.length() > longest.first().getLength()) {
                    longest.remove(longest.first());
                    longest.add(new Line(line.length(), line));
                }
            } else {
                longest.add(new Line(line.length(), line));
            }
        }
    }

    public static void main(String[] args) throws IOException {
        main2(new String[] { "longestLines2" });
    }

}

class Line implements Comparable<Line> {

    private final int length;
    private final String content;

    Line(int length, String content) {
        this.length = length;
        this.content = content;
    }

    public int getLength() {
        return length;
    }

    public String getContent() {
        return content;
    }

    public int compareTo(Line o) {
        if (this.length > o.length) {
            return 1;
        } else if (this.length == o.length) {
            return 0;
        } else {
            return -1;
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((content == null) ? 0 : content.hashCode());
        result = prime * result + length;
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Line other = (Line) obj;
        if (content == null) {
            if (other.content != null)
                return false;
        } else if (!content.equals(other.content))
            return false;
        if (length != other.length)
            return false;
        return true;
    }

}

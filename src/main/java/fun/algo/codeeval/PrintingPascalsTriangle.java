package fun.algo.codeeval;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class PrintingPascalsTriangle {
    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader bufferedReader = IOUtil.getBufferedReader("pascalsTriangle");
        List<Integer> input = IOUtil.getNonemptyLinesAsIntegers(bufferedReader);
        for (Integer i : input) {
            printPascalInOneRow(i);
        }
    }

    private static void printPascalInOneRow(int depth) {

        List<List<Integer>> pascalTriangleLevels = new LinkedList<List<Integer>>();
        pascalTriangleLevels.add(Arrays.asList(1));
        for (int i = 1; i < depth; i++) {
            List<Integer> pascalTriangleLevel = new LinkedList<Integer>();
            pascalTriangleLevel.add(1);
            for (int j = 1; j < i; j++) {
                pascalTriangleLevel.add(pascalTriangleLevels.get(i - 1).get(j - 1) + pascalTriangleLevels.get(i - 1).get(j));
            }
            pascalTriangleLevel.add(1);
            pascalTriangleLevels.add(pascalTriangleLevel);
        }
        for (List<Integer> level : pascalTriangleLevels) {
            for (Integer i : level) {
                System.out.print(i + " ");
            }
        }
        System.out.println();
    }
}

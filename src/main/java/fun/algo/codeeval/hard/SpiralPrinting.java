package fun.algo.codeeval.hard;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class SpiralPrinting {

    public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException {

        BufferedReader bufferedReader = IOUtil.getBufferedReader("spiralPrinting");
        List<List<String>> input = IOUtil.getNonemptyLinesWithCustomProcessing(bufferedReader, new SpiralPrintingLineProcessor());
        List<List<String>> result = new LinkedList<List<String>>();
        for (List<String> sequence : input) {
            result.add(getArrayElementsInSpiralOrderDifferent(Integer.valueOf(sequence.get(0)), Integer.valueOf(sequence.get(1)), sequence.subList(2, sequence.size())));
        }
        IOUtil.print(result, " ");

    }

    /*
     * this solution is inspired by: http://devdose.blogspot.co.uk/2009/12/algorithm-to-print-2d-array-in-spiral.html
     * Seems to be cleanest than the one below and works for rectangle matrices
     */
    private static List<String> getArrayElementsInSpiralOrderDifferent(int rowsNumber, int colsNumber, List<String> flatArr) {
        String[][] array = new String[rowsNumber][colsNumber];
        int flatArrIndex = 0;
        for (int i = 0; i < rowsNumber; i++) {
            for (int j = 0; j < colsNumber; j++) {
                array[i][j] = flatArr.get(flatArrIndex);
                flatArrIndex++;
            }
        }

        // Printers.print2Darray(array);

        List<String> result = new LinkedList<String>();

        int minCol = 0;
        int maxCol = colsNumber - 1;
        int minRow = 0;
        int maxRow = rowsNumber - 1;
        int i = 0;
        int cnt = 0;
        int numberOfElements = rowsNumber * colsNumber;

        
        while (cnt < numberOfElements) {
            for (i = minCol; i <= maxCol && cnt < numberOfElements; i++) {
                result.add(array[minRow][i]);
                cnt++;
            }
            for (i = minRow + 1; i <= maxRow && cnt < numberOfElements; i++) {
                result.add(array[i][maxCol]);
                cnt++;
            }
            for (i = maxCol - 1; i >= minCol && cnt < numberOfElements; i--) {
                result.add(array[maxRow][i]);
                cnt++;
            }
            for (i = maxRow - 1; i > minRow && cnt < numberOfElements; i--) {
                result.add(array[i][minCol]);
                cnt++;
            }
            minRow++;
            maxRow--;
            minCol++;
            maxCol--;
        }
        return result;
    }
    
    /**
     * this four-fors solution is simple and ok but only for square matrix, I have a problem with generalization to rectangle matrix
     */
    private static List<Integer> getArrayElementsInSpiralOrder(int yLength, int xLength, List<Integer> flatArr) {
        int[][] array = new int[xLength][yLength];
        int flatArrIndex = 0;
        for (int j = 0; j < yLength; j++) {
            for (int i = 0; i < xLength; i++) {
                array[i][j] = flatArr.get(flatArrIndex);
                flatArrIndex++;
            }
        }

        for (int y = 0; y < yLength; y++) {
            for (int x = 0; x < xLength; x++) {
                System.out.print(array[x][y]);
            }
            System.out.println();
        }

        List<Integer> result = new LinkedList<Integer>();
        int level = 0;
        while (level <= xLength / 2 - 1) {
            for (int i = level; i < array.length - level - 1; i++) {
                result.add(array[i][level]);
            }
            for (int i = level; i < array.length - 1 - level; i++) {
                result.add(array[array.length - 1 - level][i]);
            }
            for (int i = array.length - 1 - level; i > level; i--) {
                result.add(array[i][array.length - 1 - level]);
            }
            for (int i = array.length - 1 - level; i > level; i--) {
                result.add(array[level][i]);
            }
            level++;
        }
        if (xLength % 2 == 1) {
            result.add(array[xLength/2][xLength/2]);
        }
        return result;
    }

    private static class SpiralPrintingLineProcessor implements IOUtil.LineProcessor<List<String>> {

        public List<String> process(String line) {
            String[] split = line.split(";|\\s");
            List<String> result = new LinkedList<String>();
            for(String str:split){
                result.add(str.trim());
            }
            return result;
        }
    }
}

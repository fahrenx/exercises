package fun.algo.codeeval.hard;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;


public class RobotMovements {

    private static final int ARRLENGTH = 5;
    private static final Tuple BLOCKINGTUPLE1 = new Tuple(ARRLENGTH - 2, ARRLENGTH - 1);
    private static final Tuple BLOCKINGTUPLE2 = new Tuple(ARRLENGTH - 1, ARRLENGTH - 2);

    public static void main2(String[] args) {

        int numberOfPossibleMovements = findNumberOfPossibleMovements(ARRLENGTH);
        System.out.println(numberOfPossibleMovements);
    }

    public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    }

    public static void test() {
        int numberOfPossibleMovements = findNumberOfPossibleMovements(ARRLENGTH);
        // System.out.println(numberOfPossibleMovements);
    }

    private static int findNumberOfPossibleMovements(int n) {
        MutableInt result = new MutableInt(0);
        findNumberOfPossibleMovementsRec(n, new HashSet<Tuple>(), 0, 0, result);
        return result.value;
    }

    private static void findNumberOfPossibleMovementsRec(int ARRLENGTH, Set<Tuple> visited, int currentPosX, int currentPosY, MutableInt result) {
        // optimisation - fail fast on blocked bottom-right tile - by n=5 about 5% speedup
        if (visited.size() > ARRLENGTH / 2 && visited.contains(BLOCKINGTUPLE1) && visited.contains(BLOCKINGTUPLE2)) {
            if (currentPosX != ARRLENGTH - 1 || currentPosY != ARRLENGTH - 1)
            return;
        }

        visited.add(new Tuple(currentPosX, currentPosY));
        if (currentPosX == ARRLENGTH - 1 && currentPosY == ARRLENGTH - 1) {
            result.value=result.value+1;
            return;
        }
        int yUp = currentPosY - 1;
        if (yUp >= 0 && !visited.contains(new Tuple(currentPosX,yUp))) {// up
            findNumberOfPossibleMovementsRec(ARRLENGTH, new HashSet<Tuple>(visited), currentPosX, yUp, result);
        }
        int yDown = currentPosY + 1;
        if (yDown <= ARRLENGTH - 1 && !visited.contains(new Tuple(currentPosX, yDown))) {// down
            findNumberOfPossibleMovementsRec(ARRLENGTH, new HashSet<Tuple>(visited), currentPosX, yDown, result);
        }
        int xLeft = currentPosX - 1;
        if (xLeft >= 0 && !visited.contains(new Tuple(xLeft, currentPosY))) {// left
            findNumberOfPossibleMovementsRec(ARRLENGTH, new HashSet<Tuple>(visited), xLeft, currentPosY, result);
        }
        int xRight = currentPosX + 1;
        if (xRight <= ARRLENGTH - 1 && !visited.contains(new Tuple(xRight, currentPosY))) {// right
            findNumberOfPossibleMovementsRec(ARRLENGTH, new HashSet<Tuple>(visited), xRight, currentPosY, result);
        }
    }

    private static class MutableInt {
        int value;

        public MutableInt(int value) {
            this.value = value;
        }
    }
    
    private static class Tuple{
        final int x;
        final int y;

        Tuple(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int hashCode() {
            return x * y * 31;
        }

        public boolean equals(Object o) {
            if (o == null)
                return false;
            if (!(o instanceof Tuple)) {
                return false;
            }
            Tuple tuple = (Tuple) o;
            if (this.x != tuple.x) {
                return false;
            }
            if (this.y != tuple.y) {
                return false;
            }
            return true;
        }

    }

}

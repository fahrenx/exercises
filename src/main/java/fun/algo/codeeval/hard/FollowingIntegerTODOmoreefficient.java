package fun.algo.codeeval.hard;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fun.algo.codeeval.util.IOUtil;

public class FollowingIntegerTODOmoreefficient {

    public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException {

        BufferedReader bufferedReader = IOUtil.getBufferedReader("followingInteger");
        List<Integer> input = IOUtil.getNonemptyLinesAsIntegers(bufferedReader);
        List<Integer> result = new LinkedList<Integer>();
        for (Integer number : input) {
            result.add(getFollowingIntegerWithFailFastCheck2(number));
        }
        IOUtil.print(result);
    }

    public static void main2(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    }

    public static void testGetFollowingIntegerWithNewMapEachTime() {
        getFollowingInteger(115);
        getFollowingInteger(151);
        getFollowingInteger(511);
        getFollowingInteger(1015);
    }

    public static void testGetFollowingIntegerWithFailFastCheck() {
        getFollowingIntegerWithFailFastCheck(115);
        getFollowingIntegerWithFailFastCheck(151);
        getFollowingIntegerWithFailFastCheck(511);
        getFollowingIntegerWithFailFastCheck(1015);
    }

    public static void testGetFollowingIntegerWithFailFastCheck2() {
        getFollowingIntegerWithFailFastCheck2(115);
        getFollowingIntegerWithFailFastCheck2(151);
        getFollowingIntegerWithFailFastCheck2(511);
        getFollowingIntegerWithFailFastCheck2(1015);
    }

    private static int getFollowingInteger(int number) {
        Map<Integer, Integer> digitsOccurrences = getDigitsOccurrencesIgnoringZero(number);
        boolean found = false;
        int i = number + 1;
        int numberFound = 0;
        while (!found) {
            if (digitsOccurrences.equals(getDigitsOccurrencesIgnoringZero(i))) {
                found = true;
                numberFound = i;
            }
            i++;
        }
        return numberFound;
    }

    private static int getFollowingIntegerWithFailFastCheck(int number) {
        Map<Integer, Integer> digitsOccurrences = getDigitsOccurrencesIgnoringZero(number);
        boolean found = false;
        int i = number + 1;
        int numberFound = 0;
        while (!found) {
            if (checkDigitsOccurrencesMatchesIngoringZero(new HashMap<Integer, Integer>(digitsOccurrences), i)) {
                found = true;
                numberFound = i;
            }
            i++;
        }
        return numberFound;
    }

    private static int getFollowingIntegerWithFailFastCheck2(int number) {
        Map<Integer, Integer> digitsOccurrences = getDigitsOccurrencesIgnoringZero(number);
        boolean found = false;
        int i = number + 1;
        int numberFound = 0;
        while (!found) {
            if (digitsOccurrences.equals(getDigitsOccurrencesIgnoringZeroFailFast(digitsOccurrences, i))) {
                found = true;
                numberFound = i;
            }
            i++;
        }
        return numberFound;
    }
    
    private static Object getDigitsOccurrencesIgnoringZeroFailFast(Map<Integer, Integer> otherDigitsOccurrences, int number) {
        Map<Integer, Integer> digitsOccurrences = new HashMap<Integer, Integer>();
        int lastNumber = number;
        while (lastNumber > 0) {
            int digit = lastNumber % 10;
            lastNumber /= 10;
            if (digit > 0) {
                if (!otherDigitsOccurrences.containsKey(digit)) {
                    return null;
                }
                if (digitsOccurrences.containsKey(digit)) {
                    int currentNumberOfOccurrences = digitsOccurrences.get(digit);
                    digitsOccurrences.put(digit, currentNumberOfOccurrences + 1);
                } else {
                    digitsOccurrences.put(digit, 1);
                }
            }
        }
        return digitsOccurrences;
    }

    private static Map<Integer, Integer> getDigitsOccurrencesIgnoringZero(int number) {
        Map<Integer, Integer> digitsOccurrences = new HashMap<Integer, Integer>();
        int lastNumber = number;
        while (lastNumber > 0) {
            int digit = lastNumber % 10;
            lastNumber /= 10;
            if (digit > 0) {
                if (digitsOccurrences.containsKey(digit)) {
                    int currentNumberOfOccurrences = digitsOccurrences.get(digit);
                    digitsOccurrences.put(digit, currentNumberOfOccurrences + 1);
                } else {
                    digitsOccurrences.put(digit, 1);
                }
            }
        }
        return digitsOccurrences;
    }

    private static boolean checkDigitsOccurrencesMatchesIngoringZero(Map<Integer, Integer> digitsOccurrences, int number) {

        int lastNumber = number;
        while (lastNumber > 0) {
            int digit = lastNumber % 10;
            lastNumber /= 10;
            if (digit > 0) {
                if (digitsOccurrences.containsKey(digit)) {
                    int currentNumberOfOccurrences = digitsOccurrences.get(digit);
                    if (currentNumberOfOccurrences == 0) {
                        return false;
                    }
                    if (currentNumberOfOccurrences == 1) {
                        digitsOccurrences.remove(digit);
                    } else {
                        digitsOccurrences.put(digit, currentNumberOfOccurrences - 1);
                    }
                } else {
                    return false;
                }
            }
        }
        return digitsOccurrences.isEmpty();
    }

}

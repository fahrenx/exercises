package fun.algo.codeeval.hard;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class LongestRepeatedStringTODOimprovements {

    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader bufferedReader = IOUtil.getBufferedReader("repeatedString");
        List<String> input = IOUtil.getNonemptyLinesAsNonTrimmedStrings(bufferedReader);
        List<String> result = new LinkedList<String>();
        for (String word : input) {
            result.add(getLongestRepeatedString(word.toCharArray()));
        }
        for (String str : result) {
            if (str == null) {
                System.out.println("NONE");
            }
            else{
                System.out.println(str);
            }
        }
    }

    private static String getLongestRepeatedString(char[] chars) {
        // TreeSet<String> repeatedStrings = new TreeSet<String>();
        String longestRepeatedString = null;
        int currentMaxLength = 0;

        for (int i = 0; i < chars.length; i++) { // setting startingPosition
            int remainingChars = chars.length - i;
            int longestPossibleRepeatedStringStartingFromHere = remainingChars / 2;
            if (longestPossibleRepeatedStringStartingFromHere <= currentMaxLength) {
                break;
            }
            // setting length of string that potentially is repeated
            // TODO reverse
            for (int j = currentMaxLength + 1; j <= longestPossibleRepeatedStringStartingFromHere; j++) {
                // getting string that potentially is being repeated
                char[] potentiallyRepeatedString = new char[j];
                for (int k = i; k < i + j; k++) {
                    potentiallyRepeatedString[k-i] = chars[k];
                }
                
                //looking for a repetition of the string using different offsets
                int offset = 0; //offset from the end of pattern
                int offsetLimit = remainingChars-2*j; //offset cannot be too big because we wouldn't have enough space to repeat the pattern
                boolean found = false;
                while(offset<=offsetLimit && !found){
                    
                    int startingPositionOfTheRepetition = i+j+offset;
                    int endingPositionOfTheRepetition = i+j+offset+j;
                    int currentPosition = startingPositionOfTheRepetition;
                    while (currentPosition < endingPositionOfTheRepetition) {
                        if (chars[currentPosition - j - offset] != chars[currentPosition]) {
                            break;
                        }
                        currentPosition++;
                    }
                    if(currentPosition==endingPositionOfTheRepetition){
                        String repetitionFound = new String(potentiallyRepeatedString);
                        if (!repetitionFound.matches("\\s+")) {
                            longestRepeatedString = repetitionFound;
                            currentMaxLength = j;
                            found = true;
                        }
                    }
                    offset++;
                }


            }
        }
        return longestRepeatedString;
    }

    private static class RepeatedString implements Comparable<RepeatedString> {

        private final String string;
        private final int startingIndex;
        
        @Override
        public int hashCode() {
            final int prime = 31;
            int result = 1;
            result = prime * result + startingIndex;
            result = prime * result + ((string == null) ? 0 : string.hashCode());
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (obj == null)
                return false;
            if (getClass() != obj.getClass())
                return false;
            RepeatedString other = (RepeatedString) obj;
            if (startingIndex != other.startingIndex)
                return false;
            if (string == null) {
                if (other.string != null)
                    return false;
            } else if (!string.equals(other.string))
                return false;
            return true;
        }

        public RepeatedString(String string, int startingIndex) {
            this.string = string;
            this.startingIndex = startingIndex;
        }

        public int compareTo(RepeatedString other) {
            int lengthDifference = this.string.length() - other.getString().length();
            if (lengthDifference == 0) {
                return this.startingIndex - other.getStartingIndex();
            } else {
                return lengthDifference;
            }
        }

        public String getString() {
            return string;
        }

        public int getStartingIndex() {
            return startingIndex;
        }

    }

}

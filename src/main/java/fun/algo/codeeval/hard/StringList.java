package fun.algo.codeeval.hard;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import fun.algo.codeeval.util.IOUtil;
import fun.algo.combinatorics.CombinatoricsTODOmoreEffectiveClean;

public class StringList {

    public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException {

        BufferedReader bufferedReader = IOUtil.getBufferedReader("stringListVariations");
        List<NumberAndLetters> input = IOUtil.getNonemptyLinesWithCustomProcessing(bufferedReader, new StringListLineProcessor());
        List<List<String>> result = new LinkedList<List<String>>();
        for (NumberAndLetters numberAndLetters : input) {
            result.add(generatePossibleWords(numberAndLetters.getLetters(), numberAndLetters.getNumber()));
        }
        for (List<String> strings : result) {
            Collections.sort(strings);
        }
        IOUtil.print(result, ",");

    }

    private static List<String> generatePossibleWords(List<Character> letters, int number) {
        Set<List<Character>> possibleCharsSequences = CombinatoricsTODOmoreEffectiveClean.sequencesWithRepetition(letters, number);
        return convertToStrings(possibleCharsSequences);
    }

    private static List<String> convertToStrings(Set<List<Character>> possibleCharsSequences) {
        List<String> result = new LinkedList<String>();

        for (List<Character> chars : possibleCharsSequences) {
            StringBuilder sb = new StringBuilder();
            for (Character ch : chars) {
                sb.append(ch);
            }
            result.add(sb.toString());
        }
        return result;
    }

    private static class StringListLineProcessor implements IOUtil.LineProcessor<NumberAndLetters> {

        public NumberAndLetters process(String line) {
            line = line.trim();
            int comaIndex = line.indexOf(',');
            int number = Integer.valueOf(line.substring(0, comaIndex));
            char[] charsArray = line.substring(comaIndex + 1, line.length()).toCharArray();
            List<Character> letters = new ArrayList<Character>();
            for (char ch : charsArray) {
                letters.add(ch);
            }
            return new NumberAndLetters(number, letters);
        }

    }

    private static class NumberAndLetters {

        final private int number;
        final private List<Character> letters;

        public NumberAndLetters(int number, List<Character> letters) {
            this.number = number;
            this.letters = letters;
        }

        public int getNumber() {
            return number;
        }

        public List<Character> getLetters() {
            return letters;
        }

    }

}

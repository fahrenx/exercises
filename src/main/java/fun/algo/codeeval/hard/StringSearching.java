package fun.algo.codeeval.hard;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;
import fun.algo.codeeval.util.IOUtil.LineProcessor;

public class StringSearching {


    public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException {

        BufferedReader bufferedReader = IOUtil.getBufferedReader("stringSearching");
        List<List<String>> input = IOUtil.getNonemptyLinesWithCustomProcessing(bufferedReader, new StringSearchingLineProcessor());
        List<Boolean> result = new LinkedList<Boolean>();
        for (List<String> wordAndPattern : input) {
            result.add(isSubstringRecMain(wordAndPattern.get(0).toCharArray(), wordAndPattern.get(1).toCharArray()));
        }
        IOUtil.print(result);

    }

    /**
     * solution using backtracking for asterisks
     */
    private static boolean isSubstringRecMain(char[] word, char[] pattern) {
        int patternMinLength = getMinLengthOfPattern(pattern);
        // startingPos
        MutableBoolean result = new MutableBoolean(false);
        for (int i = 0; (i < word.length - patternMinLength); i++) {
            isSubstringRec(word, i, pattern, 0, result);
            if (result.getValue() == true) {
                return true;
            }
        }
        return result.getValue();
    }

    private static void isSubstringRec(char[] word, int wordStartIndex, char[] pattern, int patternStartIndex, MutableBoolean result) {
        int patternIndex = patternStartIndex;
        int wordIndex = wordStartIndex;
        while (patternIndex < pattern.length && wordIndex < word.length) {
            if (pattern[patternIndex] == '*') {
                if (patternIndex == pattern.length - 1) { // we are done
                    result.setValue(true);// we arrived at the end of pattern - match found!
                    return;
                } else {// skipping with asterisk and searching for a next real (not skipped) pattern element with backtracking - that's why we need recursion
                    for (int newStartingWordIndex = wordIndex; newStartingWordIndex < word.length; newStartingWordIndex++) {
                        isSubstringRec(word, newStartingWordIndex, pattern, patternIndex+1, result);
                    }
                    return;// we don't continue using this path
                }
            } else if (pattern[patternIndex] == '\\') { // it might mean we are escaping asterisk or it is just '\' literal
                
                if (patternIndex != pattern.length - 1 && pattern[patternIndex + 1] == '*') { // it is for escaping asterisk
                    if (word[wordIndex] == '*') {
                        wordIndex++;
                        patternIndex = patternIndex + 2;
                    }
                    else{
                        return;//matching failed
                    }
                } 
                else {                                                                     // it is '\' literal
                    if (word[wordIndex] != '\\') {
                        return;//matching failed
                    }
                    else{
                        patternIndex++;
                        wordIndex++;
                    }
                }
            } else if (pattern[patternIndex] == word[wordIndex]) {
                wordIndex++;
                patternIndex++;
            } else{
                return; // matching failed
            }
        }
        if(patternIndex==pattern.length){
            result.setValue(true);//we arrived at the end of pattern - match found!
            return;
        }
        //matching failed
    }

    private static class MutableBoolean {

        private boolean value;

        public MutableBoolean(boolean value) {
            this.setValue(value);
        }

        public boolean getValue() {
            return value;
        }

        public void setValue(boolean value) {
            this.value = value;
        }

    }

    /**
     * non recursive way, got stuck beacause of backtracking
     */
    private static boolean isSubstring(char[] word, char[] pattern) {
        int patternMinLength = getMinLengthOfPattern(pattern);
        System.out.println(patternMinLength);
        boolean isSubstring = false;
        // startingPos
        for (int i = 0; (i < word.length - patternMinLength) && !isSubstring; i++) {
            int j = i;
            boolean patternCurrentlyMatches = true;
            int patternIndex = 0;
            while (j < word.length && !isSubstring) {
                if (pattern[patternIndex] == '*') {
                    if (patternIndex == pattern.length - 1) {
                        isSubstring = true;
                        break;
                    } else {// skipping with asterisk and searching for next real char/not skipped char
                        int nextRealChar = pattern[patternIndex + 2];

                        patternIndex += 2;
                    }
                } else if (pattern[patternIndex] == word[i]) {

                }

                j++;
            }
        }
        return isSubstring;
    }

    private static int getMinLengthOfPattern(char[] pattern) {
        int minLength = 0;
        for (int i = 0; i < pattern.length; i++) {
            if (pattern[i] == '\\') {
                if (i < pattern.length - 1 && pattern[i + 1] == '*') {
                    i++;
                    minLength++;
                } else {
                    minLength++;
                }

            } else if (pattern[i] == '*') {
                // skip incrementing, asterisk can match zero elements
            } else {
                minLength++;
            }
        }
        return minLength;
    }

    private static class StringSearchingLineProcessor implements LineProcessor<List<String>> {

        public List<String> process(String line) {
            String[] str = line.split(",");
            return Arrays.asList(str);
        }

    }

}

package fun.algo.codeeval.hard;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class StringPermutations {

    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader bufferedReader = IOUtil.getBufferedReader("stringPermutations");
        List<String> input = IOUtil.getNonemptyLinesAsTrimmedStrings(bufferedReader);
        List<List<List<Character>>> result = new LinkedList<List<List<Character>>>();
        for (String stringForPermutations : input) {
            List<Character> chars = convert(stringForPermutations.toCharArray());
            result.add(getPermutations(chars));
        }
        printResults(result);

    }

    private static void printResults(List<List<List<Character>>> input) {

        List<List<String>> result = new LinkedList<List<String>>();
        for (List<List<Character>> chars : input) {
            List<String> permutationsForLine = new LinkedList<String>();
            for (int i = 0; i < chars.size(); i++) {
                StringBuilder sb = new StringBuilder();
                for (char ch : chars.get(i)) {
                    sb.append(ch);
                }
                permutationsForLine.add(sb.toString());
            }
            Collections.sort(permutationsForLine);
            result.add(permutationsForLine);
        }
        StringBuilder sb = new StringBuilder();
        for (List<String> permutationsForLineAsStrings : result) {
            for (int i = 0; i < permutationsForLineAsStrings.size() - 1; i++) {
                sb.append(permutationsForLineAsStrings.get(i));
                sb.append(",");
            }
            sb.append(permutationsForLineAsStrings.get(permutationsForLineAsStrings.size() - 1));
            sb.append("\n");
        }

        System.out.println(sb.toString());
    }


    private static List<Character> convert(char[] chars) {
        List<Character> result = new LinkedList<Character>();
        for (char ch : chars) {
            result.add(ch);
        }
        return result;
    }

    public static <T> List<List<T>> getPermutations(List<T> elements) {
        List<List<T>> results = new LinkedList<List<T>>();
        getPermutationsRec(elements, results, new LinkedList<T>());
        return results;
    }

    private static <T> void getPermutationsRec(List<T> elements, List<List<T>> results, List<T> currentSerie) {
        if (elements.isEmpty()) {
            results.add(currentSerie);
        } else {
            for (int i = 0; i < elements.size(); i++) {
                List<T> newCurrentSerie = new LinkedList<T>(currentSerie);
                newCurrentSerie.add(elements.get(i));
                List<T> newElementsList = new LinkedList<T>(elements);
                newElementsList.remove(i);
                getPermutationsRec(newElementsList, results, newCurrentSerie);
            }
        }
    }

}

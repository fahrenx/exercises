package fun.algo.codeeval.hard;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import fun.algo.codeeval.util.IOUtil;

public class ClosestPair {

    public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException {

        BufferedReader bufferedReader = IOUtil.getBufferedReader("closestPair");
        List<List<Integer>> input = IOUtil.getNonemptyLinesAsIntegersSequences(bufferedReader);
        List<Set<Point>> pointsSets = convertInputToSetOfPoints(input);
        List<Double> result = new LinkedList<Double>();
        for (Set<Point> set : pointsSets) {
            result.add(getClosestPairDistanceIn2DByDividing(set));

        }
        for (double dist : result) {
            if (dist > 10000) {
                System.out.println("INFINITY");
            } else {
                System.out.printf("%.4f", dist);
            }
        }

    }

    /**
     * refactor that
     */
    private static List<Set<Point>> convertInputToSetOfPoints(List<List<Integer>> input) {
        List<Set<Point>> pointsSets = new ArrayList<Set<Point>>();
        int morePointsExpected = 0;
        Set<Point> pointsSet = new HashSet<Point>();
        for (List<Integer> line : input) {
            if (morePointsExpected == 0) {
                if (pointsSet.size() > 0) {
                    pointsSets.add(new HashSet<Point>(pointsSet));
                    pointsSet.clear();
                }
                morePointsExpected = line.get(0);
            } else {
                pointsSet.add(new Point(line.get(0), line.get(1)));
                morePointsExpected--;
            }
        }
        if (pointsSet.size() > 0) {
            pointsSets.add(new HashSet<Point>(pointsSet));
            pointsSet.clear();
        }
        return pointsSets;
    }

    private static Double getClosestPairDistanceIn1DByDividing(Set<Point> points) {

        List<Point> sortedPoints = new ArrayList<Point>(points);
        Collections.sort(sortedPoints, new Comparator<Point>() {
            public int compare(Point p1, Point p2) {
                if (p1.x > p2.x) {
                    return 1;
                } else if (p1.x == p2.x) {
                    return 0;
                } else {
                    return -1;
                }
            }
        });
        return getClosestPairDistanceIn1DByDividing(sortedPoints);
    }

    private static Double getClosestPairDistanceIn2DByDividing(Set<Point> points) {

        List<Point> sortedPoints = new ArrayList<Point>(points);
        Collections.sort(sortedPoints, new Comparator<Point>() {
            public int compare(Point p1, Point p2) {
                if (p1.x > p2.x) {
                    return 1;
                } else if (p1.x == p2.x) {
                    return 0;
                } else {
                    return -1;
                }
            }
        });
        return getClosestPairDistanceIn2DByDividing(sortedPoints);
    }

    private static Double getClosestPairDistanceIn2DByDividing(List<Point> sortedPoints) {
        if (sortedPoints.size() <= 1) {
            return Double.MAX_VALUE;
        }
        if (sortedPoints.size() == 2) {
            return getDistance(sortedPoints.get(1), sortedPoints.get(0));
        }
        int firstMiddlePointIndex = sortedPoints.size() / 2;
        int secondMiddlePointIndex = sortedPoints.size() / 2 + 1;
        double pivot = (sortedPoints.get(firstMiddlePointIndex).x + sortedPoints.get(secondMiddlePointIndex).x) / 2;
        double leftSetMin = getClosestPairDistanceIn2DByDividing(sortedPoints.subList(0, firstMiddlePointIndex));
        double rightSetMin = getClosestPairDistanceIn2DByDividing(sortedPoints.subList(0, secondMiddlePointIndex));

        // --------------this is an interesting fragment - finding min distance of elements around pivot

        // this is our ro - our strip size - strip which denotes the area of our search
        double leftRightMin = Collections.min(Arrays.asList(leftSetMin, rightSetMin));

        // finding all elements on left side that are in strip - only they will be compared with right side elements
        List<Point> leftSidePointsInStrip = new LinkedList<Point>();
        int j = firstMiddlePointIndex;
        while (j >= 0 && (pivot - sortedPoints.get(j).x) < leftRightMin) {
            leftSidePointsInStrip.add(sortedPoints.get(j));
            j--;
        }

        // each of points selected in previous step will be compared with at most 6 points from right side set
        double minAroundPivot = Double.MAX_VALUE;
        for (Point p : leftSidePointsInStrip) {
            int numberOfComparison = 0;
            int k = secondMiddlePointIndex; // it is left-most element of righ side set - here we are starting our search of right side elements to compare to

            // second condition checks if right side point is within the width of the rectangle (or in other words - if is inside the strip)
            while (k < sortedPoints.size() && numberOfComparison < 6 && (sortedPoints.get(k).x - pivot) < leftRightMin) {
                // this condition checks if right side point is within the height of rectangle
                if (Math.abs(p.y - sortedPoints.get(k).y) < leftRightMin) {
                    double distance = getDistance(p, sortedPoints.get(k));
                    if (distance < minAroundPivot) {
                        minAroundPivot = distance;
                    }

                }
                numberOfComparison++;
                k++;
            }

        }

        double min = Collections.min(Arrays.asList(leftSetMin, rightSetMin, minAroundPivot));
        return min;
    }

    private static Double getDistance(Point point1, Point point2) {
        return Math.sqrt(Math.pow(point1.x - point2.x, 2) + Math.pow(point1.y - point2.y, 2));
    }

    private static double getClosestPairDistanceIn1DByDividing(List<Point> sortedPoints) {
        if (sortedPoints.size() <= 1) {
            return Double.MAX_VALUE;
        }
        if (sortedPoints.size() == 2) {
            return sortedPoints.get(1).x - sortedPoints.get(0).x;
        }
        int firstMiddlePointIndex = sortedPoints.size() / 2;
        int secondMiddlePointIndex = sortedPoints.size() / 2 + 1;
        // int pivot = (sortedPoints.get(firstMiddlePointIndex).x + sortedPoints.get(secondMiddlePointIndex).x) / 2;
        double leftSetMin = getClosestPairDistanceIn1DByDividing(sortedPoints.subList(0, firstMiddlePointIndex));
        double rightSetMin = getClosestPairDistanceIn1DByDividing(sortedPoints.subList(0, secondMiddlePointIndex));
        // finding closest pair around pivot
        double pivotMin = Math.abs(sortedPoints.get(secondMiddlePointIndex).x - sortedPoints.get(firstMiddlePointIndex).x);
        double min = Collections.min(Arrays.asList(leftSetMin, rightSetMin, pivotMin));
        return min;
    }

    private static Double getClosestPairDistanceIn1DBySorting(Set<Point> points) {

        List<Point> sortedPoints = new ArrayList<Point>(points);
        Collections.sort(sortedPoints, new Comparator<Point>() {
            public int compare(Point p1, Point p2) {
                if (p1.x > p2.x) {
                    return 1;
                } else if (p1.x == p2.x) {
                    return 0;
                } else {
                    return -1;
                }
            }
        });

        double min = Double.MAX_VALUE;
        for (int i = 1; i < points.size(); i++) {
            double diff = sortedPoints.get(i).x - sortedPoints.get(i - 1).x;
            if (diff < min) {
                min = diff;
            }
        }
        return min;
    }
}

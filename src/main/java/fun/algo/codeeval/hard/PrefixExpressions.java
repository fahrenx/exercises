package fun.algo.codeeval.hard;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import fun.algo.codeeval.util.IOUtil;

public class PrefixExpressions {

    private static final Set<String> OPERATORS_STRINGS = new HashSet<String>(Arrays.asList(new String[] { "+", "-", "*", "/" }));

    public static void main(String[] args) throws NumberFormatException, IOException {
        BufferedReader bufferedReader = IOUtil.getBufferedReader("prefixExpressions");
        List<String> input = IOUtil.getNonemptyLinesAsTrimmedStrings(bufferedReader);
        List<Integer> result = new LinkedList<Integer>();
        for (String polishNotationExpression : input) {
            result.add(evaluatePolishNotationFromLeft(polishNotationExpression));
        }
        IOUtil.print(result);

    }
    
    private static int evaluatePolishNotationFromLeft(String polishNotationExpression) {
        List<PolishNotationElement> expression = convertPolishNotationExpression(polishNotationExpression);
        LinkedList<Operand> operandsStack = new LinkedList<Operand>();
        LinkedList<Operator> operatorsStack = new LinkedList<Operator>();
        for (PolishNotationElement element : expression) {
            if (element.isOperator()) {
                operatorsStack.push((Operator) element);
            } else {
                operandsStack.push((Operand) element);
            }
            if (operandsStack.size() >= 2) {
                computeValue(operandsStack, operatorsStack);
            }
        }
        while (!operatorsStack.isEmpty()) {
            computeValue(operandsStack, operatorsStack);
        }
        return operandsStack.pop().getValue();
    }

    private static int evaluatePolishNotationFromRight(String polishNotationExpression) {
        List<PolishNotationElement> expression = convertPolishNotationExpression(polishNotationExpression);
        LinkedList<Operand> operandsStack = new LinkedList<Operand>();
        LinkedList<Operator> operatorsStack = new LinkedList<Operator>();
        Collections.reverse(expression);
        for (PolishNotationElement element : expression) {
            if (element.isOperator()) {
                operatorsStack.push((Operator) element);
                computeValue(operandsStack, operatorsStack);
            } else {
                operandsStack.push((Operand) element);
            }
        }
        while (!operatorsStack.isEmpty()) {
            computeValue(operandsStack, operatorsStack);
        }
        return operandsStack.pop().getValue();
    }

    private static void computeValue(LinkedList<Operand> operandsStack, LinkedList<Operator> operatorsStack) {
        Operand latestOperand = operandsStack.pop();
        Operand previousOperand = operandsStack.pop();
        Operand operand = new Operand(operatorsStack.pop().computeValue(previousOperand.getValue(), latestOperand.getValue()));
        operandsStack.push(operand);
    }

    private static List<PolishNotationElement> convertPolishNotationExpression(String polishNotationExpression) {
        List<PolishNotationElement> result = new LinkedList<PolishNotationElement>();
        String[] splitArr = polishNotationExpression.split("\\s");
        for (String element : splitArr) {
            element = element.trim();
            if(OPERATORS_STRINGS.contains(element)){
                result.add(OperatorsFactory.createOperator(element));
            }
            else{
                result.add(new Operand(Integer.valueOf(element)));
            }
        }
        return result;
    }

    private static interface PolishNotationElement {
        boolean isOperator();
    }

    private static class Operand implements PolishNotationElement {

        private final int value;

        public Operand(int value) {
            this.value = value;
        }

        public int getValue() {
            return value;
        }


        public boolean isOperator() {
            return false;
        }

        @Override
        public String toString() {
            return "" + value;
        }
    }

    private static abstract class Operator implements PolishNotationElement {
        public abstract int computeValue(int operand1, int operand2);

        public boolean isOperator() {
            return true;
        }

        @Override
        public String toString() {
            return getClass().getSimpleName();
        }
    }

    private static class DivisionOperator extends Operator {

        @Override
        public int computeValue(int operand1, int operand2) {
            return operand1 / operand2;
        }

    }

    private static class SummationOperator extends Operator {

        @Override
        public int computeValue(int operand1, int operand2) {
            return operand1 + operand2;
        }

    }

    private static class SubtractionOperator extends Operator {

        @Override
        public int computeValue(int operand1, int operand2) {
            return operand1 - operand2;
        }

    }

    private static class MultiplicationOperator extends Operator {

        @Override
        public int computeValue(int operand1, int operand2) {
            return operand1 * operand2;
        }

    }

    private static class OperatorsFactory {

        public static Operator createOperator(String operatorString) {
            if (operatorString.equals("+")) {
                return new SummationOperator();
            } else if (operatorString.equals("-")) {
                return new SubtractionOperator();
            } else if (operatorString.equals("*")) {
                return new MultiplicationOperator();
            } else if (operatorString.equals("/")) {
                return new DivisionOperator();
            } else {
                throw new IllegalArgumentException();
            }
        }
    }

}

package fun.algo.codeeval.hard;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import fun.algo.codeeval.util.IOUtil;

public class TelephoneWords {

    static Map<Integer, List<String>> mapping = new HashMap<Integer, List<String>>() {
        {
            put(0, Arrays.asList("0"));
            put(1, Arrays.asList("1"));
            put(2, Arrays.asList("a", "b", "c"));
            put(3, Arrays.asList("d", "e", "f"));
            put(4, Arrays.asList("g", "h", "i"));
            put(5, Arrays.asList("j", "k", "l"));
            put(6, Arrays.asList("m", "n", "o"));
            put(7, Arrays.asList("p", "q", "r", "s"));
            put(8, Arrays.asList("t", "u", "v"));
            put(9, Arrays.asList("w", "x", "y", "z"));
        }
    };

    public static void main(String[] args) throws NumberFormatException, IOException, InterruptedException {

        BufferedReader bufferedReader = IOUtil.getBufferedReader("telephoneWords");
        List<List<Integer>> input = IOUtil.getNonemptyLinesWithCustomProcessing(bufferedReader, new Telephone7DigitsLineProcessor());
        List<List<String>> result = new LinkedList<List<String>>();
        for (List<Integer> seq : input) {
            List<String> allPossibleWords = getAllPossibleWords(seq);
            Collections.sort(allPossibleWords);
            result.add(allPossibleWords);
        }
        IOUtil.print(result, ",");

    }

    private static List<String> getAllPossibleWords(List<Integer> digits) {
        List<List<String>> mappedToAll = new LinkedList<List<String>>();
        for (Integer i : digits) {
            mappedToAll.add(mapping.get(i));
        }
        List<String> result = getAllPossibleSequences(mappedToAll);
        return result;
    }

    private static List<String> getAllPossibleSequences(List<List<String>> mappedToAll) {
        int pos = 0;
        List<String> results = new LinkedList<String>();
        List<String> currentSequence = new LinkedList<String>();
        getAllPossibleSequencesRec(mappedToAll, pos, currentSequence, results);
        return results;
    }

    private static void getAllPossibleSequencesRec(List<List<String>> mappedToAll, int pos, List<String> currentSequence, List<String> results) {
        if (pos == mappedToAll.size()) {
            results.add(converToSingleString(currentSequence));
            return;
        }
        for (String mapping : mappedToAll.get(pos)) {
            List<String> newSeq = new LinkedList<String>(currentSequence);
            newSeq.add(mapping);
            getAllPossibleSequencesRec(mappedToAll, pos + 1, newSeq, results);
        }
    }

    private static String converToSingleString(List<String> currentSequence) {
        StringBuilder sb = new StringBuilder();
        for (String str : currentSequence) {
            sb.append(str);
        }
        return sb.toString();
    }

    private static class Telephone7DigitsLineProcessor implements IOUtil.LineProcessor<List<Integer>> {

        public List<Integer> process(String line) {
            List<Integer> result = new LinkedList<Integer>();
            for (char c : line.toCharArray()) {
                result.add(Integer.valueOf(c - '0'));
            }
            return result;
        }

    }
}

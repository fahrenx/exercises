package fun.algo.codeeval.hard;

import java.lang.reflect.InvocationTargetException;
import java.util.HashSet;
import java.util.Set;


public class MonkeyGridWalk {

    private static final int THRESHOLD = 19;

    public static void main(String[] args) {

        int numberOfPossibleMovements = findNumberOfAccessibleElementsOwnStackFor2DPlane();
        // int numberOfPossibleMovements = findNumberOfAccessibleElementsRecursion();

        System.out.println(numberOfPossibleMovements);
    }

    public static void main2(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    }

    static int findNumberOfAccessibleElementsOwnStackFor2DPlane() {
        int elementsBetweenAxes = findNumberOfAccessibleElementsOwnStack(1, 1, new AccessibilityChecker() {
            public boolean isAccessible(int x, int y) {
                return (x > 0 && y > 0) && getSumOfDigits(x) + getSumOfDigits(y) < THRESHOLD;
            }
        });
        int elementsOnNonNegativePartOfAxes = findNumberOfAccessibleElementsOwnStack(0, 0, new AccessibilityChecker() {
            public boolean isAccessible(int x, int y) {
                return (x == 0 || y == 0) && getSumOfDigits(x) + getSumOfDigits(y) < THRESHOLD;
            }
        });
        // System.out.println(elementsOnNonNegativePartOfAxes);
        int elementsOnZeroPoint = 1;
        return 4 * elementsBetweenAxes + 2 * elementsOnNonNegativePartOfAxes - elementsOnZeroPoint;
    }

    public static void testStackVersion() {
        findNumberOfAccessibleElementsOwnStack(0, 0, new AccessibilityChecker() {
            public boolean isAccessible(int x, int y) {
                return getSumOfDigits(x) + getSumOfDigits(y) < THRESHOLD;
            }
        });
    }

    public static void testRecursiveVersion() {
        findNumberOfAccessibleElementsRecursion();
    }

    private static int findNumberOfAccessibleElementsRecursion() {
        HashSet<Tuple> visited = new HashSet<Tuple>();
        findNumberOfAccessibleElementsRec(visited, 0, 0);
        // System.out.println(visited);
        return 4 * visited.size();
    }

    /*
     * it may easily cause stackoverflowerror due to heavy use of recursion
     */
    private static void findNumberOfAccessibleElementsRec(Set<Tuple> visited, int currentPosX, int currentPosY) {

        // System.out.println(visited);
        int yUp = currentPosY - 1;
        if (yUp >= 0 && !visited.contains(new Tuple(currentPosX, yUp))) {// up
            if (isAccessible(currentPosX, yUp)) {
                visited.add(new Tuple(currentPosX, yUp));
                findNumberOfAccessibleElementsRec(visited, currentPosX, yUp);
            }
        }
        int yDown = currentPosY + 1;
        if (!visited.contains(new Tuple(currentPosX, yDown))) {// down
            if (isAccessible(currentPosX, yDown)) {
                visited.add(new Tuple(currentPosX, yDown));
                findNumberOfAccessibleElementsRec(visited, currentPosX, yDown);
            }
        }
        int xLeft = currentPosX - 1;
        if (xLeft >= 0 && !visited.contains(new Tuple(xLeft, currentPosY))) {// left
            if (isAccessible(xLeft, currentPosY)) {
                visited.add(new Tuple(xLeft, currentPosY));
                findNumberOfAccessibleElementsRec(visited, xLeft, currentPosY);
            }
        }
        int xRight = currentPosX + 1;
        if (!visited.contains(new Tuple(xRight, currentPosY))) {// right
            if (isAccessible(xRight, currentPosY)) {
                visited.add(new Tuple(xRight, currentPosY));

                findNumberOfAccessibleElementsRec(visited, xRight, currentPosY);
            }
        }
    }

    /**
     * like the one above but with my own stack to avoid stackoverflow errors
     * 
     * @return
     */
    private static int findNumberOfAccessibleElementsOwnStack(int initialPosX, int initialPosY, AccessibilityChecker accessibilityChecker) {

        Set<Tuple> elementsToVisit = new HashSet<Tuple>();
        Set<Tuple> visited = new HashSet<Tuple>();
        elementsToVisit.add(new Tuple(initialPosX, initialPosY));
        while (!elementsToVisit.isEmpty()) {
            // System.out.println(elementsToVisit);
            Tuple currentElement = elementsToVisit.iterator().next();
            elementsToVisit.remove(currentElement);
            int currentPosX = currentElement.x;
            int currentPosY = currentElement.y;
            int yUp = currentPosY - 1;
            if (yUp >= 0 && !visited.contains(new Tuple(currentPosX, yUp))) {// up
                if (accessibilityChecker.isAccessible(currentPosX, yUp)) {
                    elementsToVisit.add(new Tuple(currentPosX, yUp));
                }
            }
            int yDown = currentPosY + 1;
            if (!visited.contains(new Tuple(currentPosX, yDown))) {// down
                if (accessibilityChecker.isAccessible(currentPosX, yDown)) {
                    elementsToVisit.add(new Tuple(currentPosX, yDown));
                }
            }
            int xLeft = currentPosX - 1;
            if (xLeft >= 0 && !visited.contains(new Tuple(xLeft, currentPosY))) {// left
                if (accessibilityChecker.isAccessible(xLeft, currentPosY)) {
                    elementsToVisit.add(new Tuple(xLeft, currentPosY));
                }
            }
            int xRight = currentPosX + 1;
            if (!visited.contains(new Tuple(xRight, currentPosY))) {// right
                if (accessibilityChecker.isAccessible(xRight, currentPosY)) {
                    elementsToVisit.add(new Tuple(xRight, currentPosY));
                }
            }
            visited.add(new Tuple(currentPosX, currentPosY));
        }
        return visited.size();

    }

    private static interface AccessibilityChecker {

        boolean isAccessible(int x, int y);

    }


    private static boolean isAccessible(int currentPosX, int currentPosY) {
        return getSumOfDigits(currentPosX) + getSumOfDigits(currentPosY) < THRESHOLD;
    }

    public static int getSumOfDigits(int number) {
        int sum = 0;
        while (number > 0) {
            sum += number % 10;
            number /= 10;
        }

        return sum;
    }

    private static class Tuple {
        final int x;
        final int y;

        Tuple(int x, int y) {
            this.x = x;
            this.y = y;
        }

        @Override
        public String toString() {
            return " [x=" + x + ", y=" + y + "]";
        }

        public int hashCode() {
            return x * y * 31;
        }

        public boolean equals(Object o) {
            if (o == null)
                return false;
            if (!(o instanceof Tuple)) {
                return false;
            }
            Tuple tuple = (Tuple) o;
            if (this.x != tuple.x) {
                return false;
            }
            if (this.y != tuple.y) {
                return false;
            }
            return true;
        }

    }
}

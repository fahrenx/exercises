package fun.algo.codeeval.hard;

import java.io.BufferedReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.List;

import fun.algo.codeeval.util.IOUtil;

public class PalindromicRanges {
    public static void main2(String[] args) throws NumberFormatException, IOException, InterruptedException {

        BufferedReader bufferedReader = IOUtil.getBufferedReader("palindromicRanges");
        List<List<Integer>> input = IOUtil.getNonemptyLinesAsIntegersSequences(bufferedReader);
        List<Integer> result = new LinkedList<Integer>();
        for (List<Integer> range : input) {
            result.add(getNumberOfSubrangeWithEvenPalindromsNumberBruteforce(range.get(0), range.get(1)));
        }
        IOUtil.print(result);

    }

    /**
     * intersting how more efficient for big value dynamic programming solution is,
     *  for this value set:
        getNumberOfSubrangeWithEvenPalindromsNumber(1, 2);
        getNumberOfSubrangeWithEvenPalindromsNumber(1, 7);
        getNumberOfSubrangeWithEvenPalindromsNumber(87, 88);
        getNumberOfSubrangeWithEvenPalindromsNumber(87, 93);
        getNumberOfSubrangeWithEvenPalindromsNumber(2, 9);
        getNumberOfSubrangeWithEvenPalindromsNumber(11, 21);
        getNumberOfSubrangeWithEvenPalindromsNumber(1, 200);
     * 
     * the difference is huge: (dynamic programming solution is more than 250 times faster)
     * method: testDynamicProgrammingSolution( runs: 1000 ) avg time: 0.184424374 ms
     * method: testBruteforceSolution( runs: 1000 ) avg time: 49.201737558 ms
     */
    public static void main(String[] args) throws IllegalArgumentException, IllegalAccessException, InvocationTargetException {
    }

    public static void testDynamicProgrammingSolution() {
        getNumberOfSubrangeWithEvenPalindromsNumber(1, 2);
        getNumberOfSubrangeWithEvenPalindromsNumber(1, 7);
        getNumberOfSubrangeWithEvenPalindromsNumber(87, 88);
        getNumberOfSubrangeWithEvenPalindromsNumber(87, 93);
        getNumberOfSubrangeWithEvenPalindromsNumber(2, 9);
        getNumberOfSubrangeWithEvenPalindromsNumber(11, 21);
        getNumberOfSubrangeWithEvenPalindromsNumber(1, 200);
    }

    public static void testBruteforceSolution() {
        getNumberOfSubrangeWithEvenPalindromsNumberBruteforce(1, 2);
        getNumberOfSubrangeWithEvenPalindromsNumberBruteforce(1, 7);
        getNumberOfSubrangeWithEvenPalindromsNumberBruteforce(87, 88);
        getNumberOfSubrangeWithEvenPalindromsNumberBruteforce(87, 93);
        getNumberOfSubrangeWithEvenPalindromsNumberBruteforce(2, 9);
        getNumberOfSubrangeWithEvenPalindromsNumberBruteforce(11, 21);
        getNumberOfSubrangeWithEvenPalindromsNumberBruteforce(1, 200);

    }

    /**
     * bruteForceSolution
     */
    private static int getNumberOfSubrangeWithEvenPalindromsNumberBruteforce(Integer start, Integer end) {
        int numberOfEvenRanges = 0;
        for (int i = 0; i < end - start + 1; i++) {
            for (int j = i; j < end - start + 1; j++) {
                int numberOfPalindromes = 0;
                for (int k = start + i; k <= start + j; k++) {
                    if (isPalindrome(k)) {
                        numberOfPalindromes++;
                    }

                }
                if (numberOfPalindromes % 2 == 0) {
                    numberOfEvenRanges++;
                }
            }


        }
        return numberOfEvenRanges;
    }

    /**
     * solution using dynamic programming
     */
    private static int getNumberOfSubrangeWithEvenPalindromsNumber(Integer start, Integer end) {

        // array on x axis has starting positions and on y axis has range length
        int[][] rangesAndPalindroms = new int[end - start + 1][end - start + 1];
        // initialisation
        int currentNumberOfPalindroms = 0;
        for (int i = start; i <= end; i++) {
            if (isPalindrome(i)) {
                currentNumberOfPalindroms++;
            }
            // 'start' is subtracted to translate absolute values to relative ones
            rangesAndPalindroms[0][i - start] = currentNumberOfPalindroms;
        }
        // filling out other values in table
        for (int i = 1; i < end - start + 1; i++) {
            for (int j = 0; j < end - start + 1 - i; j++) {
                // it is better to solve example on paper to explain it but briefly: f([3,5]) = f([2,5])-f([2,2])
                rangesAndPalindroms[i][j] = rangesAndPalindroms[i - 1][j + 1] - rangesAndPalindroms[i - 1][0];
            }
            for (int j = end - start + 1 - i; j < end - start + 1; j++) {
                rangesAndPalindroms[i][j] = -1;
            }

        }
        return countNumberOfEvenValues(rangesAndPalindroms);
    }

    private static int countNumberOfEvenValues(int[][] arr) {
        int numberOfEvenValues = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr[i].length; j++) {
                if (arr[i][j] % 2 == 0) {
                    numberOfEvenValues++;
                }
            }

        }
        return numberOfEvenValues;
    }

    private static boolean isPalindrome(int number) {
        String numberStr = String.valueOf(number);
        if (numberStr.length() <= 1) {
            return true;
        }
        int i=0;
        int j=numberStr.length()-1;
        while (i < j) {
            if(numberStr.charAt(i)!=numberStr.charAt(j)){
                return false;
            }
            i++;
            j--;
        }
        return true;
    }
}

package fun.algo.combinatorics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import fun.algo.codeeval.util.IOUtil;

public class CombinatoricsTODOmoreEffectiveClean {

    public static long getNumberOfCombinations(int k, int n) {
        throw new UnsupportedOperationException();
    }
    
    public static void main(String[] args) {
        // getAllCombinationsRec(Arrays.asList(1, 2, 3, 4, 5, 6, 7), 3);
        IOUtil.print(getPermutations(Arrays.asList(1, 2, 3)));
    }

    public static <T> List<List<T>> getPermutations(List<T> elements) {
        List<List<T>> results = new LinkedList<List<T>>();
        getPermutationsRec(elements, results, new LinkedList<T>());
        return results;
    }


    private static <T> void getPermutationsRec(List<T> elements, List<List<T>> results, List<T> currentSerie) {
        if (elements.isEmpty()) {
            results.add(currentSerie);
        } else {
            for (int i = 0; i < elements.size(); i++) {
                // TODO can we improve this? do we realy have to create so many new collections?
                List<T> newCurrentSerie = new LinkedList<T>(currentSerie);
                newCurrentSerie.add(elements.get(i));
                List<T> newElementsList = new LinkedList<T>(elements);
                newElementsList.remove(i);
                getPermutationsRec(newElementsList, results, newCurrentSerie);
            }
        }
    }

    public static <T> List<Set<T>> getAllCombinationsRec(List<T> elements, int k) {
        List<List<Integer>> indicesList = generateIndicesList(new ArrayList<List<Integer>>(), new ArrayList<Integer>(), 0, elements.size(), k);
        List<Set<T>> result = convertIndicesToFinalResult(elements, indicesList);
        return result;
    }

    /**
     * merge with permutationsSimple
     */
    public static <T> Set<List<T>> sequencesWithoutRepetition(List<T> elements, int k) {
        Set<List<T>> results = new HashSet<List<T>>();
        sequencesWithoutRepetitionRec(elements, k, new LinkedList<T>(), results);
        return results;
    }
    
    /**
     * merge with without repetition
     */
    public static <T> Set<List<T>> sequencesWithRepetition(List<T> elements, int k) {
        Set<List<T>> results = new HashSet<List<T>>();
        sequencesWithRepetitionRec(elements, k, new LinkedList<T>(), results);
        return results;
    }

    private static <T> void sequencesWithRepetitionRec(List<T> elements, int k, List<T> currentSequence, Set<List<T>> results) {
        if (currentSequence.size() == k) {
            results.add(currentSequence);
            return;
        }
        for (T el : elements) {
            List<T> newCurrentSequence = new LinkedList<T>(currentSequence);
            newCurrentSequence.add(el);
            sequencesWithRepetitionRec(elements, k, newCurrentSequence, results);
        }
    }

    private static<T> void sequencesWithoutRepetitionRec(List<T> elements, int k, List<T> currentSequence, Set<List<T>> results) {
        if (currentSequence.size() == k) {
            results.add(currentSequence);
            return;
        }
        for (T el : elements) {
            List<T> newElements = new LinkedList<T>(elements);
            newElements.remove(el);
            List<T> newCurrentSequence = new LinkedList<T>(currentSequence);
            newCurrentSequence.add(el);
            sequencesWithoutRepetitionRec(newElements, k, newCurrentSequence, results);
        }
    }


    private static List<List<Integer>> generateIndicesList(List<List<Integer>> result, List<Integer> currentSerie, int currentStartingIndex, int n, int k) {
        if(currentSerie.size()==k){
            result.add(currentSerie);
            return result;
        }
        int remaining = k-currentSerie.size()-1;
        int limit = n - remaining;
        for (int i = currentStartingIndex; i < limit; i++) {
            List<Integer> newSerie = new ArrayList<Integer>(currentSerie);
            newSerie.add(i);
            generateIndicesList(result, newSerie, i + 1, n, k);
        }
        return result;
    }

    private static <T> List<Set<T>> convertIndicesToFinalResult(List<T> elements, List<List<Integer>> indicesLists) {
        List<Set<T>> result = new LinkedList<Set<T>>();
        for (List<Integer> indicesList : indicesLists) {
            Set<T> resultEntry = new HashSet<T>();
            for (Integer index : indicesList) {
                resultEntry.add(elements.get(index));
            }
            result.add(resultEntry);
        }
        return result;
    }


    private static void printResults(List<List<Integer>> indicesList) {
        System.out.println("number:" + indicesList.size());
        for (List<Integer> list : indicesList) {
            System.out.println(list);
        }
    }

}

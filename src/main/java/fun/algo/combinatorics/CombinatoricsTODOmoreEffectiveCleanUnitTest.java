package fun.algo.combinatorics;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import static org.assertj.core.api.Assertions.*;

import org.junit.Test;

public class CombinatoricsTODOmoreEffectiveCleanUnitTest {

    @SuppressWarnings("serial")
    @Test
    public void givenSimpleIntSet1_whenGetAllCombinations_thenCorrectResultReturned() {
        List<Set<Integer>> allCombinations = CombinatoricsTODOmoreEffectiveClean.getAllCombinationsRec(new ArrayList<Integer>() {
            {
                add(1);
                add(2);
                add(3);
            }
        }, 2);
        Set<Integer> a = new HashSet<Integer>(){{add(1);add(2);}};
        Set<Integer> b = new HashSet<Integer>(){{add(1);add(3);}};
        Set<Integer> c = new HashSet<Integer>(){{add(3);add(2);}};
        assertThat(allCombinations).contains(a, b, c);

    }
    
    @SuppressWarnings("serial")
    @Test
    public void givenSimpleIntSet2_whenGetAllCombinations_thenCorrectResultReturned() {
        List<Set<Integer>> allCombinations = CombinatoricsTODOmoreEffectiveClean.getAllCombinationsRec(new ArrayList<Integer>() {
            {
                add(1);
                add(2);
            }
        }, 2);
        Set<Integer> expectedResult = new HashSet<Integer>() {
            {
                add(1);
                add(2);
            }
        };
        assertThat(allCombinations).contains(expectedResult);
    }
    
    @SuppressWarnings("serial")
    @Test
    public void givenSimpleIntTestSamplew_whenGetPermutations_thenCorrectResultReturned() {
        List<List<Integer>> allCombinations = CombinatoricsTODOmoreEffectiveClean.getPermutations(new ArrayList<Integer>() {
            {
                add(1);
                add(2);
                add(3);
            }
        });
        List<Integer> perm1 = new LinkedList<Integer>(Arrays.asList(1, 2, 3));
        List<Integer> perm2 = new LinkedList<Integer>(Arrays.asList(1, 3, 2));
        List<Integer> perm3 = new LinkedList<Integer>(Arrays.asList(2, 1, 3));
        List<Integer> perm4 = new LinkedList<Integer>(Arrays.asList(2, 3, 1));
        List<Integer> perm5 = new LinkedList<Integer>(Arrays.asList(3, 1, 2));
        List<Integer> perm6 = new LinkedList<Integer>(Arrays.asList(3, 2, 1));

        assertThat(allCombinations).contains(perm1, perm2, perm3, perm4, perm5, perm6);
    }

    @SuppressWarnings("serial")
    @Test
    public void givenSimpleIntTestSamplew_whenGetSequencesWithoutRepetition_thenCorrectResultReturned() {
        Set<List<Integer>> allSequences = CombinatoricsTODOmoreEffectiveClean.sequencesWithoutRepetition(new ArrayList<Integer>() {
            {
                add(1);
                add(2);
                add(3);
            }
        }, 2);
        List<Integer> seq1 = new LinkedList<Integer>(Arrays.asList(1, 2));
        List<Integer> seq2 = new LinkedList<Integer>(Arrays.asList(2, 1));
        List<Integer> seq3 = new LinkedList<Integer>(Arrays.asList(1, 3));
        List<Integer> seq4 = new LinkedList<Integer>(Arrays.asList(3, 1));
        List<Integer> seq5 = new LinkedList<Integer>(Arrays.asList(2, 3));
        List<Integer> seq6 = new LinkedList<Integer>(Arrays.asList(3, 2));

        assertThat(allSequences).contains(seq1, seq2, seq3, seq4, seq5, seq6);
    }

}

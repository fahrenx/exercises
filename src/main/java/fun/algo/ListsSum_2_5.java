package fun.algo;

import java.util.Arrays;

import fun.algo.datastructures.SinglyLinkedList;
import fun.algo.generators.Generators;

public class ListsSum_2_5 {

    public static void main(String[] args) {

        SinglyLinkedList<Integer> list1 = Generators.generateSinglyLinkedList(Arrays.asList(9, 9, 9));
        SinglyLinkedList<Integer> list2 = Generators.generateSinglyLinkedList(Arrays.asList(9, 9, 1));
        System.out.println(sumTwoDigitListsWithReversedOrderIterative(list1, list2));
        SinglyLinkedList.Node<Integer> result = sumTwoDigitListsWithReversedOrderRecursive(list1.head, list2.head, 0);

        while (result != null) {
            System.out.print(result);
            result = result.next;
        }

        System.out.println(sumTwoDigitListsWithReversedOrderRecursive2(list1, list2));

    }

    public static SinglyLinkedList<Integer> sumTwoDigitListsWithReversedOrderIterative(SinglyLinkedList<Integer> list1, SinglyLinkedList<Integer> list2) {

        if (list1.head == null && list2.head == null) {
            return list1;
        } else if (list1.head == null) {
            return list2;
        } else if (list2.head == null) {
            return list1;
        }
        SinglyLinkedList.Node<Integer> list1Node = list1.head;
        SinglyLinkedList.Node<Integer> list2Node = list2.head;
        SinglyLinkedList<Integer> result = new SinglyLinkedList<Integer>();
        int carry = 0;
        while (list1Node != null && list2Node != null) {
            int sum = list1Node.value + list2Node.value;
            System.out.println("sum without carry" + sum);
            sum += carry;
            carry = 0;
            if (sum >= 10) {
                carry = carry + (sum / 10);
                sum = sum % 10;
            }
            System.out.println("carry" + carry);
            result.append(new SinglyLinkedList.Node<Integer>(sum));
            list1Node = list1Node.next;
            list2Node = list2Node.next;
            System.out.println("sum" + sum);
            System.out.println();
        }
        while (list1Node != null) {
            result.append(new SinglyLinkedList.Node<Integer>(list1Node.value + carry));
            list1Node = list1Node.next;
            carry = 0;
        }
        while (list2Node != null) {
            result.append(new SinglyLinkedList.Node<Integer>(list2Node.value + carry));
            list2Node = list2Node.next;
            carry = 0;
        }
        if (carry > 0) {
            result.append(new SinglyLinkedList.Node<Integer>(carry));
        }
        return result;
    }

    public static SinglyLinkedList.Node<Integer> sumTwoDigitListsWithReversedOrderRecursive(SinglyLinkedList.Node<Integer> list1Head, SinglyLinkedList.Node<Integer> list2Head, int carry) {
        if (list1Head == null && list2Head == null) {
            if (carry > 0) {
                return new SinglyLinkedList.Node<Integer>(carry);
            } else {
                return null;
            }
        }
        int value;
        if (list1Head == null) {
            value = list2Head.value;
            list2Head = list2Head.next;
        } else if (list2Head == null) {
            value = list1Head.value;
            list1Head = list1Head.next;
        } else {
            value = list1Head.value + list2Head.value;
            list1Head = list1Head.next;
            list2Head = list2Head.next;
        }
        value = value+carry;
        if(value>=10){
            carry = value / 10;
            value = value % 10;
        }
        SinglyLinkedList.Node<Integer> node = new SinglyLinkedList.Node<Integer>(value);
        node.next = sumTwoDigitListsWithReversedOrderRecursive(list1Head, list2Head, carry);
        return node;
    }

    /**
     * another version with just a different signature to show that we can be quite flexible when it comes to recursive and function and their signatures
     */
    public static SinglyLinkedList<Integer> sumTwoDigitListsWithReversedOrderRecursive2(SinglyLinkedList<Integer> list1Head, SinglyLinkedList<Integer> list2Head) {

        SinglyLinkedList.Node<Integer> result = sumTwoDigitListsWithReversedOrderRecursive(list1Head.head, list2Head.head, 0);
        return new SinglyLinkedList<Integer>(result);
    }

}

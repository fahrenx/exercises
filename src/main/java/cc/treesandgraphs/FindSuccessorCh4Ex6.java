package cc.treesandgraphs;

import utils.trees.NodeP;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * lessons learnt
 * - check many different cases, don't concentrate on just one (like no children case)
 */
public class FindSuccessorCh4Ex6 {

    public static void main(String[] args) {
        NodeP root = new NodeP(7);
        root.left(new NodeP(3, root, 4, 6));
        root.right(new NodeP(8));

        int successor = findSuccessor(root.getRight()); //6
        assertThat(successor).isEqualTo(8);
    }

    private static int findSuccessor(NodeP node) {
        NodeP temp = node;
        System.out.println(node.getKey());
        // has right child - find leftmost child in right subtree
        if (temp.getRight() != null) {
            temp = temp.getRight();
            while(temp.getLeft()!=null){
                temp = temp.getLeft();
            }
            return temp.getKey();
        }
        // does not have children on the right - look first parent to the right
        while (temp.getParent() != null) {
            if (temp.getParent().getLeft() == temp) {
                return temp.getParent().getKey();
            }
            temp = temp.getParent();
        }
        return -1;//not found
    }

}

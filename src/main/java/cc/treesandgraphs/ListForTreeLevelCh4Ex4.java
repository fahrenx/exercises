package cc.treesandgraphs;

import utils.trees.Node;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * lessons learnt:
 *  - if your results require O(n), optimizing your algorithm itself to space complexity below that might be pointless
 */
public class ListForTreeLevelCh4Ex4 {

    public static void main(String[] args) {
        Node root = new Node(1, new Node(2, 3, 4), new Node(6, new Node(5), null));
        List<List<Integer>> lists = listsForLevels(root);
        System.out.println(lists);
        assertThat(lists).hasSize(3);
        assertThat(lists.get(0)).containsExactly(1);
        assertThat(lists.get(1)).containsExactly(2, 6);
        assertThat(lists.get(2)).containsExactly(3, 4, 5);
    }

    /**
     * modification of BFS
     */
    public static List<List<Integer>> listsForLevels(Node root) {
        Queue<Node> q = new LinkedList<>();
        q.add(root);
        List<List<Integer>> results = new LinkedList<>();
        int remainingOnLevel = 1;  // instead of keeping this counters I could reuse lists from previous level as parents lists
        int remainingOnNextLevel = 0;
        int currentLevel = 0;
        while (!q.isEmpty()) {
            Node node = q.poll();
            if (node.getLeft() != null) {
                q.add(node.getLeft());
                remainingOnNextLevel++;
            }
            if (node.getRight() != null) {
                q.add(node.getRight());
                remainingOnNextLevel++;
            }
            addOnLevel(results, currentLevel, node);
            remainingOnLevel--;
            if (remainingOnLevel == 0) {
                currentLevel++;
                remainingOnLevel = remainingOnNextLevel;
                remainingOnNextLevel = 0;
            }
        }
        return results;
    }

    private static void addOnLevel(List<List<Integer>> results, int level, Node node) {
        if (results.size() < level + 1) {
            results.add(new LinkedList<>());
        }
        results.get(level).add(node.getKey());
    }

}

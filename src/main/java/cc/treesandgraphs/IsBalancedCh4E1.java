package cc.treesandgraphs;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * lessons learnt:
 *  - always try to prove your solution is wrong by coming up with interesting cases, samples, don't lazily assume it is correct
 *  - if you see the same computation is being unnecessarily repeated, try to optimize - like the optimization from book
 */
public class IsBalancedCh4E1 {

    public static void main(String[] args) {
        assertThat(isBalanced(new Node(1))).isTrue();

        Node unbalanced = new Node(1, new Node(2, new Node(3), null), null);
        assertThat(isBalanced(unbalanced)).isFalse();

        Node balanced = new Node(1, new Node(2, new Node(3), null), new Node(2));
        assertThat(isBalanced(balanced)).isTrue();

        Node unbalancedTricky = new Node(1, new Node(2, new Node(3, new Node(4), null), null), new Node(6, new Node(7, new Node(10), null), new Node(8, new Node(12), new Node(14, new Node(15), null))));
        assertThat(isBalanced(unbalancedTricky)).isFalse();


    }

    /**
     * Works, but not very efficient, clearly to many repeated traversals while checking height, it is O(n^2)
     */
    public static boolean isBalanced(Node root) {
        if (root == null) return true;
        int maxLeft = findMaxHeightEarlyCheck(root.left, 0);
        int maxRight = findMaxHeightEarlyCheck(root.right, 0);
        if(maxLeft == -1 || maxRight == -1) return false;       // optimization from book
        if (Math.abs(maxLeft - maxRight) > 1) {
            System.out.println("max left :" + maxLeft + " - max right : " + maxRight);
            return false;
        }
        return isBalanced(root.left) && isBalanced(root.right);
    }

    /**
     * incorrect - I forgot about cases where min can be low and max quite high but tree is still balanced
     */
    public static boolean isBalancedIncorrect(Node root) {
        int maxHeight = findMaxHeight(root, 1);
        int minHeight = findMinHeight(root, 1);
        System.out.println("min " + minHeight);
        System.out.println("max " + maxHeight);
        return maxHeight - minHeight <= 1;
    }

    /**
     * part of incorrect solution
     */
    private static int findMinHeight(Node root, int currHeight) {
        if (root == null) {
            return currHeight;
        }
        currHeight++;
        return Math.min(findMaxHeight(root.left, currHeight), findMaxHeight(root.right, currHeight));
    }

    private static int findMaxHeightEarlyCheck(Node root, int currHeight) {
        if (root == null) {
            return currHeight;
        }
        currHeight++;
        int leftMax = findMaxHeight(root.left, currHeight);
        int rightMax = findMaxHeight(root.right, currHeight);
        if(Math.abs(leftMax - rightMax) > 1){
            return -1;
        }
        return Math.max(leftMax, rightMax);
    }

    private static int findMaxHeight(Node root, int currHeight) {
        if (root == null) {
            return currHeight;
        }
        currHeight++;
        return Math.max(findMaxHeight(root.left, currHeight), findMaxHeight(root.right, currHeight));
    }

    public static class Node {

        private final int key;
        private final Node left;
        private final Node right;

        public Node(int key, Node left, Node right) {
            this.key = key;
            this.left = left;
            this.right = right;
        }

        public Node(int key) {
            this(key, null, null);
        }

        public int getKey() {
            return key;
        }

        public Node getLeft() {
            return left;
        }

        public Node getRight() {
            return right;
        }

    }

}

package cc.treesandgraphs;

import com.google.common.collect.ImmutableList;

import java.util.*;

public class GraphsBFSandDFS {


    public static void main(String[] args) {
        Map<Integer, List<Integer>> adj = new HashMap<>();
        adj.put(0, ImmutableList.of(1, 2, 4));
        adj.put(1, ImmutableList.of(0, 3, 4));
        adj.put(2, ImmutableList.of(0, 3));
        adj.put(3, ImmutableList.of(1, 2, 4));
        adj.put(4, ImmutableList.of(0, 1, 3));
        Graph graph = new Graph(adj);
        System.out.println("bfs:");
        graph.bfs(0);
        System.out.println("dfs explicit stack:");
        sampleGraphGoodForDFS().dfsExplicitStack(0);
        System.out.println("dfs rec:");
        sampleGraphGoodForDFS().dfsRecursive(0);

    }

    private static Graph sampleGraphGoodForDFS() {
        Map<Integer, List<Integer>> adj = new HashMap<>();
        adj.put(0, ImmutableList.of(2, 1));
        adj.put(1, ImmutableList.of(0, 3, 4));
        adj.put(2, ImmutableList.of(0, 1));
        adj.put(3, ImmutableList.of(1));
        adj.put(4, ImmutableList.of(1));
        return new Graph(adj);
    }

    public static class Graph {

        private final Map<Integer, List<Integer>> adj;

        public Graph(Map<Integer, List<Integer>> adj) {
            this.adj = adj;
        }

        public List<Integer> getAdjacent(Integer nodeId) {
            return adj.get(nodeId);
        }

        /**
         * BFS = use QUEUE - and that's it, the rest comes from that fact
         * it is up to you what you define as visited and where you call that visit method
         */
        public void bfs(int source) {
            Set<Integer> visited = new HashSet<>(); // those on which visit has been already called
            Queue<Integer> queue = new LinkedList<>();
            visit(source);
            queue.add(source);
            visited.add(source);
            while (!queue.isEmpty()) {
                int curr = queue.poll();
                for (int v : getAdjacent(curr)) {
                    if (!visited.contains(v)) {
                        visit(v);
                        queue.add(v);
                        visited.add(v);
                    }
                }
            }
        }

        public void visit(int nodeId) {
            System.out.println("Visiting node : " + nodeId);
        }

        public void dfsExplicitStack(int source) {
            Set<Integer> visited = new HashSet<>();
            LinkedList<Integer> stack = new LinkedList<>();
            stack.push(source);
            visited.add(source);
            while (!stack.isEmpty()) {
                int curr = stack.pop();
                visit(curr);
                for (int v : getAdjacent(curr)) {
                    if (!visited.contains(v)) {
                        stack.push(v);
                        visited.add(v);
                    }
                }
            }
        }

        public void dfsRecursive(int source) {
            Set<Integer> visited = new HashSet<>();
            dfsRecursive(source, visited);
        }

        private void dfsRecursive(int nodeId, Set<Integer> visited) {
            visit(nodeId);
            visited.add(nodeId);
            for (int v : getAdjacent(nodeId)) {
                if (!visited.contains(v)) {
                    dfsRecursive(v, visited);
                }
            }
        }
    }


}

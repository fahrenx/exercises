package fundamental;

import utils.markers.Solution;

public class Sorting {

    @Solution
    public static void insertionSort(int[] arr) {
        for (int i = 1; i < arr.length; i++) {
            // go to the left with current element as far as it is possible, 'j' will be your pilot
            int j = i - 1;
            int curr = arr[i];
            while (j >= 0 && curr < arr[j]) {
                int temp = arr[j];
                arr[j] = arr[j + 1];
                arr[j + 1] = temp;
                j--;
            }
        }
    }


    public static void quicksort(int[] arr) {
        quicksort(arr, 0, arr.length - 1);
    }

    /**
     * Initial if a bit convoluted but I prefer to catch weird cases after call then before deciding to make a call (start>=end is crucial).
     * Also, be careful with one-off errors - exactly this combination of inequalities in the main loop is needed
     * it is logical you prefer to have i>j not i>=j after coming from while, and also while moving pointers you want to
     * stop at first equal to pivot not too overshoot
     */
    @Solution
    private static void quicksort(int[] arr, int start, int end) {
//        System.out.println(String.format("%s ---  %d : %d", Arrays.toString(arr), start, end));
        if (start < 0  || end > arr.length - 1  || start >= end) return;
        int pivot = arr[start + (end - start) / 2]; // pivot in the middle
        int i = start;
        int j = end;
        while (i < j) {
            while (arr[i] < pivot) {
                i++;
            }
            while (arr[j] > pivot) {
                j--;
            }
            swap(arr, i, j);
            i++;
            j--;
        }
        quicksort(arr, start, j);
        quicksort(arr, i, end);
    }


    @Solution
    public static void radixSort(int[] arr){
        
    }


    private static void swap(int arr[], int i, int j) {
        int temp = arr[j];
        arr[j] = arr[i];
        arr[i] = temp;
    }


    public static void main(String[] args) {

    }

}

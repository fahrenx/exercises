package fundamental;

import java.util.*;


public class RadixSort {


    static class Heap{

        int heap[];
        int size = 0;

        Heap(int size){
            this.heap = new int[size];
        }

        public int takeMin(){
            System.out.println("heap (size" + size +") :"+ asString());
            int res = heap[0];
            heap[0] = heap[size-1];
            size--;
            siftDown(0);
            System.out.println("heap after sifting (size" + size +") :"+ asString());
            return res;
        }

        private String asString(){
            StringBuilder sb = new StringBuilder();
            sb.append("[");
            for(int i=0; i < size; i++){
                sb.append(" ");
                sb.append(heap[i]);
            }
            sb.append("]");
            return sb.toString();
        }

        public void insert(int el){
            heap[size] = el;
            size++;
            siftUp(size - 1);
        }

        public void siftUp(int i){
            int el = heap[i];
            int parentIndex = parent(i);
            if(parentIndex !=-1 && el < heap[parentIndex]){
                // swap
                heap[i] = heap[parentIndex];
                heap[parentIndex] = el;
                // continue
                siftUp(parentIndex);
            }
        }

        public void siftDown(int i){
            int el = heap[i];
            int leftIndex = left(i);
            if(leftIndex !=-1){
                int left = heap[leftIndex];
                int rightIndex = right(i);
                if (rightIndex != -1 && heap[rightIndex] < el){ // if true both left and right have to be checked too
                    if(heap[rightIndex] < left){
                        heap[i] = heap[rightIndex];
                        heap[rightIndex] = el;
                        siftDown(rightIndex);
                    } else {
                        heap[i] = left;
                        heap[leftIndex] = el;
                        siftDown(leftIndex);
                    }
                } else if (left < el){ // if true only left has to be checked
                    heap[i] = left;
                    heap[leftIndex] = el;
                    siftDown(leftIndex);
                }
            }
        }

        int parent(int i){
            int index = (i-1)/2;
            return (index >= size || index < 0) ? -1: index;
        }

        int left(int i){
            int index = 2*i + 1;
            return index >= size ? -1: index;
        }

        int right(int i){
            int index = 2*i + 2;
            return index >= size ? -1: index;
        }
        // [9, 8, 9, 2, 2, 0, 4]
        public void buildUsingFloyd(int[] a) {
            int leavesNumber = (a.length - 1) / 2;
            // adding leaves
            size = a.length;
            for (int i = a.length - leavesNumber; i < a.length; i++) {
                heap[i] = a[i];
            }
            // adding parents and sifting them down
            for (int i = a.length - leavesNumber - 1; i >= 0; i--) {
                heap[i] = a[i];
                siftDown(i);
            }
        }
    }

    public static void main(String[] args) {
        int length = new Random().nextInt(10);
        int[] a = new int[length];
        for (int i = 0; i < a.length; i++) {
            a[i] = new Random().nextInt(10);
        }
        a = new int[]{9, 8, 9, 2, 2, 0, 4};
        System.out.println(Arrays.toString(a));
        heapSort(a);
        System.out.println(Arrays.toString(a));
    }

    private static void heapSort(int a[]) {
        Heap heap = buildHeapFloyd(a);
        for(int i=0; i<a.length;i++){
            int el = heap.takeMin();
            System.out.println("min:" + el);
            a[i] = el;
        }
    }

    private static Heap buildHeapNaive(int a[]){
        Heap h = new Heap(a.length);
        for(int el: a){
            h.insert(el);
        }
        return h;
    }

    private static Heap buildHeapFloyd(int a[]){
        Heap h = new Heap(a.length);
        h.buildUsingFloyd(a);
        return h;
    }


    private static void cycleSort(int a[]){
        for (int cycleStart = 0; cycleStart < a.length -1; cycleStart++) {
            int item = a[cycleStart];
            // count
            int pos = cycleStart;
            for (int j = cycleStart+1; j < a.length ; j++)
                if(a[j] < item) pos++;

            // checking if already in the right place
            if(cycleStart == pos){
                continue;
            }
            // move

            // skip duplicates if any
            while(a[pos] == item) {
                pos++;
            }
            // swap
            a[cycleStart] = a[pos];
            a[pos] = item;

            // move just swapped element to the correct position
            while(pos != cycleStart) {
                pos = cycleStart;
                item = a[pos];
                for (int j = cycleStart + 1; j < a.length; j++)
                    if (a[j] < item) pos++;
                if(pos == cycleStart) continue;
                while (a[pos] == item) {
                    pos++;
                }
                // swap
                a[cycleStart] = a[pos];
                a[pos] = item;
            }
        }
    }



    private static void selectionSort(int[] a){
        for(int i=0; i<a.length-1;i++){
            int minIndex = i;
            for(int j=i+1; j<a.length;j++) {
                if(a[j] < a[minIndex]) minIndex = j;
            }
            int temp = a[minIndex];
            a[minIndex] = a[i];
            a[i] = temp;
        }
    }



    private static void quicksort(int a[], int start, int end){
        if(end <= start) return;
        int pivot = a[start + (end-start)/2];
        int i = start;
        int j = end - 1;
        while(i <= j){
            while(a[i] < pivot) i++;
            while(a[j] > pivot) j--;
            if(i <= j) {
                int temp = a[j];
                a[j] = a[i];
                a[i] = temp;
                i++;
                j--;
            }
        }
        quicksort(a, start, j);
        quicksort(a, i, end);
    }


    private static void quicksort2(int a[], int start, int end){
        if(end <= start) return;
        int pivot = a[end-1];
        int i = start;
        int j = end - 1;
        while(i < j){
            while(i < j && a[i] <= pivot) i++;
            while(j > i && a[j] >= pivot) j--;
            if(i != j) {
                int temp = a[j];
                a[j] = a[i];
                a[i] = temp;
            }
        }
        a[end-1] = a[j];
        a[j] = pivot;

        quicksort(a, start, j);
        quicksort(a, j+1, end);
    }


}

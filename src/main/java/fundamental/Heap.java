package fundamental;

import java.util.ArrayList;
import java.util.List;

/**
 * lessons learnt:
 * - concise naming improves readability - swap is enough and better than swapBetweenIndices
 * - before clicking run, go through your code and double check it, it is perfect moment to catch stupid errors - look for symmetry, variables naming,
 *   one-off errors, flow control, infinite loops etc.
 *
 */
public class Heap {

    List<Integer> elements = new ArrayList<>();

    public static void heapSort(List<Integer> list){
        Heap heap = new Heap();
        for (Integer el: list){
            heap.add(el);
        }
        int size = list.size();
        list.clear();
        for (int i = 0; i < size; i++) {
            list.add(heap.poll());
        }
    }

    public void add(int el) {
        elements.add(el);
        shiftUp(elements.size() - 1);
    }

    public int peek() {
        return elements.get(0);
    }

    public int poll() {
        int element = elements.remove(0);
        shiftDown(0);
        return element;
    }

    private void shiftDown(int index) {
        if (index >= (elements.size() - 1) / 2) {
            return;
        }
        int left = leftChildIndex(index);
        int right = rightChildIndex(index);
        int currEl = elements.get(index);
        if (right == -1) {
            if (currEl > elements.get(left)) {
                swap(left, index);
                shiftDown(left);
            }
        } else if (currEl <= elements.get(left) && currEl <= elements.get(right)) {
            return;
        } else if (elements.get(left) <= elements.get(right) ){
            swap(left, index);
            shiftDown(left);
        } else {
            swap(right, index);
            shiftDown(right);
        }
    }

    /**
     * also known as heapify
     */
    private void shiftUp(int index) {
        int parentIndex = parentIndex(index);
        if (index == 0 || elements.get(index) >= elements.get(parentIndex)) {
            return;
        }
        swap(index, parentIndex);
        shiftUp(parentIndex);
    }

    private void swap(int a, int b) {
        int temp = elements.get(b);
        elements.set(b, elements.get(a));
        elements.set(a, temp);
    }

    private int leftChildIndex(int index) {
        return 2 * index + 1;
    }

    private int rightChildIndex(int index) {
        return 2 * index + 2;
    }

    private int parentIndex(int index) {
        if (index <= 0) {
            return -1;
        }
        return (index - 1) / 2;
    }

}

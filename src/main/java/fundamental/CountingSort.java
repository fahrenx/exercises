package fundamental;

import java.util.Arrays;

/**
 * https://www.cs.usfca.edu/~galles/visualization/CountingSort.html
 * it is O(n+k) - 2 iterations on n and one initialization for k and summing up iteration for k
 */
public class CountingSort {

  public static void main(String[] args) {
    System.out.println(Arrays.toString(new CountingSort().sort(new int[]{3, 1, 2, 2, 5, 4, 3, 1}, 5)));
    System.out.println(Arrays.toString(new CountingSort().sort(new int[]{0, 4, 2, 2, 0, 0, 1, 1, 0, 1, 0, 2, 4, 2}, 4)));
    System.out.println(Arrays.toString(new CountingSort().sort(new int[]{2, 4, 0, 6, 1, 4, 0}, 6)));
  }

  /**
   * @param arr - array of non negative integers between [0,k]
   * @param k   - maximum value in array
   */
  public int[] sort(int[] arr, int k) {
    int[] counts = new int[k + 1];
    for (int el : arr) {
      counts[el]++;
    }
    for (int i = 1; i < counts.length; i++) {
      counts[i] = counts[i] + counts[i - 1];
    }
    int[] output = new int[arr.length];
    for (int i = arr.length - 1; i >= 0; i--) {
      int el = arr[i];
      output[counts[el] - 1] = el;
      counts[el]--;
    }
    return output;
  }
}

package fundamental;


import java.util.Arrays;

import static org.assertj.core.api.AssertionsForInterfaceTypes.assertThat;

/**
 * see Sedgewick
 */
public class StringSort {

  public static void main(String[] args) {
    testKeyCountingSort();
    testLsdSort();

    String[] a = new String[]{"file8.txt", "file2.txt", "file9.txt", "file10.txt", "file001.txt", "file002.txt", "file010.txt", "file011.txt",  "file100.txt"};
    Arrays.sort(a);
    System.out.println(Arrays.toString(a));
  }

  private static void testLsdSort() {
    String[] input = new String[]{"8833", "4435", "4332", "4434", "9932", "2334"};
    assertThat(lsdSort(input, 128)).contains(
      "2334",
      "4332",
      "4434",
      "4435",
      "8833",
      "9932"
    );
  }

  private static void testKeyCountingSort() {
    Entry[] input = new Entry[]{
      new Entry(2, "Fabiański"),
      new Entry(3, "Piszczek"),
      new Entry(1, "Glik"),
      new Entry(1, "Pazdan"),
      new Entry(2, "Jędrzejczyk"),
      new Entry(1, "Mączyński"),
      new Entry(1, "Linetty"),
      new Entry(1, "Zieliński"),
      new Entry(3, "Błaszczykowski"),
      new Entry(2, "Grosicki"),
      new Entry(3, "Lewandowski"),
      new Entry(2, "Cionek"),
      new Entry(2, "Milik"),
      new Entry(3, "Makuszewski")
    };
    assertThat(keyCountingSort(input, 3)).contains(
      new Entry(1, "Glik"),
      new Entry(1, "Pazdan"),
      new Entry(1, "Mączyński"),
      new Entry(1, "Linetty"),
      new Entry(1, "Zieliński"),
      new Entry(2, "Fabiański"),
      new Entry(2, "Jędrzejczyk"),
      new Entry(2, "Grosicki"),
      new Entry(2, "Cionek"),
      new Entry(2, "Milik"),
      new Entry(3, "Piszczek"),
      new Entry(3, "Błaszczykowski"),
      new Entry(3, "Lewandowski"),
      new Entry(3, "Makuszewski")
    );
  }

  /**
   * Assumes that every String has some integer key from known limited range of values
   */
  private static Entry[] keyCountingSort(Entry[] a, int maxValue) {
    int[] count = new int[maxValue + 2]; // 0-index is left empty, rest slots are taken by value+1, e.g for values [0,3] we need [0:4]
    for (Entry i : a) {
      count[i.key + 1]++; // on position i, we have frequency for value i - 1
    }
    for (int i = 1; i < count.length; i++) {
      count[i] = count[i] + count[i - 1]; // now on position i we have info how many elements there is that are smaller than i
    }
    Entry[] sorted = new Entry[a.length];
    for (Entry i : a) {
      // this is critical, you don't do [i.key + 1] anymore - as you are asking now - how many elements there is that are <= than this
      int index = count[i.key];
      sorted[index] = i;
      count[i.key]++; // next element with the same value will have to come after the current so increasing
    }
    return sorted;
  }

  /**
   * a - all elements in this input array must have equal width
   * alphabetSize - for example if each char in input element is ASCII we can assume alphabetSize is 128
   */
  private static String[] lsdSort(String[] a, int alphabetSize) {
    int w = a[0].length(); // input word length
    for (int i = w - 1; i >= 0; i--) { // Leas Significant Digit first
      int[] count = new int[alphabetSize + 1];
      // counting sort over 'column'
      for (String word : a) {
        char ch = word.charAt(i);
        count[ch + 1]++;
      }
      for (int j = 1; j < count.length; j++) {
        count[j] = count[j] + count[j - 1];
      }
      String[] result = new String[a.length]; // need an extra array as can't do it in-place
      for (String word : a) {
        char ch = word.charAt(i);
        result[count[ch]] = word;
        count[ch]++;
      }
      a = result; // result contains words sorted by columns [i:end], assign to a and repeat
    }
    return a; // at this point result contains words sorted by column [0:end] so completely
  }

  private static class Entry {

    public final int key;
    public final String value;

    private Entry(int key, String value) {
      this.key = key;
      this.value = value;
    }

    @Override
    public boolean equals(Object o) {
      if (this == o) return true;
      if (o == null || getClass() != o.getClass()) return false;

      Entry entry = (Entry) o;

      if (key != entry.key) return false;
      return value != null ? value.equals(entry.value) : entry.value == null;
    }

    @Override
    public int hashCode() {
      int result = key;
      result = 31 * result + (value != null ? value.hashCode() : 0);
      return result;
    }

    @Override
    public String toString() {
      return "Entry{" +
        "key=" + key +
        ", value='" + value + '\'' +
        '}';
    }
  }
}

package fundamental;

import java.util.LinkedList;
import java.util.List;

public class BST {

  public final Node root;


  public BST(Node root) {
    this.root = root;
  }

  public static BST create(int rootKey) {
    return new BST(new Node(rootKey));
  }


  public void add(int... keys) {
    for (int k : keys) {
      add(k, root);
    }
  }

  public Node add(int key) {
    return add(key, root);
  }

  public Node add(int key, Node currentNode) {
    if (key == currentNode.getKey()) {
      return null;
    } else if (key < currentNode.getKey()) {
      if (currentNode.getLeft() != null) {
        return add(key, currentNode.getLeft());
      }
      Node node = new Node(key);
      currentNode.setLeft(node);
      return node;
    } else {
      if (currentNode.getRight() != null) {
        return add(key, currentNode.getRight());
      }
      Node node = new Node(key);
      currentNode.setRight(node);
      return node;
    }
  }

  public Node search(int key) {
    return search(key, root);
  }

  public List<Integer> bfs() {
    List<Integer> results = new LinkedList<>();
    LinkedList<Node> queue = new LinkedList<>();
    queue.add(root);
    while (!queue.isEmpty()) {
      Node node = queue.removeFirst();
      results.add(node.getKey());
      if (node.getLeft() != null) {
        queue.add(node.getLeft());
      }
      if (node.getRight() != null) {
        queue.add(node.getRight());
      }
    }
    return results;
  }

  public List<Integer> traverseInOrder() {
    LinkedList<Integer> results = new LinkedList<>();
    traverseInOrder(root, results);
    return results;
  }

  public List<Integer> traversePreOrder() {
    LinkedList<Integer> results = new LinkedList<>();
    traversePreOrder(root, results);
    return results;
  }

  public List<Integer> traversePostOrder() {
    LinkedList<Integer> results = new LinkedList<>();
    traversePostOrder(root, results);
    return results;
  }

  private void traverseInOrder(Node node, LinkedList<Integer> results) {
    if (node.getLeft() != null) {
      traverseInOrder(node.getLeft(), results);
    }
    results.add(node.getKey());
    if (node.getRight() != null) {
      traverseInOrder(node.getRight(), results);
    }
  }

  private void traversePreOrder(Node node, LinkedList<Integer> results) {
    results.add(node.getKey());
    if (node.getLeft() != null) {
      traversePreOrder(node.getLeft(), results);
    }
    if (node.getRight() != null) {
      traversePreOrder(node.getRight(), results);
    }
  }

  /**
   * symmetrical with inOrder, just right swap with left
   */
  private void traversePostOrder(Node node, LinkedList<Integer> results) {
    if (node.getRight() != null) {
      traversePostOrder(node.getRight(), results);
    }
    results.add(node.getKey());
    if (node.getLeft() != null) {
      traversePostOrder(node.getLeft(), results);
    }
  }

  public Node search(int key, Node node) {
    if (node == null) {
      return null;
    } else if (node.getKey() == key) {
      return node;
    } else if (key < node.getKey()) {
      return search(key, node.getLeft());
    } else {
      return search(key, node.getRight());
    }
  }

  public static class Node {

    private final int key;
    private Node left;
    private Node right;

    public Node(int key) {
      this.key = key;
    }

    public Node(int key, Node left, Node right) {
      this.key = key;
      this.left = left;
      this.right = right;
    }

    public int getKey() {
      return key;
    }

    public Node getLeft() {
      return left;
    }

    public void setLeft(Node left) {
      this.left = left;
    }

    public Node getRight() {
      return right;
    }

    public void setRight(Node right) {
      this.right = right;
    }
  }

}

package fundamental;

import java.util.LinkedList;
import java.util.Queue;

public class BinaryTree {

  public final Node root;

  public BinaryTree(int key){
    this.root = new Node(key);
  }

  public void printBFS() {
    Queue<BinaryTree.Node> q = new LinkedList<>();
    q.add(root);
    while(!q.isEmpty()){
      Node el = q.poll();
      System.out.println(el.key);
      if(el.left != null) q.add(el.left);
      if(el.right != null) q.add(el.right);
    }
  }

  public static class Node {
    public final int key;
    public Node left;
    public Node right;

    public Node(int key) {
      this.key = key;
    }

    public Node(int key, Node left, Node right) {
      this.key = key;
      this.left = left;
      this.right = right;
    }

    public Node(int key, int left, int right) {
      this.key = key;
      this.left = new Node(left);
      this.right = new Node(right);
    }

    public int getKey() {
      return key;
    }

    public Node getLeft() {
      return left;
    }

    public Node addLeft(int key) {
      this.left = new Node(key);
      return this.left;
    }

    public Node addLeft(Node left) {
      this.left = left;
      return this.left;
    }

    public Node getRight() {
      return right;
    }

    public Node addRight(Node right) {
      this.right = right;
      return this.right;
    }

    public Node addRight(int key) {
      this.right = new Node(key);
      return this.right;
    }
  }
}

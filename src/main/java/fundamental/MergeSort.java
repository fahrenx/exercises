package fundamental;

import java.util.Arrays;

public class MergeSort {


  public static void main(String[] args) {
    int[] arr = new int[]{-1, 8, 44, 2, 4, 5, 2, 12, 5};
    System.out.println(Arrays.toString(mergeSort(arr, 0, arr.length-1)));
    int[] arr2 = new int[]{-1};
    System.out.println(Arrays.toString(mergeSort(arr2, 0, arr2.length - 1)));
    int[] arr3 = new int[]{2,0};
    System.out.println(Arrays.toString(mergeSort(arr3, 0, arr3.length - 1)));
  }

  public static int[] mergeSort(int[] arr, int start, int end) {
    if(end>arr.length-1){
      return new int[]{};
    }
    if (start == end) {
      return new int[]{arr[start]};
    }
    int mid = start + (end - start) / 2;
    int[] left = mergeSort(arr, start, mid);
    int[] right = mergeSort(arr, mid + 1, end);
    int[] result = new int[left.length + right.length];
    int pos = 0;
    int leftPos = 0;
    int rightPos = 0;
    while (leftPos < left.length && rightPos < right.length) {
      if (left[leftPos] < right[rightPos]) {
        result[pos] = left[leftPos];
        leftPos++;
      } else {
        result[pos] = right[rightPos];
        rightPos++;
      }
      pos++;
    }
    while (leftPos < left.length) {
      result[pos] = left[leftPos];
      pos++;
      leftPos++;
    }
    while (rightPos < right.length) {
      result[pos] = right[rightPos];
      pos++;
      rightPos++;
    }
    return result;
  }

}

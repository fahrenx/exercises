import java.util.*;

/**
 * Bunch of potentially reusable code from exercises.
 * Group them if you recognise some clusters.
 */
public class Snippets {

    public static void main(String[] args) {
        int n = 33;
        StringBuilder sb = new StringBuilder();
        while (n > 0){
            sb.append(n % 2) ;
            n/=2;
        }
        System.out.println(sb.reverse().toString());
    }

    private SortedMap<Integer, List<Integer>> arrayToTreeMap(int[] nums) {
        SortedMap<Integer, List<Integer>> res = new TreeMap<>(Collections.reverseOrder());
        for(int i=0; i<nums.length; i++){
            int value = nums[i];
            List<Integer> curr = res.get(value);
            if(curr == null){
                LinkedList<Integer> ls = new LinkedList<>();
                ls.add(i);
                res.put(value, ls);
            } else {
                curr.add(value);
            }
        }
        return res;
    }


    private long atoi(String str){
        char[] ch= str.toCharArray();
        int i = ch.length-1;
        int base = '0';
        long mul = 1;
        long sum = 0;
        while(i>=0){
            sum+=(ch[i] - base) * mul; // base or mul must be long to make this calculations in long arithmetic
            mul*=10;
            i--;
        }
        return sum;
    }

    private String itoa(long num){
        StringBuilder sb = new StringBuilder();
        int base = '0';
        do{
            char c = (char)(base + (num % 10)); // add codepoint of '0' to map int into appropriate char
            sb.append(c);
            num/=10L;
        }while(num >0);
        return sb.reverse().toString();
    }
    public String multiply(String num1, String num2) {
        StringBuilder sb = new StringBuilder();
        int aLength = num1.length();
        int bLength = num2.length();
        int[] prev = new int[aLength+bLength+2];
        int[] current = new int[aLength+bLength+2];
        for(int i= aLength - 1; i>=0; i--){
            int b = getNumAt(num2, i);
            int carry = 0;
            int index = current.length - 1 - (aLength -1-i); // start at end and move to the left
            for (int j= bLength - 1; j>=0; j--){
                int a = getNumAt(num1, j);
                int mul = a*b;
                int total = mul + carry;
                int toWrite = total % 10;
                carry = total/10;
                current[index] = toWrite;
                index--;
            }
            if(carry>0) current[index] = carry;
            prev = sum(prev, current);
            current = new int[current.length]; // zeroing
        }
        for(int i= prev.length - 1; i>=0; i--){
            sb.append(prev[i]);
        }
        return sb.reverse().toString();
    }

    private int getNumAt(String str, int index){
        return str.charAt(index) - '0';
    }
    private int[] sum(int[] a, int[] b){
        int[] result = new int[a.length];
        int carry = 0;
        for(int i=a.length-1; i>=0; i++){
            int sum = a[i] + b[i] + carry;
            int newPart = sum%10;
            carry = sum/10;
            result[i+1] = newPart;
            if(sum == 0){
                return result;
            }
        }
        return result;
    }
}

package experiment;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.charset.StandardCharsets;
import java.util.*;



public class Solution {
    public static void main(String args[] ) throws Exception {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT */
        FileInputStream is = new FileInputStream(new File("test.txt"));
        System.setIn(is);
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        Map<Integer, Set<Integer>> dcContent = new HashMap<>();
        Map<Integer, Integer> dsToDc = new HashMap<>();

        for (int dc = 1; dc <= n; dc++) {
            Set<Integer> content = new HashSet<>();
            int dataSetsNumber = sc.nextInt();
            for (int i = 0; i < dataSetsNumber; i++) {
                int datasetId = sc.nextInt();
                content.add(datasetId);
                dsToDc.put(datasetId, dc); // it may overwrite previously inserted but it does not matter
            }
            dcContent.put(dc, content); // set content of current data center
        }
        System.out.println(dcContent);
        System.out.println(dsToDc);
        copy(dcContent, dsToDc);
    }

    public static void copy(Map<Integer, Set<Integer>> dcContent, Map<Integer, Integer> dsToDc){
        Set<Integer> allDatasets = dsToDc.keySet();
        for(Map.Entry<Integer, Set<Integer>> e: dcContent.entrySet()){
            int missing = allDatasets.size() - e.getValue().size();
            for(int datasetId: allDatasets){
                if(missing == 0) break; // small optimisation
                if(!e.getValue().contains(datasetId)){ // missing
                    int from = dsToDc.get(datasetId);
                    int to = e.getKey();
                    System.out.println(datasetId + " " + from + " " + to);
                    missing--;
                }
            }
        }
        System.out.println("DONE");
    }
}

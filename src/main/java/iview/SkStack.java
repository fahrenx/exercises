package iview;


import java.util.*;

public class SkStack {

    public static final int MAX_VAL = (int)Math.pow(2,12)-1; // maximum value that can be stored in 12bit unsigned int - it is equal to 4095

    public static void main(String[] args) {
        System.out.println(new SkStack().solution("99*9*9*9*9*9*9*9"));
    }

    public int solution(String S) {
        LinkedList<String> stack = new LinkedList<String>();
        try{
            for(char ch: S.toCharArray()){
                if(isDigit(ch)){
                    stack.push(String.valueOf(ch));
                } else if (ch == '*'){
                    int num1 = Integer.parseInt(stack.pop());
                    int num2 = Integer.parseInt(stack.pop());
                    int result = num1*num2;
                    if(result > MAX_VAL){
                        System.out.println("overflow");
                        return -1;
                    }
                    stack.push(String.valueOf(result));
                } else if (ch == '+'){
                    int num1 = Integer.parseInt(stack.pop());
                    int num2 = Integer.parseInt(stack.pop());
                    int result = num1 + num2;
                    if(result > MAX_VAL){
                        return -1;
                    }
                    stack.push(String.valueOf(result));
                } else {
                    return -1; // unrecognized character
                }
            }
        } catch(NoSuchElementException e){
            return -1;
        }
        if(stack.isEmpty()){
            return -1;
        }
        return Integer.parseInt(stack.pop());
    }

    public boolean isDigit(char ch){
        return ch>='0' && ch<='9';
    }
}

package contest;


import utils.MathUtils;
import utils.markers.Solution;

/**
 * 6 pom
 * <p>
 * https://code.google.com/codejam/contest/32003/dashboard#s=p2
 * <p>
 * - think always about worst case scenarios! like it breaks on floor 1 or it does not break on any floor
 * - the key is to understand that safe/non-breaking drop is reducing scope of search by 1, and non-safe/potentially-breaking
 * drop is reducing scope by half (and as such is used in binary search)
 */
public class EggDrop {

    @Solution
    public EggDropResult calculateFDB(int floors, int maxDrop, int maxBroken) {
        int minDrops =  optimizeForDrops(floors, maxBroken);
        int minBroken = optimizeForBroken(floors, maxDrop, maxBroken);
        long maxFloors = maxFloors(maxDrop, maxBroken);
        return new EggDropResult(maxFloors, minDrops, minBroken);
    }


    private int optimizeForDrops(int floors, int maxBroken) {
        int floorsToCheck = floors;
        int drops = 0;
        int brokenLeft = maxBroken - 1; // at least one broken egg is required if algorithm is supposed to work in all scenarios
        // binary search
        while (brokenLeft > 0) {
            floorsToCheck = floorsToCheck / 2;
            brokenLeft--;
            drops++;
        }
        drops += floorsToCheck; // here we may break at most one egg, we already subtracted it earlier from brokenLeft variable
        return drops;
    }

    private long maxFloors(int maxDrop, int maxBroken) {
        long floorsFromSlowSearch = maxDrop - maxBroken;


        long floorsFromBinarySearch = (long) Math.pow(2, maxBroken) - 1; // this -1 comes from the worst case scenario (and manual calculations)

        long maxFloors = floorsFromBinarySearch + floorsFromSlowSearch;
        if (maxFloors > (int) Math.pow(2, 32)) {
            return -1;
        }
        return maxFloors;
    }

    /**
     * initial broken version, again, similarly for optimizeForBroken I did it opposite to the proper way
     */
    private long maxFloorsV1(int maxDrop, int maxBroken) {
        long floorsFromBinarySearch = (long) Math.pow(2, maxBroken) - 1; // this -1 comes from the worst case scenario (and manual calculations)
        long floorsFromSlowSearch = maxDrop - maxBroken;
        long maxFloors = floorsFromBinarySearch + floorsFromSlowSearch;
        if (maxFloors > (int) Math.pow(2, 32)) {
            return -1;
        }
        return maxFloors;
    }

    private int optimizeForBroken(int floors, int maxDrop, int maxBroken) {
        int dropsLeft = maxDrop;
        int broken = 1; // at leas  t one broken egg is required if algorithm is supposed to work in all scenarios
        // binary search
        int floorsToCheck = floors;
        while (floorsToCheck > 0) {
            // check if we can now finish binary search (which is costly in terms of breaking eggs) and switch to search from bottom up floor by floor
            if (floorsToCheck <= dropsLeft) { // here we may break at most one egg, we already subtracted it earlier from brokenLeft variable
                return broken;
            }
            floorsToCheck = floorsToCheck / 2;
            broken++;
        }
        return broken;
    }

    /**
     * initial version, not working properly
     */
    private int optimizeForBrokenV1(int floors, int maxDrop, int maxBroken) {
        int nonBrokenDrops = maxDrop - maxBroken;
        int broken = 1; // at leas  t one broken egg is required if algorithm is supposed to work in all scenarios
        // we start searching from the bottom to reduce scope of binary search in the next phase
        // consequently reducing the number of broken eggs in total
        int floorsToCheck = floors - nonBrokenDrops;
        // binary search
        while (floorsToCheck > 0) {
            floorsToCheck = floorsToCheck / 2;
            broken++;
        }
        return broken;
    }

    /**
     * auxiliary function
     */
    boolean isSolvable(int floors, int maxDrop, int maxBroken) {
        int floorsToCheck = floors;
        int dropsLeft = maxDrop;
        int brokenLeft = maxBroken - 1; // at least one broken egg is required if algorithm is supposed to work in all scenarios
        while (brokenLeft > 0) {
            floorsToCheck = floorsToCheck / 2;
            brokenLeft--;
            dropsLeft--;
        }
        return dropsLeft >= floorsToCheck;
    }

    /**
     * created to feel the problem better
     */
    private boolean isSolvableInitial(int floors, int maxDrop, int maxBroken) {
        // D-optimization
        int optimalMaxDNeededIfUnlimitedB = (int) Math.floor(MathUtils.log(floors, 2));
        int bNeededForOptimalD = optimalMaxDNeededIfUnlimitedB - 1;

        // B-optimization
        int bNeededifUnlimitedD = 1;
        int maxDNeededIfMnimizedB = floors - 1;

        if (maxDrop >= optimalMaxDNeededIfUnlimitedB) {
            return true;
        }
        if (maxBroken < bNeededForOptimalD) {
            return maxDrop > optimalMaxDNeededIfUnlimitedB;
        }
        return true;
    }

    public static class EggDropResult {

        private final long maxFloors;
        private final int minDrops;
        private final int minBroken;

        public EggDropResult(long maxFloors, int minDrops, int minBroken) {
            this.maxFloors = maxFloors;
            this.minDrops = minDrops;
            this.minBroken = minBroken;
        }

        public long getMaxFloors() {
            return maxFloors;
        }

        public int getMinDrops() {
            return minDrops;
        }

        public int getMinBroken() {
            return minBroken;
        }
    }

}

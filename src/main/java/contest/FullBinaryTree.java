package contest;

import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import utils.ContestIOController;
import utils.markers.Solution;

import java.io.IOException;
import java.util.*;

import static java.util.stream.Collectors.toCollection;

/**
 * lessons learnt:
 *  - brilliant idea about reversing problem - finding biggest full binary tree instead exact deletions that need to be done
 *  - faster solution can be also the one which is easier to implement and more elegant
 *  - priority queue is helpful sometimes but just sorting list will often do
 *
 *  TODO - try linear solution
 */
public class FullBinaryTree {


    public final ContestIOController contestIOController = new ContestIOController();

    public static void main(String[] args) throws IOException {
        System.out.println(new FullBinaryTree().solve("FullBinTree-large-practice.in", true));
    }

    public String solve(String inputFilename, boolean outputFile) throws IOException {
        return contestIOController.processInputMultipleLines(inputFilename, this::solveWrapper, outputFile);
    }

    public String solveWrapper(List<String> input) {
//        System.out.println("---case start");
//        for (String s : input) {
//            System.out.println(s);
//        }
//        System.out.println("---case end");
        return String.valueOf(findMinNumberOfRemovals(Integer.valueOf(input.get(0)), input.subList(1, input.size())));
    }

    @Solution
    public int findMinNumberOfRemovals(int numOfNodes, List<String> edges) {
        Map<Integer, Set<Integer>> adj = convertToAdjacencyList(edges);
        // handling corner cases to simplify code in next sections
        if (numOfNodes == 1) return 0; // degenerated case, nothing to do
        if (numOfNodes == 2) return 1; // one edge, just remove one node

        int result = numOfNodes;
        Set<Integer> possibleRoots = possibleRoots(adj);
        System.out.println("considered roots" + possibleRoots);
        for (Integer root : possibleRoots) {
            int maxSize = findSizeOfMaxFullBinaryTree(adj, -1, root);
            int minRemovals = numOfNodes - maxSize;
            result = Math.min(result, minRemovals);
        }
        return result;
    }

    private int findSizeOfMaxFullBinaryTree(Map<Integer, Set<Integer>> adj, Integer parent, Integer root) {
        Set<Integer> children = adj.get(root).stream().filter(node -> !node.equals(parent)).collect(toCollection(HashSet::new));
        System.out.println("number of children" + children.size());
        if (children.size() < 2) {
            return 1;
        } else if (children.size() == 2) {
            Iterator<Integer> it = children.iterator();
            Integer firstChild = it.next();
            Integer secondChild = it.next();
            return findSizeOfMaxFullBinaryTree(adj, root, firstChild) + findSizeOfMaxFullBinaryTree(adj, root, secondChild) + 1;
        } else {
            LinkedList<Integer> maxSizesForChildren = new LinkedList<>();
            for (Integer child : children) {
                int maxSize = findSizeOfMaxFullBinaryTree(adj, root, child);
                maxSizesForChildren.add(maxSize);
            }
            System.out.println("prio: "+ maxSizesForChildren);
            Collections.sort(maxSizesForChildren);
            return maxSizesForChildren.pollLast()+ maxSizesForChildren.pollLast() + 1;
        }
    }

    private Set<Integer> possibleRoots(Map<Integer, Set<Integer>> adj) {
        Set<Integer> possibleRoots = Sets.newHashSet();
        for (Map.Entry<Integer, Set<Integer>> entry : adj.entrySet()) {
            if (entry.getValue().size() >= 2) {
                possibleRoots.add(entry.getKey());
            }
        }
        return possibleRoots;
    }

    // extract to class

    private Map<Integer, Set<Integer>> convertToAdjacencyList(List<String> edges) {
        Map<Integer, Set<Integer>> adj = Maps.newHashMap();
        for (String edge : edges) {
            String[] xy = edge.split(" ");
            Integer x = Integer.valueOf(xy[0]);
            Integer y = Integer.valueOf(xy[1]);
            addEdge(adj, x, y);
            addEdge(adj, y, x);
        }
        for (Map.Entry entry : adj.entrySet()) {
            System.out.println("key" + entry.getKey() + " values " + entry.getValue());
        }
        return adj;
    }

    private void addEdge(Map<Integer, Set<Integer>> adj, Integer x, Integer y) {
        if (adj.containsKey(x)) {
            adj.get(x).add(y);
        } else {
            Set<Integer> set = new HashSet<>();
            set.add(y);
            adj.put(x, set);
        }
    }

}

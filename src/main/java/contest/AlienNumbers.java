package contest;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.runner.Runner;
import org.openjdk.jmh.runner.RunnerException;
import org.openjdk.jmh.runner.options.Options;
import org.openjdk.jmh.runner.options.OptionsBuilder;
import utils.ContestIOController;
import utils.markers.Solution;

import java.io.IOException;

/**
 * 3 poms (including mechanism for IO and JMH)
 */
@State(Scope.Thread)
public class AlienNumbers {

    public static final AlienNumbers INSTANCE = new AlienNumbers();
    public final ContestIOController contestIOController = new ContestIOController();

    public static void main2(String[] args) throws IOException, RunnerException {
        Options opt = new OptionsBuilder()
                .include(AlienNumbers.class.getSimpleName())
                .warmupIterations(5)
                .measurementIterations(5)
                .forks(1)
                .build();

        new Runner(opt).run();
    }

    @Benchmark
    public void performanceTest(){
        INSTANCE.convert("Foo", "oF8", "0123456789");
    }

    public static void main(String[] args) throws IOException {
        System.out.println(INSTANCE.solve("A-large-practice.in", true));
    }

    public String solve(String inputFilename, boolean outputFile) throws IOException {
        return contestIOController.processInput(inputFilename, this::solveWrapper,outputFile);
    }

    private String solveWrapper(String line){
        String[] args = line.split(" ");
        return convert(args[0], args[1], args[2]);
    }

    /* actual algorithm */

    @Solution
    public  String convert(String number, String sourceLang, String destLang) {
        return convertFromDecimal(convertToDecimal(number, sourceLang), destLang);
    }

    private int convertToDecimal(String number, String lang) {
        char[] numberArr = number.toCharArray();
        int currMultiplier = 1;
        int multiplier = lang.length();
        int decimalValue = 0;
        for (int i = numberArr.length - 1; i >= 0; i--) {
            int digitValue = lang.indexOf(numberArr[i]);
            decimalValue += currMultiplier * digitValue;
            currMultiplier *= multiplier;
        }
        return decimalValue;
    }

    private String convertFromDecimal(int number, String targetLang) {
        int multiplier = targetLang.length();
        StringBuilder sb = new StringBuilder();
        while (number > 0) {
            int reminder = number % multiplier;
            char digitFromTargetLang = targetLang.charAt(reminder);
            sb.append(digitFromTargetLang);
            number = number / multiplier;
        }
        return sb.reverse().toString();
    }


}

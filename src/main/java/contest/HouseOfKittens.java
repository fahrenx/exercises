package contest;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;
import com.google.common.collect.Sets;
import utils.ContestIOController;
import utils.markers.Solution;

import java.io.IOException;
import java.util.*;

import static java.util.stream.Collectors.toCollection;

public class HouseOfKittens {

    public final ContestIOController contestIOController = new ContestIOController();

    public static void main(String[] args) throws IOException {
        System.out.println(new HouseOfKittens().solve("CatnipsSample.in", false));
    }

    public String solve(String inputFilename, boolean outputFile) throws IOException {
        return contestIOController.processInputInNLines(3, inputFilename, this::solveWrapper, outputFile);
    }

    public String solveWrapper(List<String> input) {
        System.out.println("---case start");
        for (String s : input) {
            System.out.println(s);
        }
        System.out.println("---case end");

        String[] verticesAndWalls = input.get(0).split(" ");
        String[] wallsStart = input.get(1).split(" ");
        String[] wallsEnd = input.get(2).split(" ");
        Walls walls = Walls.create(wallsStart, wallsEnd);

        CatnipsAssignment catnipsAssignment = chooseCatnips(Integer.parseInt(verticesAndWalls[0]), Integer.parseInt(verticesAndWalls[1]), walls);

        StringBuilder sb = new StringBuilder();
        sb.append(catnipsAssignment.getNumberOfCatnips()).append("\n");
        for (Integer catnipFlavour : catnipsAssignment.getVertexAndCatnips()) {
            sb.append(catnipFlavour).append(" ");
        }
        return String.valueOf(sb.toString().trim());
    }

    @Solution
    public CatnipsAssignment chooseCatnips(int verticesNum, int wallsNum, Walls walls) {
        Map<Integer, Integer> verticesToNumOfRooms = Maps.newHashMap();
        for (int i = 1; i <= verticesNum; i++) {
            verticesToNumOfRooms.put(i, walls.getDegree(i) + 1);
        }


        // find min vertex room
        // try value above and if not successful decrease by one each time
        // brute-force on distribution of catnips flavour catnipsTypes^verticesNum with early prunning until you run out of options or you found solution
        // how to create distribution? separate function generating Map and this map is argument to further checking function: "boolean allFlavoursAvailableInEachRoom(distribution)"
        // checking if works - all catnips flavours available in each room - best to start with smallest rooms

        return new CatnipsAssignment(2, ImmutableList.of(1, 2));
    }



    public static class Rooms {


        public void addNewRoom(int vertex){
            Set<Integer> room = new HashSet<>();
            room.add(vertex);
            this.rooms.add(room);
        }

        Set<Set<Integer>> rooms = new TreeSet<>(new Comparator<Set<Integer>>() {
            @Override
            public int compare(Set<Integer> o1, Set<Integer> o2) {
                if (o1.size() > o2.size()) return 1;
                else if (o1.size() < o2.size()) return -1;
                return 0;
            }
        });

        public boolean allFlavoursAvailableInEachRoom(Map<Integer, Integer> catnipsDistribution, int catnipsTypes) {
            for (Set<Integer> room : rooms) {
                int numberOfDiffCatnips = room.stream().map(a -> catnipsDistribution.get(a)).collect(toCollection(HashSet::new)).size();
                if (numberOfDiffCatnips != catnipsTypes) {
                    return false;
                }
            }
            return true;
        }


    }

    public static class CatnipsAssignment {

        private final int numberOfCatnips;

        private final List<Integer> vertexAndCatnips;

        public CatnipsAssignment(int numberOfCatnips, List<Integer> vertexAndCatnips) {
            this.numberOfCatnips = numberOfCatnips;
            this.vertexAndCatnips = vertexAndCatnips;
        }

        public int getNumberOfCatnips() {
            return numberOfCatnips;
        }

        public List<Integer> getVertexAndCatnips() {
            return vertexAndCatnips;
        }
    }


    public static class Walls {

        private final Map<Integer, Set<Integer>> walls;

        private Walls(Map<Integer, Set<Integer>> walls) {
            this.walls = new HashMap<>(walls);
        }

        public boolean areConnected(int u, int v) {
            return walls.getOrDefault(u, Sets.newHashSet()).contains(v);
        }

        public int getDegree(int u) {
            return walls.getOrDefault(u, Sets.newHashSet()).size();
        }

        public Set<Integer> getAdjacentVertices(int u) {
            return walls.getOrDefault(u, Sets.newHashSet());
        }

        private static void addWall(Map<Integer, Set<Integer>> walls, Integer x, Integer y) {
            addWallUni(walls, x, y);
            addWallUni(walls, y, x);
        }

        public static Walls create(String[] starts, String[] ends) {
            Map<Integer, Set<Integer>> walls = Maps.newHashMap();
            for (int i = 0; i < starts.length; i++) {
                addWall(walls, Integer.valueOf(starts[i]), Integer.valueOf(ends[i]));
            }
            return new Walls(walls);
        }

        private static void addWallUni(Map<Integer, Set<Integer>> walls, Integer x, Integer y) {
            if (walls.containsKey(x)) {
                walls.get(x).add(y);
            } else {
                Set<Integer> set = new HashSet<>();
                set.add(y);
                walls.put(x, set);
            }
        }


    }


}

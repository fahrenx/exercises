package tcollections;

import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.StringJoiner;


// TODO concurrent or immutable or primitive type list, double linked list
// more comprehensive, testing - multi-step and property based

/**
 * Mutable, single-linked
 *
 * @param <T> type of elements stored
 */
public class TLinkedList<T> implements Iterable<T> {

    private Node<T> head = null;
    private int size = 0;

    public TLinkedList() {
    }

    public TLinkedList(Node<T> head, int size) {
        this.head = head;
        this.size = size;
    }

  public TLinkedList(Node<T> head) {
    this.head = head;
    int i = 0;
    while(head!= null){
      head = head.next;
      i++;
    }
    this.size = i;
  }

    public Node<T> headNode(){
      return head;
    }

    public T get(int index) {
        Node<T> node = moveTo(index);
        return node.value();
    }

    /**
     * Returns sublist starting at 'start' (inclusive) up to index 'end' (exclusive)
     * Sublist is backed by the original list - any changes made to sublist will modify the original list
     * and vice versa. (TODO ???)
     */
    public TLinkedList<T> sublist(int start, int end) {
        validateIndex(start);
        if (end > size) {
            throw new IndexOutOfBoundsException("end cannot be greater than size");
        }
        if (end <= start) { // or strictly less?
            throw new IllegalArgumentException("end must be greater than start");
        }
        Node<T> startingNode = moveTo(start);
        return new TLinkedList<>(startingNode, end - start);
    }

    public T removeAtIndex(int index) {
        validateIndex(index);
        if (index == 0) {
            T removed = head.value;
            head = head.next;
            size--;
            return removed;
        }
        Node<T> precedingNode = moveTo(index - 1);
        T removed = precedingNode.next.value;
        // unlinking
        if (precedingNode.next.next == null) { // if removing last element
            precedingNode.next = null;
        } else { // removing non-last element
            precedingNode.next = precedingNode.next.next;
        }
        size--;
        return removed;
    }

    /**
     * Returns node at position == index
     */
    private Node<T> moveTo(int index) {
        validateIndex(index);
        int i = index;
        Node<T> curr = head;
        while (i > 0) { // move ahead (index - 1) times, finishing at index-th position
            curr = curr.next;
            i--;
        }
        return curr;
    }

    public boolean remove(T el) {
        if (head.value().equals(el)) {
            head = head.next;
            size--;
            return true;
        }

        Node<T> curr = head;
        while (curr.next != null) {
            if (curr.next.value() == el) {
                curr.next = curr.next.next;
                size--;
                return true;
            }
            curr = curr.next;
        }
        return false;
    }

    public void add(T e) {
        if (head == null) {
            head = new Node<>(e, null);
        } else {
            Node<T> curr = head;
            while (curr.next != null) { // move ahead until positioned on last element
                curr = curr.next;
            }
            curr.next = new Node<>(e, null);
        }
        size++;
    }

    public int size() {
        return size;
    }

    private void validateIndex(int index) {
        if (index < 0 || index >= size) {
            throw new IndexOutOfBoundsException("Invalid index: " + index);
        }
    }

    private boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Iterator<T> iterator() {
        return new TLinkedListIterator<T>(head);
    }

    public static class Node<T> {
        private final T value;
        private Node<T> next;

        public Node(T value, Node<T> next) {
            this.value = value;
            this.next = next;
        }

      public Node(T value) {
        this.value = value;
      }

      public Node<T> next() {
            return next;
        }

        public Node<T> setNext(Node<T> next) {
            this.next = next;
            return next;
        }

        public T value() {
            return value;
        }

      @Override
      public String toString() {
        return "Node(" + value + ")";
      }
    }

    public static class TLinkedListIterator<T> implements Iterator<T> {
        private Node<T> curr;

        private TLinkedListIterator(Node<T> curr) {
            this.curr = curr;
        }

        @Override
        public boolean hasNext() {
            return curr != null;
        }

        @Override
        public T next() {
            if (!hasNext()) throw new NoSuchElementException("No element found!");
            T result = curr.value;
            curr = curr.next;
            return result;
        }
    }

    public String prettyPrint() {
      return prettyPrint(-1);
    }

    public String prettyPrint(int max) {
      if(head == null ) return "<empty>";
      StringJoiner sj = new StringJoiner("->", "[", "]");
      int i = 0;
      for(T e: this){
        sj.add(e.toString());
        if(i++ == max) {
          sj.add("...");
          break;
        }
      }
      return sj.toString();
    }

}

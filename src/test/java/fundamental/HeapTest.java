package fundamental;


import com.google.common.collect.ImmutableList;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class HeapTest {

    @Test
    public void givenElementsAdded_whenPeek_thenSmallestElementReturned(){
        Heap heap = new Heap();
        heap.add(5);
        heap.add(3);
        heap.add(2);
        heap.add(11);
        heap.add(4);
        heap.add(8);

        int el = heap.peek();

        assertThat(el).isEqualTo(2);
    }

    @Test
    public void givenElementsAdded_whenPollCalledMultipleTimes_thenElementsReturnedInAscendingOrder(){
        // given
        Heap heap = new Heap();
        ImmutableList<Integer> input = ImmutableList.of(3, 5, 2, 4, 1, 17);
        input.forEach(heap::add);
        // when
        List<Integer> retrieved = new ArrayList(input.size());
        for (int i = 0; i < input.size() ; i++) {
            retrieved.add(heap.poll());
        }
        // then
        assertThat(retrieved).isEqualTo(ImmutableList.of(1, 2, 3, 4, 5, 17));
    }

    @Test
    public void givenList_whenHeapSort_thenListSorted(){
        // given
        List<Integer> input = new ArrayList(Arrays.asList(3, 5, 2, 4, 1, 17));
        // when
        Heap.heapSort(input);
        // then
        assertThat(input).isEqualTo(ImmutableList.of(1, 2, 3, 4, 5, 17));
    }

}
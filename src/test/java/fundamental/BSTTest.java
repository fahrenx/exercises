package fundamental;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class BSTTest {

    @Test
    public void whenNewKeyAdded_thenNodeWithThatKeyReturned() {
        BST tree = BST.create(10);

        BST.Node result = tree.add(20);

        assertThat(result.getKey()).isEqualTo(20);
    }

    @Test
    public void givenKeyAdded_whenTheSameKeyAddedAgain_thenNullReturned() {
        BST tree = BST.create(10);
        assertThat(tree.add(20)).isNotNull();

        BST.Node result = tree.add(20);

        assertThat(result).isNull();
    }

    @Test
    public void givenKeyAdded_whenSearch_thenNodeRetrieved() {
        BST tree = BST.create(10);
        assertThat(tree.add(20)).isNotNull();
        assertThat(tree.add(12)).isNotNull();
        assertThat(tree.add(23)).isNotNull();
        assertThat(tree.add(1)).isNotNull();

        BST.Node result = tree.search(20);

        assertThat(result.getKey()).isEqualTo(20);
    }

    @Test
    public void givenKeyNotAdded_whenSearch_thenNullReturned() {
        BST tree = BST.create(10);
        assertThat(tree.add(20)).isNotNull();
        assertThat(tree.add(12)).isNotNull();
        assertThat(tree.add(23)).isNotNull();
        assertThat(tree.add(1)).isNotNull();

        BST.Node result = tree.search(18);

        assertThat(result).isNull();
    }

    @Test
    public void givenElementsAdded_whenTraverseInOrder_thenReturnedSorted() {
        BST tree = BST.create(10);
        tree.add(20);
        tree.add(40);
        tree.add(30);
        tree.add(1);
        tree.add(55);

        List<Integer> keysInOrder = tree.traverseInOrder();

        assertThat(keysInOrder).containsExactly(1, 10, 20, 30, 40, 55);
    }

    @Test
    public void givenElementsAdded_whenTraversePreOrder_thenReturnedInCorrectOrder() {
        BST tree = BST.create(10);
        tree.add(20);
        tree.add(40);
        tree.add(30);
        tree.add(1);
        tree.add(55);

        List<Integer> keysInOrder = tree.traversePreOrder();

        assertThat(keysInOrder).containsExactly(10, 1, 20, 40, 30, 55);
    }

    @Test
    public void givenElementsAdded_whenTraversePostOrder_thenReturnedInCorrectOrder() {
        BST tree = BST.create(10);
        tree.add(20);
        tree.add(40);
        tree.add(30);
        tree.add(1);
        tree.add(55);

        List<Integer> keysInOrder = tree.traversePostOrder();

        assertThat(keysInOrder).containsExactly(55, 40, 30, 20, 10, 1);
    }

    @Test
    public void givenElementsAddedCase1_whenBfs_thenKeysReturnedInCorrectOrder() {
        BST tree = BST.create(10);
        tree.add(20);
        tree.add(40);
        tree.add(30);
        tree.add(1);
        tree.add(55);

        List<Integer> keysInOrder = tree.bfs();

        assertThat(keysInOrder).containsExactly(10, 1, 20, 40, 30, 55);
    }

    @Test
    public void givenElementsAddedCase2_whenBfs_thenKeysReturnedInCorrectOrder() {
        BST tree = BST.create(10);
        tree.add(8);
        tree.add(12);
        tree.add(6);
        tree.add(9);
        tree.add(11);
        tree.add(13);

        List<Integer> keysInOrder = tree.traversePreOrder();

        assertThat(keysInOrder).containsExactly(10, 8, 12, 6, 9, 11, 13);
    }

}
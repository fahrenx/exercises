package fundamental;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class SortingTest {


    @Test
    public void givenArray_whenInsertionSort_thenSorted() {
        int[] arr = {6, 3, 5, 1};

        Sorting.insertionSort(arr);

        assertThat(arr).isSorted();
    }

    @Test
    public void givenArray_whenQuicksort_thenSorted() {
        checkQuicksort(new int[]{22, 3, 3, 52, 7, 8, 0, 9, 99});
        checkQuicksort(new int[]{0,1,0,6,7});
        checkQuicksort(new int[]{-2,0,-4,6,7});
        checkQuicksort(new int[]{-2,1,-4,6,7});
    }

    private void checkQuicksort(int[] arr) {
        Sorting.quicksort(arr);
        assertThat(arr).isSorted();
    }

}
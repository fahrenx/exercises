package tcollections;

import org.junit.Ignore;
import org.junit.Test;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

// TODO convert to AssertJ and Junit 5
public class TLinkedListTest {

    @Test
    public void whenAdded_shouldIncreaseSize() {
        TLinkedList<Integer> list = ls();
        list.add(5);
        list.add(3);

        assertThat(list.size()).isEqualTo(2);
    }

    @Test
    public void whenNew_shouldBeEmpty() {
        TLinkedList<Integer> list = ls();

        assertThat(list.size()).isEqualTo(0);
        assertContent(list);
    }

    @Test
    public void shouldRetrieveByIndex() {
        TLinkedList<Integer> list = ls();
        list.add(0);
        list.add(100);
        list.add(200);
        list.add(300);
        list.add(400);

        assertThat(list.get(0)).isEqualTo(0);
        assertThat(list.get(1)).isEqualTo(100);
        assertThat(list.get(2)).isEqualTo(200);
        assertThat(list.get(3)).isEqualTo(300);
        assertThat(list.get(4)).isEqualTo(400);
    }

    private <T extends Exception> void assertExceptionThrown(Class<T> clazz, Runnable action) {
        try {
            action.run();
            fail("Exception " + clazz.getSimpleName() + "expected but not thrown");
        } catch (Exception exception) {
            if (clazz.isInstance(exception)) {
                // ok
            } else {
                throw exception;
            }
        }
    }

    @Test
    public void whenGetWithInvalidIndex_shouldThrowOutOfBoundException() {
        assertExceptionThrown(IndexOutOfBoundsException.class, () -> ls().get(-1));
        assertExceptionThrown(IndexOutOfBoundsException.class, () -> ls(0, 100).get(10));
    }

    @Test
    public void whenAdded_shouldRetrieveInTheAddingOrder() {
        TLinkedList<Integer> list = ls();
        int[] elements = new int[]{5, 3, 4};
        for (int e : elements) list.add(e);

        assertContent(list, 5, 3, 4);
    }

    // remove element

    @Test
    public void givenSingleElementsMatching_whenRemove_shouldRemoveIt() {
        TLinkedList<Integer> list = ls(1, 2, 3);

        boolean result = list.remove(2);

        assertThat(result).isTrue();
        assertContent(list, 1, 3);
    }

    @Test
    public void givenHeadElementsMatching_whenRemove_shouldRemoveIt() {
        TLinkedList<Integer> list = ls(1, 2, 3);

        boolean result = list.remove(1);

        assertThat(result).isTrue();
        assertContent(list, 2, 3);
    }

    @Test
    public void givenManyElementsMatching_whenRemove_shouldRemoveFirstOne() {
        TLinkedList<Integer> list = ls(1, 2, 3, 2);

        boolean result = list.remove(2);

        assertThat(result).isTrue();
        assertContent(list, 1, 3, 2);
    }

    @Test
    public void givenNoElementsMatching_whenRemove_shouldReturnFalseAndNotModify() {
        TLinkedList<Integer> list = ls(1, 2, 3);

        boolean result = list.remove(4);

        assertThat(result).isFalse();
        assertContent(list, 1, 2, 3);
    }


    // remove at index

    @Test
    public void givenInvalidIndex_whenRemoveAtIndex_shouldThrowOutOfBoundException() {
        assertExceptionThrown(IndexOutOfBoundsException.class,
                () -> ls(1, 2, 3).removeAtIndex(12));
        assertExceptionThrown(IndexOutOfBoundsException.class,
                () -> ls(1, 2, 3).removeAtIndex(-2));
    }

    @Test
    public void testRemove() {
        TLinkedList<Integer> list = ls(8, 5, 9);

        int removed = list.removeAtIndex(1);

        assertThat(removed).isEqualTo(5);
        assertContent(list, 8, 9); // TODO can I pass int not Integer? varargs?
    }

    @Test
    public void givenLastIndex_whenRemoveAtIndex_shouldRemove() {
        TLinkedList<Integer> list = ls(8, 5, 9);

        int removed = list.removeAtIndex(2);

        assertThat(removed).isEqualTo(9);
        assertContent(list, 8, 5);
    }

    @Test
    public void testRemoveFromFirstIndex() {
        TLinkedList<Integer> list = ls(8, 5, 9);

        int removed = list.removeAtIndex(0);

        assertThat(removed).isEqualTo(8);
        assertContent(list, 5, 9);
    }

    // sublist

    @Ignore
    @Test
    public void whenSublist_correctRangesReturned() {
        TLinkedList<Integer> list = ls(5, 10, 15, 20);

        // TODO this style of multiple assertions is better - migrate to this where possible
        // TODO make sure ls are still usable
        assertContent(list.sublist(0, 1), 5);
        assertContent(list.sublist(0, 2), 5, 10);
        assertContent(list.sublist(0, 3), 5, 10, 15);
        assertContent(list.sublist(0, 4), 5, 10, 15, 20);
        assertContent(list.sublist(2, 4), 15, 20);
        assertContent(list.sublist(3, 4), 20);
        assertContent(list.sublist(1, 4), new Integer[]{10, 15, 20});
    }

    @Ignore
    @Test
    public void whenSublist_changeToSublistVisibleInOriginalOne() {

    }

    @Ignore
    @Test
    public void whenSublist_changeInOriginalListVisibleInSublist() {

    }

    private <T> TLinkedList<T> ls(T... elements) {
        TLinkedList<T> list = new TLinkedList<>();
        for (T e : elements) {
            list.add(e);
        }
        return list;
    }

    private <T> void assertContent(TLinkedList<T> list, T... elements) {
        assertThat(list.size()).isEqualTo(elements.length);
        List<T> expected = Arrays.asList(elements);
        List<T> listContent = new LinkedList<>();
        for (T e : list) { // traversing that way rather than by index for efficiency
            listContent.add(e);
        }
        assertThat(listContent).isEqualTo(expected);
    }
}

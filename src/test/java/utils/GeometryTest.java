package utils;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GeometryTest {


    @Test
    public void intersectionAreaOfCompleteOverlap() {
        long area = Geometry.intersectionArea(
                0, 0, 3, 3,
                0, 0, 3, 3);

        assertThat(area).isEqualTo(9);
    }

    @Test
    public void intersectionAreaOfFirstInsideSecond() {
        long area = Geometry.intersectionArea(
                0, 0, 5, 5,
                1, 2, 4, 4);

        assertThat(area).isEqualTo(6);
    }

    @Test
    public void intersectionAreaOfSecondInsideFirst() {
        long areaOfIdentical = Geometry.intersectionArea(
                1, 1, 2, 4,
                0, 0, 6, 6);

        assertThat(areaOfIdentical).isEqualTo(3);
    }

    @Test
    public void intersectionAreaOfNonOverlappingIsZero() {
        long areaOfIdentical = Geometry.intersectionArea(
                1, 1, 2, 2,
                3, 3, 5, 5);

        assertThat(areaOfIdentical).isEqualTo(0);
    }

    @Test
    public void intersectionOfOverlapping() {
        long area = Geometry.intersectionArea(2, 0, 4, 6, 3, 1, 5, 2);

        assertThat(area).isEqualTo(1);
    }


}
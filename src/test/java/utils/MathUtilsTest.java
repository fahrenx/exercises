package utils;

import org.junit.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class MathUtilsTest {

  @Test
  public void combNoRep() {
    assertThat(MathUtils.combNoRep(5, 10)).isEqualTo(252);
    assertThat(MathUtils.combNoRep(9, 24)).isEqualTo(1307504);
    assertThat(MathUtils.combNoRep(10, 44)).isEqualTo(2481256778L);
//        assertThat(MathUtils.combNoRep(10, 88)).isEqualTo(4513667845896L); //TODO overflow
  }

  @Test
  public void combNoRepOptimized() {
    assertThat(MathUtils.combNoRepOptimized(5, 10)).isEqualTo(252);
    assertThat(MathUtils.combNoRepOptimized(9, 24)).isEqualTo(1307504);
    assertThat(MathUtils.combNoRepOptimized(10, 44)).isEqualTo(2481256778L);
    //        assertThat(MathUtils.combNoRep(10, 88)).isEqualTo(4513667845896L); //TODO overflow

  }

  @Test
  public void factorial() {
    assertThat(MathUtils.factorial(3)).isEqualTo(6);
    assertThat(MathUtils.factorial(5)).isEqualTo(120);
  }

  @Test
  public void sumInRange() {
    assertThat(MathUtils.sumInRange(0, 1)).isEqualTo(1);
    assertThat(MathUtils.sumInRange(-2, 0)).isEqualTo(-3);
    assertThat(MathUtils.sumInRange(1, 5)).isEqualTo(15);
    assertThat(MathUtils.sumInRange(1, 4)).isEqualTo(10);
  }

  @Test
  public void testCombGeneration() {
    combGenTest1(new int[]{0, 1, 2}, 2);
    combGenTest1(new int[]{0, 1, 2, 3}, 2);
    combGenTest1(new int[]{0, 1, 2, 3}, 4);
    combGenTest1(new int[]{0, 1, 2, 3, 4 , 5}, 3);


  }

  private void combGenTest1(int[] arr, int k) {
    List<List<Integer>> results = MathUtils.generatorCombinations(arr, k);
    System.out.println(results);
    assertThat((long) results.size()).isEqualTo(MathUtils.combNoRep(k, arr.length));
  }

}
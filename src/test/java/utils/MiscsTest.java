package utils;


import org.assertj.core.api.AbstractListAssert;
import org.junit.Test;

import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;

public class MiscsTest {

    @Test
    public void givenNotEnoughElements_whenSplitIntoChunks_thenLastChunkIsZero() {
        List<String> chunks = Miscs.splitStringIntoNChunks("123", 4);

//        assertChunkSizes(chunks, 1, 1, 1, 0);
    }

    @Test
    public void givenMultiplyOf_whenSplitIntoNChunks_thenAllChunksEqual() {
        List<String> chunks = Miscs.splitStringIntoNChunks("123456789", 3);

//        assertChunkSizes(chunks, 3, 3, 3);
    }

    @Test
    public void givenNotMultiplyOfN_whenSplitIntoNChunks_thenAllChunksAlmostEquals() {
        List<String> chunks = Miscs.splitStringIntoNChunks("1234567", 3);

//        assertChunkSizes(chunks, 3, 2, 2);
    }

    @Test
    public void givenZeroElements_whenSplitIntoChunks_thenNZeroChunks() {
        List<String> chunks = Miscs.splitStringIntoNChunks("", 4);

//        assertChunkSizes(chunks, 0, 0, 0, 0);
    }
//
//    private AbstractListAssert<?, ? extends List<? extends Integer>, Integer> assertChunkSizes(List<String> chunks, Integer... sizes) {
//        return assertThat(chunks.stream().map(str -> str.length()).collect(Collectors.toList())).containsExactly(sizes);
//    }

}

package utils;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class MaximumSliceTest {

    @Test
    public void whenMaximumSliceLinear_thenCorrectAnswer(){
        assertThat(MaximumSlice.maximumSliceLinear(new int[]{1, -1, 2, 5})).isEqualTo(7);
        assertThat(MaximumSlice.maximumSliceLinear(new int[]{3, -1, 1, 2, -5})).isEqualTo(5);
    }

}
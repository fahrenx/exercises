package utils;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class LeadersTest {

    @Test
    public void whenFindLeaderBrute_thenCorrectResult(){
        assertThat(Leaders.findLeaderBrute(new int[]{1,2,3,1,1}).get()).isEqualTo(1);
        assertThat(Leaders.findLeaderBrute(new int[]{7,7,3,7,1}).get()).isEqualTo(7);
        assertThat(Leaders.findLeaderBrute(new int[]{7,4,3,7,1}).isPresent()).isFalse();
        assertThat(Leaders.findLeaderBrute(new int[]{2}).get()).isEqualTo(2);
        assertThat(Leaders.findLeaderBrute(new int[]{}).isPresent()).isFalse();
    }

    @Test
    public void whenFindLeaderSort_thenCorrectResult(){
        assertThat(Leaders.findLeaderSort(new int[]{1, 2, 3, 1, 1}).get()).isEqualTo(1);
        assertThat(Leaders.findLeaderSort(new int[]{7, 7, 3, 7, 1}).get()).isEqualTo(7);
        assertThat(Leaders.findLeaderSort(new int[]{7, 4, 3, 7, 1}).isPresent()).isFalse();
        assertThat(Leaders.findLeaderSort(new int[]{2}).get()).isEqualTo(2);
        assertThat(Leaders.findLeaderSort(new int[]{}).isPresent()).isFalse();
    }

    @Test
    public void whenFindLeaderLinear_thenCorrectResult(){
        assertThat(Leaders.findLeaderLinear(new int[]{1, 2, 3, 1, 1}).get()).isEqualTo(1);
        assertThat(Leaders.findLeaderLinear(new int[]{7, 7, 3, 7, 1}).get()).isEqualTo(7);
        assertThat(Leaders.findLeaderLinear(new int[]{7, 4, 3, 7, 1}).isPresent()).isFalse();
        assertThat(Leaders.findLeaderLinear(new int[]{2}).get()).isEqualTo(2);
        assertThat(Leaders.findLeaderLinear(new int[]{}).isPresent()).isFalse();
    }


}
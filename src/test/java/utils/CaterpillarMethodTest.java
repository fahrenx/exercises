package utils;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class CaterpillarMethodTest {

    @Test
    public void givenMineAlgorithm_whenLookingForSum_thenCorrectResult() {
        assertThat(CaterpillarMethod.hasSumMine(new int[]{2, 3, 4}, 7)).isTrue();
        assertThat(CaterpillarMethod.hasSumMine(new int[]{1, 3, 4}, 2)).isFalse();
        assertThat(CaterpillarMethod.hasSumMine(new int[]{1, -3, 4}, 2)).isTrue();
        assertThat(CaterpillarMethod.hasSumMine(new int[]{4, 5, 6}, 7)).isFalse();
        assertThat(CaterpillarMethod.hasSumMine(new int[]{3, 3, 2}, 10)).isFalse();
    }

    @Test
    public void givenMineAlgorithmForNonNegatives_whenLookingForSum_thenCorrectResult() {
        assertThat(CaterpillarMethod.hasSumMineNonNegative(new int[]{2, 3, 4}, 7)).isTrue();
        assertThat(CaterpillarMethod.hasSumMineNonNegative(new int[]{1, 3, 4}, 2)).isFalse();
        assertThat(CaterpillarMethod.hasSumMineNonNegative(new int[]{4, 5, 6}, 7)).isFalse();
        assertThat(CaterpillarMethod.hasSumMineNonNegative(new int[]{3, 3, 2}, 10)).isFalse();
        assertThat(CaterpillarMethod.hasSumMineNonNegative(new int[]{3, 3, 2}, 10)).isFalse();

    }

    @Test
    public void givenCaterpillarAlgorithm_whenLookingForSum_thenCorrectResult() {
        assertThat(CaterpillarMethod.hasSumNonNegative(new int[]{2, 3, 4}, 7)).isTrue();
        assertThat(CaterpillarMethod.hasSumNonNegative(new int[]{1, 3, 4}, 2)).isFalse();
        assertThat(CaterpillarMethod.hasSumNonNegative(new int[]{4, 5, 6}, 7)).isFalse();
        assertThat(CaterpillarMethod.hasSumNonNegative(new int[]{3, 3, 2}, 10)).isFalse();
        assertThat(CaterpillarMethod.hasSumNonNegative(new int[]{3, 3, 2}, 10)).isFalse();
    }

}
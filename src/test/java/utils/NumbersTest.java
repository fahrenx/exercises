package utils;


import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class NumbersTest {

    @Test
    public void testChangeBaseFromDec(){
        assertThat(Numbers.changeBaseDigitsOnly("123", 10, 2)).isEqualTo(Integer.toString(123, 2));
        assertThat(Numbers.changeBaseDigitsOnly("123", 10, 8)).isEqualTo(Integer.toString(123, 8));
        assertThat(Numbers.changeBaseDigitsOnly("0", 10, 8)).isEqualTo(Integer.toString(0, 8));
        assertThat(Numbers.changeBaseDigitsOnly("333", 10, 10)).isEqualTo("333");
    }
    //TODO how about negative numbers?

    @Test
    public void testChangeBaseFromNonDec(){
        // TODO
//        assertThat(Numbers.changeBaseDigitsOnly("1001", 2, 10)).isEqualTo("9");
    }
}
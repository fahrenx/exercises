package contest;

import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EggDropTest {


    @Test
    public void whenSample1_thenSolvable(){
        boolean isSolvable = new EggDrop().isSolvable(3, 3, 3);

        assertThat(isSolvable).isTrue();
    }

    @Test
    public void whenSample2_thenSolvable(){
        boolean isSolvable = new EggDrop().isSolvable(7, 5, 3);

        assertThat(isSolvable).isTrue();
    }

    @Test
    public void whenSample1_thenCorrectAnswer(){
        EggDrop.EggDropResult eggDropResult = new EggDrop().calculateFDB(3, 3, 3);

        assertThat(eggDropResult.getMaxFloors()).isEqualTo(7);
        assertThat(eggDropResult.getMinDrops()).isEqualTo(2);
        assertThat(eggDropResult.getMinBroken()).isEqualTo(1);
    }

    @Test
    public void whenSample2_thenCorrectAnswer(){
        EggDrop.EggDropResult eggDropResult = new EggDrop().calculateFDB(7, 5, 3);

        assertThat(eggDropResult.getMaxFloors()).isEqualTo(25);
        assertThat(eggDropResult.getMinDrops()).isEqualTo(3);
        assertThat(eggDropResult.getMinBroken()).isEqualTo(2);
    }


}
package contest;

import org.junit.Test;
import utils.Constants;
import utils.Miscs;

import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

public class AlienNumbersTest {

    @Test
    public void whenSample1_thenCorrectResult() {
        String convertedNumber = new AlienNumbers().convert("Foo", "oF8", "0123456789");

        assertThat(convertedNumber).isEqualTo("9");
    }

    @Test
    public void whenSample2_thenCorrectResult() {
        String convertedNumber = new AlienNumbers().convert("9", "0213456789", "oF8");

        assertThat(convertedNumber).isEqualTo("Foo");
    }

    @Test(timeout = 3000)
    public void whenSampleFile1_thenReturnsCorrectResult() throws IOException {
        String result = new AlienNumbers().solve("alien1.in", false);

        String expected = Miscs.readFileAsString(Constants.OLD_INPUT_FILES_DIR + "/alien1.expected.out");
        assertThat(result).isEqualTo(expected);
    }
}
## configuring JMH


- add [Gradle plugin](https://github.com/melix/jmh-gradle-plugin) to build file
- move your test class to `jmh/test`
- make sure that: 
  - benchmark class is public and nested in some package
  - regex in build config like `jmh { include = ['Solution*'] }` matches benchmark class
  - benchmark class has `@Benchmark` annotation *
  - there are no no-arg constructor (default constructor).
  - inner classes arevstatic.
- run `gradle jmh`

## other notes about running:
* Grade - If something seems odd/not refreshen then kill the daemon - "grade —stop” also delete "build" and "out" folder
* setting parameters in build file works, can't use cli ones: https://github.com/melix/jmh-gradle-plugin#configuration-options
* Examples: https://github.com/melix/jmh-gradle-example

`*` - jmh gradle plugin ignores main in normal `gradle jmh` and applies default parameters like:
```
# Warmup: 20 iterations, 1 s each
# Measurement: 20 iterations, 1 s each
# Timeout: 10 min per iteration
# Threads: 1 thread, will synchronize iterations
# Benchmark mode: Throughput, ops/time
# Benchmark: solutions.AvgArraySolution.performanceTest
```


### configuration and interpretation of results


```
Result "solutions.AvgArraySolution.performanceTest":
  25.094 ±(99.9%) 0.206 ops/s [Average]
  (min, avg, max) = (22.001, 25.094, 27.629), stdev = 0.871
  CI (99.9%): [24.889, 25.300] (assumes normal distribution)


# Run complete. Total time: 00:07:42

Benchmark                  Mode  Cnt   Score   Error  Units
Solution.performanceTest  thrpt  200  25.094 ± 0.206  ops/s
```

This are results for no-op
```
11:24:30.489 [QUIET] [system.out] Result "solutions.AvgArraySolution.performanceTest":
11:24:30.489 [QUIET] [system.out]   1623899438.550 ±(99.9%) 14398482.975 ops/s [Average]
11:24:30.489 [QUIET] [system.out]   (min, avg, max) = (1365441165.655, 1623899438.550, 1773271201.957), stdev = 60964078.501
11:24:30.489 [QUIET] [system.out]   CI (99.9%): [1609500955.575, 1638297921.525] (assumes normal distribution)
11:24:30.489 [QUIET] [system.out]
11:24:30.490 [QUIET] [system.out]
11:24:30.490 [QUIET] [system.out] # Run complete. Total time: 00:07:34
11:24:30.490 [QUIET] [system.out]
11:24:30.492 [QUIET] [system.out] Benchmark                  Mode  Cnt           Score          Error  Units
11:24:30.493 [QUIET] [system.out] Solution.performanceTest  thrpt  200  1623899438.550 ± 14398482.975  ops/s
```


__home setup__

`Intel(R) Core(TM) i7-7820HQ CPU @ 2.90GHz`
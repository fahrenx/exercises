/*

    how to get speed?
    1) target t and distance?
    2) some dependency on progress?
    3) based on timescale?

    TODO:
    readings :
    - current speeds
    - distance to next
    - position
    - tyres - type and condition
    - weather
    - last lap
    - position
    - oil temperature
    - brake temperatur
    - temperator
    - weather
    - gearbox, cooling condition
    - damage
    - gear (not yet given acceleration is not simulated)

    control:
    - tires for next pit stop
    - aggressiveness level - increases risk of accident and tyre usage but speeds up
    - extras: wing adjustements fixes, debris removal from radiators, replacing damaged parts

*/

var config = {
    type: Phaser.AUTO,
    width: 800,
    height: 600,
    backgroundColor: '#2d2d2d',
    parent: 'phaser-example',
    scene: {
        create: create,
        update: update
    }
};

var path;
var graphics;

var game = new Phaser.Game(config);
var tweens = [];
var i = 0;

var text;

var pitStopRequested = false;
var pitStopTime = 0;
player = "BOT";
var drivers = {
    "BOT" : {speed: 1.015, team: "MER"},
    "HAM" : {speed: 1.02, team: "MER"},
    "VET" : {speed: 1.015, team: "FER"},
    "VER" : {speed: 1.018, team: "RED"}
}

var teams = {
    "MER": {color: "0xD3D3D3"},
    "FER": {color: "0xff0000"},
    "RED": {color: "0x0000ff"}
}


// name, laps, last lap
raceResults = {}
var arr = [];

function preload() {

    game.load.image('diamond', 'assets/sprites/diamond.png');
    game.load.spritesheet('button', 'assets/buttons/button_sprite_sheet.png', 193, 71);
}

var followers = {};

var driversSectors = {
}

sectors = {
        "first-long":   {x:50, y:500, speed: 1},
        "ellipses":     {x:150, y:300, speed: 0.65},
        "second-long":  {x:500, y:50, speed: 1},
        "gentle":       {x:650, y:300, speed: 0.7},
        "last-long":    {x:420, y:462, speed: 1},
        "last-curve":   {x:100, y:500, speed: 0.5}
}


function create ()
{
    graphics = this.add.graphics();

    for(d in drivers){
        console.log("driver" + d);
        followers[d] = { t: 0, vec: new Phaser.Math.Vector2() };
    }

    // setting up a circuit
    //  Path starts at 100x100
    path = new Phaser.Curves.Path(50, 500);
    path.lineTo(150, 300);


    // xRadius, yRadius, startAngle, endAngle, clockwise, rotation
    path.ellipseTo(200, 100, 120, 300, false, 45);


    path.ellipseTo(20, 100, 100, 300, true);
    path.ellipseTo(20, 20, 120, 300, false);
    path.ellipseTo(20, 20, 120, 300, true);
    path.ellipseTo(20, 20, 120, 300, false);
    path.lineTo(650, 300);
    path.ellipseTo(100, 100, 10, 100, false);
    path.ellipseTo(100, 100, 10, 100, false);
    path.lineTo(100, 500);
    path.ellipseTo(25, 25, 0, 180, false);

    // https://stackoverflow.com/questions/750486/javascript-closure-inside-loops-simple-practical-example?rq=1
    // wrapper for closures
    var that = this;
    function createfunc(name) {
        return function() { printLap(name, that); };
    }
    // path.ellipseTo(60);
    for(var name in drivers){
        driverProps = drivers[name];
        tw = this.tweens.add({
            targets: followers[name],
            t: 1,
            ease: 'Easing.Linear',
            duration: 10000 / driverProps.speed,
            yoyo: false,
            repeat: 5,
            onRepeat: createfunc(name),
        });
        tweens[name] = tw;
    }

    // global events
    // var timer = this.time.addEvent({
    //     delay: 500,                // ms
    //     callback: changeSpeed,
    //     //args: [],
    //     callbackScope: this,
    //     loop: true
    // });

    // buttons, text
    const pauseButton = this.add.text(100, 100, 'Pause II', { fill: '#0f0' });
    pauseButton.setInteractive().on('pointerdown', () => pause() );
    const pitStopButton = this.add.text(100, 50, 'Pit stop', { fill: '#0f0' });
    pitStopButton.setInteractive().on('pointerdown', () => pitStop() );
    createText(this);
    console.log(tweens);
    console.log(drivers);
    console.log(followers);

}

function createText(gameObj){

    var style = { font: "16px Courier", fill: "#fff", tabs: [ 164, 120, 80 ] };

    var headings = [ 'Name', 'Laps', 'Time' ];

    textHeaders = gameObj.add.text(200, 500, 'Name\t\Laps\tTime', style);
    // textHeaders.parseList(headings);


    var content = "";
    for(var entry in raceResults){
        line = ""
        for( c in raceResults[entry]){
            console.log(raceResults[entry][c]);
            line = line + raceResults[entry][c] + "\t  "
        }
        content = content + line + "\n";
    }
    console.log(content);
    text = gameObj.add.text(200, 520, content, style);

    // text.parseList(raceResults);
}


function printLap(name, gameObj) {
    // timeScale does not affect duration or elapsed, it just speeds up the game time and how much given duration will take of real time

    console.log("name: " + name);
    t = tweens[name]
    // console.log("Lap time: " + t.totalDuration);
    // console.log("elapsed : " + t["elapsed"]);
    // console.log("progress : " + t["progress"]);
    // console.log("timescale : " + t["timeScale"]);
    console.log("Last lap real time : " + t["elapsed"] / t["timeScale"] + pitStopTime);
    pitStopTime = 0;
    console.log(t);
    if(name == player && pitStopRequested){
        console.log("Starting pit stop...");
        tweens[player].timeScale = 0;
        pitStopTime = 2000 + 2000 * Math.random();
        var timer = gameObj.time.addEvent({
            delay: pitStopTime,
            callback: finishPitStop,
            //args: [],
            callbackScope: this,
            loop: false
        });
        pitStopRequested = false;
    }
}

function finishPitStop(){
    console.log("Finishing pistop, it took " + pitStopTime + " ms");
    tweens[player].timeScale = 1.0;
}



function pitStop() {
    console.log("Pit stop requested");
    pitStopRequested = true;
    // tweens.forEach(function(t){
    //     var newSpeed = 0.5 + Math.random();
    //     t.timeScale = newSpeed;
    // });
}


var flag = true;
function pause() {
    if (flag){
        for(k in tweens){
            tweens[k].pause();
            console.log(tweens);
            console.log(path.getPoint(followers[k].t, followers[k].vec));
        };
    } else {
        for(k in tweens){
            tweens[k].resume();
        };
    }
    flag = !flag;
}

var prevPoint;
var prevProgress;
var prevElapsed;

var i=0;
function update ()
{
    graphics.clear();
    graphics.lineStyle(2, 0xffffff, 1);
    path.draw(graphics);

    // checking speed:
    // progressDiff gives constant
    // xy diff is screwed by curves
    // I want - slower on curves, faster on straight,
    // last one on paused, slow on pit stop, differences between drivers
    point = path.getPoint(followers[player].t, followers[player].vec);
    elapsed= tweens[player].elapsed;
    progress = tweens[player].progress;
    if(prevPoint){
        var dist = Phaser.Math.Distance.Between(point.x,point.y,prevPoint.x,prevPoint.y);
        var progressDiff = progress - prevProgress
        if(progressDiff == 0){ // check if paused?
            console.log("speed: 0");
        }else {
            console.log("speed: " + progressDiff/(elapsed - prevElapsed) * 1000000);
        }
    }
    i++;
    // prevPoint = point;
    prevElapsed = elapsed;
    prevProgress = progress;


    graphics.fillStyle(0xff0000, 1);
    for(s in sectors){
        props = sectors[s];
        graphics.fillCircle(props.x, props.y, 2);
    }


    for(var driverName in followers){
        var f = followers[driverName];
        // console.log("Follower" + followers[driverName]);
        // path.getPoint(f.t, f.vec);
        var color = teams[drivers[driverName].team].color;
        var point = path.getPoint(f.t, f.vec);
        graphics.fillStyle(color, 1);
        graphics.fillCircle(f.vec.x, f.vec.y, 12);
        for(var s in sectors){
            var props = sectors[s];
            var dist = Phaser.Math.Distance.Between(point.x, point.y, props.x, props.y);
            if(dist < 5.0 && driversSectors[driverName] != s){
                // console.log("Entering sector " + s);
                driversSectors[driverName] = s;
                tweens[driverName].timeScale = drivers[driverName].speed * props.speed;
            }
        }
    }


}
